<?php

namespace Fie\Automation\App;

use Fie\Automation\Service;

require_once('bootstrap.php');

if (Service::isApi()) {
    require_once('app/JsonService.php');
} else {
    require_once('app/HtmlService.php');
}
