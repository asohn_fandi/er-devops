<?php

namespace Fie\Test\Automation;

use Fie\Automation\ServiceBase;

final class SimpleService extends ServiceBase
{
    private $ssOptions = [];
    private $ssHelpText = '';

    public function ssSetOptions($options) {
        $this->ssOptions = $options;
        return $this;
    }
    public function ssSetHelpText($helpText) {
        $this->ssHelpText = $helpText;
        return $this;
    }

    # extended methods:
    protected function onSuccess(): void {}
    protected function onFailure(): void {}
    protected function getServiceOptions(): array { return $this->ssOptions; }
    protected function help(): string { return $this->ssHelpText; }
    protected function configure(): ServiceBase { return $this; }
    public function run(): ServiceBase { return $this; }
}
