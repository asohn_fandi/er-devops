<?php

namespace Fie\Test\Automation\Auth;

use Fie\Automation\Auth\User;
use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase
{
    /**
     * @covers \Fie\Automation\Auth\User::authenticate()
     */
    public function testAuthenticate() {
        # Arrange
        $user = new User();

        # Act
        $result = $user->authenticate();

        # Assert
        $this->assertTrue($result);
    }
}
