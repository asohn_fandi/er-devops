<?php

namespace Fie\Test\Automation\Shell;

use Fie\Automation\ServiceUtils\Logger;
use Fie\Automation\Shell\ExecuteCommand;
use PHPUnit\Framework\TestCase;

final class ExecuteCommandTest extends TestCase
{
    /**
     * @covers \Fie\Automation\Shell\ExecuteCommand::__construct()
     * @covers \Fie\Automation\Shell\ExecuteCommand::run()
     * @covers \Fie\Automation\Shell\ExecuteCommand::getExitStatus()
     * @dataProvider dataRunExitStatus
     * @param int $expectedExitStatus
     * @param string $command
     */
    public function testRunExitStatus($expectedExitStatus, $command) {
        # Arrange
        $logger = (new Logger())->silent();
        $executeCommand = new ExecuteCommand($logger);

        # Act
        $executeCommand->run($command);
        $exitStatus = $executeCommand->getExitStatus();

        # Assert
        $this->assertSame($expectedExitStatus, $exitStatus);
    }
    public function dataRunExitStatus() {
        return [
            'no command'      => [1, ''],
            'valid command'   => [0, 'ls /'],
        ];
    }

    /**
     * @covers \Fie\Automation\Shell\ExecuteCommand::__construct()
     * @covers \Fie\Automation\Shell\ExecuteCommand::run()
     * @covers \Fie\Automation\Shell\ExecuteCommand::getExitStatus()
     */
    public function testRunExitStatusInvalidCommand() {
        # Arrange
        $logger = (new Logger())->silent();
        $executeCommand = new ExecuteCommand($logger);

        # Act
        $executeCommand->run('ls /dev/null/bogus');
        $exitStatus = $executeCommand->getExitStatus();

        # Assert
        $this->assertNotSame(0, $exitStatus);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecuteCommand::__construct()
     * @covers \Fie\Automation\Shell\ExecuteCommand::run()
     * @covers \Fie\Automation\Shell\ExecuteCommand::getOutput()
     * @dataProvider dataRunOutput
     * @param string $pipeName
     * @param string $command
     */
    public function testRunOutput($pipeName, $command) {
        # Arrange
        $logger = (new Logger())->silent();
        $executeCommand = new ExecuteCommand($logger);

        # Act
        $executeCommand->run($command);
        $output = $executeCommand->getOutput();

        # Assert
        $this->assertNotEmpty($output[$pipeName]);
    }
    public function dataRunOutput() {
        return [
            'has STDOUT' => ['stdout', 'ls /'],
            'has STDERR' => ['stderr', 'ls /dev/null/bogus'],
        ];
    }

    /**
     * @covers \Fie\Automation\Shell\ExecuteCommand::__construct()
     * @covers \Fie\Automation\Shell\ExecuteCommand::run()
     * @covers \Fie\Automation\Shell\ExecuteCommand::getSTDOUT()
     */
    public function testGetSTDOUT() {
        # Arrange
        $logger = (new Logger())->silent();
        $executeCommand = new ExecuteCommand($logger);

        # Act
        $executeCommand->run("echo hello");
        $stdout = $executeCommand->getSTDOUT();

        # Assert
        $this->assertEquals('hello', $stdout);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecuteCommand::__construct()
     * @covers \Fie\Automation\Shell\ExecuteCommand::run()
     * @covers \Fie\Automation\Shell\ExecuteCommand::getSTDERR()
     */
    public function testGetSTDERR() {
        # Arrange
        $logger = (new Logger())->silent();
        $executeCommand = new ExecuteCommand($logger);

        # Act
        $executeCommand->run("perl -e 'print STDERR \"error\"'");
        $stderr = $executeCommand->getSTDERR();

        # Assert
        $this->assertEquals('error', $stderr);
    }
}
