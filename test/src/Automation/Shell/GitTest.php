<?php

namespace Fie\Test\Automation\Shell;

use Fie\Automation\ServiceUtils\BuildConfig;
use Fie\Automation\ServiceUtils\Logger;
use Fie\Automation\ServiceUtils\Logger\LogLevel;
use Fie\Automation\Shell\Git;
use PHPUnit\Framework\TestCase;

final class GitTest extends TestCase
{
    private $buildConfig;

    /**
     * @param null|string $name
     * @param array $data
     * @param string $dataName
     * @throws \Exception
     */
    public function __construct(?string $name = null, array $data = [], string $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->buildConfig = new BuildConfig();
    }

    /**
     * @covers \Fie\Automation\Shell\Git::__construct()
     * @covers \Fie\Automation\Shell\Git::getRepositoryRoot()
     * @covers \Fie\Automation\Shell\Git::__destruct()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testInit() {
        # Arrange
        $logger = (new Logger())->silent();

        # Act
        $git = new Git($logger, $this->buildConfig);

        # Assert
        $this->assertInstanceOf(Git::class, $git);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::setExecuteCommandObjects()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testGetExecuteCommand() {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class() {
            public function run() {
                return [0, []];
            }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects(['bogus', $fauxExecuteCommand]);

        # Act
        $git->removeOldArtifacts();

        # Assert
        $this->assertInstanceOf(Git::class, $git);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::__construct()
     * @covers \Fie\Automation\Shell\Git::getTarVerboseFlag()
     * @dataProvider dataGetTarVerboseFlag
     * @param $level
     * @param $expected
     * @throws \Exception
     */
    public function testGetTarVerboseFlag($level, $expected) {
        # Arrange
        $logger = new Logger();
        $logger->level($level);
        $git = new Git($logger, $this->buildConfig);

        # Act
        $tarVerboseFlag = $git->getTarVerboseFlag();

        # Assert
        $this->assertSame($expected, $tarVerboseFlag);
    }
    public function dataGetTarVerboseFlag() {
        return [
            'verbose'     => [LogLevel::INFO, 'v'],
            'not verbose' => [LogLevel::PRINT, ''],
        ];
    }

    /**
     * @covers \Fie\Automation\Shell\Git::getOptions()
     */
    public function testGetOptions() {
        # Arrange/Act
        $gitOptions = Git::getOptions();

        # Assert
        $this->assertInternalType('array', $gitOptions);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @throws \Exception
     */
    public function testEnsureRepositoryInitialized() {
        # Assert (expect)
        $this->expectExceptionMessage('Repository not initialized.');

        # Arrange
        $logger = (new Logger())->silent();
        $git = new Git($logger, $this->buildConfig);

        # Act
        $git->branchExists('something');
    }

    /**
     * @covers \Fie\Automation\Shell\Git::getArtifactName()
     * @covers \Fie\Automation\Shell\Git::getArtifactPrefix()
     * @dataProvider dataGetArtifactName
     * @param string $repositoryName
     * @param string $resourceName
     * @param string $expected
     * @throws \Exception
     */
    public function testGetArtifactName($repositoryName, $resourceName, $expected) {
        # Arrange
        $logger = (new Logger())->silent();
        $git = new Git($logger, $this->buildConfig);

        # Act
        $artifactName = $git->getArtifactName($repositoryName, $resourceName);

        # Assert
        $expected = $git->getArtifactPrefix() . $expected . '.tar.gz';
        $this->assertSame($expected, $artifactName);
    }
    public function dataGetArtifactName() {
        return [
            ['blah', 'blah', 'blah_blah'],
            ['SomeThing', 'SomeThing', 'something_something'],
            ['Some--Prefix', 'Some Suffix', 'some-prefix_some-suffix'],
        ];
    }

    /**
     * @covers \Fie\Automation\Shell\Git::getArtifactDirectory()
     * @throws \Exception
     */
    public function testGetArtifactDirectory() {
        # Arrange
        $logger = (new Logger())->silent();
        $git = new Git($logger, $this->buildConfig);

        # Act
        $artifactDirectory = $git->getArtifactDirectory();

        # Assert
        $this->assertSame('/tmp', $artifactDirectory);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::removeOldArtifacts()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testRemoveOldArtifacts() {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class() {
            public function run() {
                return [0, []];
            }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $result = $git->removeOldArtifacts();

        # Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::branchExists()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @dataProvider dataBranchExists
     * @param int $exitStatus
     * @param bool $branchExists
     * @throws \Exception
     */
    public function testBranchExists($exitStatus, $branchExists) {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class($exitStatus) {
            private $exitStatus;
            public function __construct($exitStatus) {
                $this->exitStatus = $exitStatus;
            }
            public function run() { return $this; }
            public function getExitStatus() { return $this->exitStatus; }

        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $result = $git->branchExists('something');

        # Assert
        $this->assertEquals($branchExists, $result);
    }
    public function dataBranchExists() {
        return [
            'exists'         => [0, true],
            'does not exist' => [1, false],
        ];
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::tagExists()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @dataProvider dataTagExists
     * @param int $exitStatus
     * @param bool $tagExists
     * @throws \Exception
     */
    public function testTagExists($exitStatus, $tagExists) {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class($exitStatus) {
            private $exitStatus;
            public function __construct($exitStatus) {
                $this->exitStatus = $exitStatus;
            }
            public function run() { return $this; }
            public function getExitStatus() { return $this->exitStatus; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $result = $git->tagExists('something');

        # Assert
        $this->assertEquals($tagExists, $result);
    }
    public function dataTagExists() {
        return [
            'exists'         => [0, true],
            'does not exist' => [1, false],
        ];
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::resourceExists()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testResourceExistsBranch() {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommandBranch = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommandBranch]);

        # Act
        $result = $git->resourceExists('something');

        # Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::resourceExists()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testResourceExistsTag() {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommandBranch = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 1; }
        };
        $fauxExecuteCommandTag = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommandBranch, $fauxExecuteCommandTag]);

        # Act
        $result = $git->resourceExists('something');

        # Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::clone()
     * @covers \Fie\Automation\Shell\Git::getRepositoryRoot()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testClone() {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $result = $git->clone('express');
        $repositoryRoot = $git->getRepositoryRoot();

        # Assert
        $this->assertTrue($result);
        $this->assertRegExp('!^/tmp/automation_repo_!', $repositoryRoot);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::clone()
     * @covers \Fie\Automation\Shell\Git::getRepositoryRoot()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testCloneTwice() {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $git->clone('express');
        $result = $git->clone('express');
        $repositoryRoot = $git->getRepositoryRoot();

        # Assert
        $this->assertTrue($result);
        $this->assertRegExp('!^/tmp/automation_repo_!', $repositoryRoot);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::clone()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @covers \Fie\Automation\Shell\Git::getRepositoryDirectory()
     * @covers \Fie\Automation\Shell\Git::getRepositoryPrefix()
     * @covers \Fie\Automation\Shell\Git::getClonePrefix()
     * @dataProvider dataCloneFailure
     * @param $repositoryName
     * @param $exceptionMessage
     * @throws \Exception
     */
    public function testCloneFailure($repositoryName, $exceptionMessage) {
        # Assert (expect)
        $this->expectExceptionMessageRegExp('/^'.$exceptionMessage.'/');

        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 1; }
            public function getSTDERR() { return 'test'; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $git->clone($repositoryName);
    }
    public function dataCloneFailure() {
        return [
            'er-devops'    => ['er-devops', 'DANGER: requested repository is devops'],
            'empty string' => ['',          "Could not clone repository ''"],
            'failed clone' => ['bogus',     "Could not clone repository 'bogus'"],
        ];
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::getNextTag()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testGetNextTag() {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
            public function getSTDOUT() { return 'v2.345.600'; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $result = $git->getNextTag('v2');

        # Assert
        $this->assertSame('v2.345.601', $result);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::getNextTag()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testGetNextTagFailure() {
        # Assert (expect)
        $this->expectExceptionMessage('Could not determine next tag.');

        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
            public function getSTDOUT() { return null; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $git->getNextTag('v2');
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::mergePrepare()
     * @covers \Fie\Automation\Shell\Git::merge()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testMerge() {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommandPrep = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
        };
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommandPrep, $fauxExecuteCommand]);

        # Act
        $result = $git->merge('someBranch');

        # Assert
        $this->assertInstanceOf(Git::class, $result);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::mergePrepare()
     * @covers \Fie\Automation\Shell\Git::merge()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testMergePrepareFailure() {
        # Assert (expect)
        $this->expectExceptionMessageRegExp('/^Could not prepare for git merge/');

        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommandPrep = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 1; }
            public function getSTDERR() { return 'something went wrong'; }
        };
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommandPrep, $fauxExecuteCommand]);

        # Act
        $git->merge('someBranch');
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::mergePrepare()
     * @covers \Fie\Automation\Shell\Git::merge()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testMergeFailure() {
        # Assert (expect)
        $this->expectExceptionMessageRegExp('/^Could not git merge/');

        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommandPrep = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
        };
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 1; }
            public function getSTDERR() { return 'something went wrong'; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommandPrep, $fauxExecuteCommand]);

        # Act
        $git->merge('someBranch');
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::tag()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testTag() {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $result = $git->tag('someTag');

        # Assert
        $this->assertInstanceOf(Git::class, $result);
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::tag()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @throws \Exception
     */
    public function testTagFailure() {
        # Assert (expect)
        $this->expectExceptionMessageRegExp('/^Could not git tag/');

        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class() {
            public function run() { return $this; }
            public function getExitStatus() { return 1; }
            public function getSTDERR() { return 'something went wrong'; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $git->tag('someTag');
    }

    /**
     * @covers \Fie\Automation\Shell\Git::ensureRepositoryInitialized()
     * @covers \Fie\Automation\Shell\Git::hasCodeChanges()
     * @covers \Fie\Automation\Shell\Git::getExecuteCommand()
     * @dataProvider dataHasCodeChanges
     * @param $numFilesChanged
     * @param $expected
     * @throws \Exception
     */
    public function testHasCodeChanges($numFilesChanged, $expected) {
        # Arrange
        $logger = (new Logger())->silent();
        $fauxExecuteCommand = new class($numFilesChanged) {
            private $numFieldsChanged;
            public function __construct($numFilesChanged) {
                $this->numFieldsChanged =$numFilesChanged;
            }
            public function run() { return $this; }
            public function getExitStatus() { return 0; }
            public function getSTDOUT() { return $this->numFieldsChanged; }
        };
        $git = (new Git($logger, $this->buildConfig))
            ->setExecuteCommandObjects([$fauxExecuteCommand]);

        # Act
        $result = $git->hasCodeChanges('someBranch', 'someDestinationBranch');

        # Assert
        $this->assertEquals($expected, $result);
    }
    public function dataHasCodeChanges() {
        return [
            'no changes'             => ['      0',  false],
            'one file changed'       => ['      1',  true],
            'multiple files changed' => ['      12', true],
        ];
    }
}
