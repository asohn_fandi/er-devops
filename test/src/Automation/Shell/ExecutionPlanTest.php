<?php

namespace Fie\Test\Automation\Shell;

use Exception;
use Fie\Automation\Service;
use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\Logger;
use Fie\Automation\Shell\ExecutionPlan;
use PHPUnit\Framework\TestCase;

final class ExecutionPlanTest extends TestCase
{
    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executePlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executeStep()
     * @covers \Fie\Automation\Shell\ExecutionPlan::terminate()
     * @throws \Exception
     */
    public function testRunSuccess() {
        # Arrange
        $logger = (new Logger())->silent();
        $executionPlan = new class($logger) extends ExecutionPlan {
            protected function onSuccess(): void {}
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            protected function getExecutionPlan(): array {
                return [
                    ['command' => 'echo "hi mom"'],
                    ['command' => 'echo "hi dad"'],
                ];
            }
        };
        $service = new Service($logger, $executionPlan);

        # Act
        $result = $service->run();

        # Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executePlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executeStep()
     * @covers \Fie\Automation\Shell\ExecutionPlan::describeRollbackPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executeRollbackPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::terminate()
     * @throws \Exception
     */
    public function testRunFailure() {
        # Arrange
        $logger = (new Logger())->silent();
        $executionPlan = new class($logger) extends ExecutionPlan {
            protected function onSuccess(): void {}
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            protected function getExecutionPlan(): array {
                return [
                    # valid structure but bad shell command
                    ['command' => 'bogus'],
                ];
            }
        };
        $service = new Service($logger, $executionPlan);

        # Act
        $result = $service->run();

        # Assert
        $this->assertFalse($result);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::terminate()
     * @throws \Exception
     */
    public function testRunSuccessGetExecutionPlanThrows() {
        # Arrange
        $logger = (new Logger())->silent();
        $executionPlan = new class($logger) extends ExecutionPlan {
            protected function onSuccess(): void {}
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            protected function getExecutionPlan(): array {
                throw new Exception('something bad happened');
            }
        };
        $service = new Service($logger, $executionPlan);

        # Act
        $result = $service->run();

        # Assert
        $this->assertFalse($result);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executePlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executeStep()
     * @covers \Fie\Automation\Shell\ExecutionPlan::describeRollbackPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executeRollbackPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::terminate()
     * @throws \Exception
     */
    public function testRunFailureWithRollback() {
        # Arrange
        $logger = (new Logger())->silent();
        $executionPlan = new class($logger) extends ExecutionPlan {
            protected function onSuccess(): void {}
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            protected function getExecutionPlan(): array {
                return [
                    # valid structure but bad shell command
                    [
                        'command' => 'bogus',
                        'rollback' => 'echo "rollback"',
                    ],
                ];
            }
        };
        $service = new Service($logger, $executionPlan);

        # Act
        $result = $service->run();

        # Assert
        $this->assertFalse($result);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executePlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executeStep()
     * @covers \Fie\Automation\Shell\ExecutionPlan::describeRollbackPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executeRollbackPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::terminate()
     * @throws \Exception
     */
    public function testRunRollbackSuccess() {
        # Arrange
        $logger = (new Logger())->silent();
        $executionPlan = new class($logger) extends ExecutionPlan {
            protected function onSuccess(): void {}
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            protected function getExecutionPlan(): array {
                return [
                    [
                        'command'       => 'echo "hi mom"',
                        'rollback'      => 'echo "hi dad"',
                        'clearRollback' => true,
                    ],
                ];
            }
        };
        $service = new Service($logger, $executionPlan);

        # Act
        $result = $service->run();

        # Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executePlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executeStep()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::describeRollbackPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::executeRollbackPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::terminate()
     * @throws \Exception
     */
    public function testRunRollbackFailure() {
        # Arrange
        $logger = (new Logger())->silent();
        $executionPlan = new class($logger) extends ExecutionPlan {
            protected function onSuccess(): void {}
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            protected function getExecutionPlan(): array {
                return [
                    [
                        'command'  => 'bogus',
                        'rollback' => ['echo "hi"', 'bogus'],
                    ],
                ];
            }
        };
        $service = new Service($logger, $executionPlan);

        # Act
        $result = $service->run();

        # Assert
        $this->assertFalse($result);
    }
}
