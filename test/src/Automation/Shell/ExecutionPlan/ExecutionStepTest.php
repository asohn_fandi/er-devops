<?php

namespace Fie\Test\Automation\Shell\ExecutionPlan;

use Fie\Automation\Shell\ExecutionPlan\ExecutionStep;
use PHPUnit\Framework\TestCase;

final class ExecutionStepTest extends TestCase
{
    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::__construct()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::getCommand()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::getRollback()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::getClearRollback()
     * @throws \Exception
     */
    public function testConstructValid() {
        # Arrange
        $executionStep = new ExecutionStep([
            'command'       => 'echo "command"',
            'rollback'      => ['echo "rollback1"', 'echo "rollback2"'],
            'clearRollback' => true,
        ]);

        # Act
        $command = $executionStep->getCommand();
        $rollback = $executionStep->getRollback();
        $clearRollback = $executionStep->getClearRollback();

        # Assert
        $this->assertSame('echo "command"', $command);
        $this->assertSame(['echo "rollback1"', 'echo "rollback2"'], $rollback);
        $this->assertTrue($clearRollback);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::fromArray()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::__construct()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::getCommand()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::getRollback()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::getClearRollback()
     * @throws \Exception
     */
    public function testFromArray() {
        # Arrange
        $executionStep = ExecutionStep::fromArray([
            'command'       => 'echo "command"',
            'rollback'      => ['echo "rollback1"', 'echo "rollback2"'],
            'clearRollback' => true,
        ]);

        # Act
        $command = $executionStep->getCommand();
        $rollback = $executionStep->getRollback();
        $clearRollback = $executionStep->getClearRollback();

        # Assert
        $this->assertSame('echo "command"', $command);
        $this->assertSame(['echo "rollback1"', 'echo "rollback2"'], $rollback);
        $this->assertTrue($clearRollback);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::__construct()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::getRollback()
     * @throws \Exception
     */
    public function testConstructValidSingleRollback() {
        # Arrange/Act
        $executionStep = new ExecutionStep([
            'command'       => 'echo "command"',
            'rollback'      => 'echo "rollback1"',
            'clearRollback' => true,
        ]);

        # Act
        $rollback = $executionStep->getRollback();

        # Assert
        $this->assertSame(['echo "rollback1"'], $rollback);
    }

    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::__construct()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::validateStructure()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::validateCommand()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::validateRollback()
     * @dataProvider dataValidateStepThrows
     * @param array $step
     * @param string $exceptionText
     * @throws \Exception
     */
    public function testValidateStepThrows($step, $exceptionText) {
        # Assert (expect)
        $this->expectExceptionMessageRegExp($exceptionText);

        # Arrange/Act
        new ExecutionStep($step);
    }
    public function dataValidateStepThrows() {
        return [
            'empty string command' => [
                ['command' => ''],
                "/^No command to execute$/i",
            ],
            'not string command' => [
                ['command' => 123],
                "/^Malformed command$/i",
            ],
            'null rollback command' => [
                ['command' => 'echo "command"', 'rollback' => [null]],
                "/^No command to execute for rollback/i",
            ],
            'empty string rollback command' => [
                ['command' => 'echo "command"', 'rollback' => ['']],
                "/^No command to execute for rollback/i",
            ],
            'not string rollback command' => [
                ['command' => 'echo "command"', 'rollback' => [123]],
                "/^Malformed command for rollback/i",
            ],
        ];
    }

    /**
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::__construct()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::validateStructure()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::validateCommand()
     * @covers \Fie\Automation\Shell\ExecutionPlan\ExecutionStep::validateRollback()
     * @dataProvider dataValidateStepValid
     * @param array $step
     * @throws \Exception
     */
    public function testValidateStepValid($step) {
        # Arrange/Act
        $executionStep = new ExecutionStep($step);

        # Assert
        $this->assertInstanceOf(ExecutionStep::class, $executionStep);
    }
    public function dataValidateStepValid() {
        return [
            'valid command' => [
                ['command' => 'echo "command"'],
            ],
            'valid rollback command string' => [
                ['command' => 'echo "command"', 'rollback' => 'echo "rollback1"'],
            ],
            'valid rollback command array(1)' => [
                ['command' => 'echo "command"', 'rollback' => ['echo "rollback1"']],
            ],
            'valid rollback command array(2)' => [
                ['command' => 'echo "command"', 'rollback' => ['echo "rollback1"', 'echo "rollback2"']],
            ],
            'valid command with cleared rollback' => [
                ['command' => 'echo "command"', 'clearRollback' => true],
            ],
        ];
    }
}
