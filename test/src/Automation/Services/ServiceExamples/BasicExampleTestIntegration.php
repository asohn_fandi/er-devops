<?php

namespace Fie\Test\Automation\Services\ServiceExamples;

use Fie\Automation\Service;
use Fie\Automation\Services\ServiceExamples\BasicExample;
use Fie\Automation\ServiceUtils\Logger;
use PHPUnit\Framework\TestCase;

class BasicExampleTestIntegration extends TestCase
{
    /**
     * @param $name
     * @return \ReflectionMethod
     * @throws \ReflectionException
     */
    private static function getMethod($name) {
        $class = new \ReflectionClass('Fie\Automation\Services\ServiceExamples\BasicExample');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    /**
     * @covers \Fie\Automation\Service::isPhpUnit()
     * @covers \Fie\Automation\Service::run()
     * @covers \Fie\Automation\Services\ServiceExamples\BasicExample::getServiceOptions()
     * @covers \Fie\Automation\Services\ServiceExamples\BasicExample::help()
     * @covers \Fie\Automation\Services\ServiceExamples\BasicExample::configure()
     * @covers \Fie\Automation\Services\ServiceExamples\BasicExample::run()
     * @throws \Exception
     */
    public function testIntegration() {
        # Arrange
        $GLOBALS['argv'] = ['--do'];
        $getOptions = self::getMethod('getServiceOptions');
        $help = self::getMethod('help');
        $logger = (new Logger())->silent();

        # Act
        $basicExample = new BasicExample($logger);
        $service = new Service($logger, $basicExample);
        $result = $service->run();
        $options = $getOptions->invoke($basicExample);
        $helpText = $help->invoke($basicExample);

        # Assert
        $this->assertInternalType('array', $options);
        $this->assertInternalType('string', $helpText);
        $this->assertTrue($result);
    }
}
