<?php

namespace Fie\Test\Automation\Services\ServiceExamples;

use Fie\Automation\Service;
use Fie\Automation\Services\ServiceExamples\RollbackExample;
use Fie\Automation\ServiceUtils\DataTracker;
use Fie\Automation\ServiceUtils\Logger;
use PHPUnit\Framework\TestCase;

class RollbackExampleTestIntegration extends TestCase
{
    /**
     * @covers \Fie\Automation\Service::run()
     * @covers \Fie\Automation\ServiceUtils\Options::getOptionBoolean()
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::getServiceOptions()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::help()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::configure()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::run()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::setBuildConfig()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::setLogger()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::getExecutionPlan()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::onSuccess()
     * @covers \Fie\Automation\ServiceBase::exitSuccess()
     * @throws \Exception
     */
    public function testIntegration() {
        # Arrange
        $GLOBALS['argv'] = [
            "--do",
            "--testEmptyPlan=false",
            "--testEmptyStep=false",
            "--testInvalidCommand=false",
            "--testMalformedCommand=false",
            "--testRollback=false",
            "--testRollbackFailure=false",
        ];
        $logger = (new Logger())->setDataTracker(new DataTracker('application/json'));

        # Act
        $rollbackExample = new RollbackExample($logger);
        $service = new Service($logger, $rollbackExample);
        ob_start();
        $result = $service->run();
        ob_end_clean();

        # assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\Service::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::getServiceOptions()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::help()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::configure()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::run()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::getExecutionPlan()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::onFailure()
     * @covers \Fie\Automation\ServiceBase::exitFailure()
     * @dataProvider dataIntegrationFailure
     * @param string $testEmptyPlan
     * @param string $testEmptyStep
     * @param string $testInvalidCommand
     * @param string $testMalformedCommand
     * @param string $testRollback
     * @param string $testRollbackFailure
     * @throws \Exception
     */
    public function testIntegrationFailure($testEmptyPlan, $testEmptyStep, $testInvalidCommand, $testMalformedCommand, $testRollback, $testRollbackFailure) {
        # Arrange
        $GLOBALS['argv'] = [
            "--do",
            "--testEmptyPlan=$testEmptyPlan",
            "--testEmptyStep=$testEmptyStep",
            "--testInvalidCommand=$testInvalidCommand",
            "--testMalformedCommand=$testMalformedCommand",
            "--testRollback=$testRollback",
            "--testRollbackFailure=$testRollbackFailure",
        ];
        $logger = (new Logger())->setDataTracker(new DataTracker('application/json'));

        # Act
        $rollbackExample = new RollbackExample($logger);
        $service = new Service($logger, $rollbackExample);
        ob_start();
        $result = $service->run();
        ob_end_clean();

        # Assert
        $this->assertFalse($result);
    }
    public function dataIntegrationFailure() {
        return [
            'testEmptyStep'        => ['false', 'true',  'false', 'false', 'false', 'false'],
            'testInvalidCommand'   => ['false', 'false', 'true',  'false', 'false', 'false'],
            'testMalformedCommand' => ['false', 'false', 'false', 'true',  'false', 'false'],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::getServiceOptions()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::help()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::configure()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::run()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::getExecutionPlan()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::onFailure()
     * @covers \Fie\Automation\ServiceBase::exitFailure()
     * @dataProvider dataIntegrationFailureException
     * @param string $testEmptyPlan
     * @param string $testEmptyStep
     * @param string $testInvalidCommand
     * @param string $testMalformedCommand
     * @param string $testRollback
     * @param string $testRollbackFailure
     * @throws \Exception
     */
    public function testIntegrationFailureException($testEmptyPlan, $testEmptyStep, $testInvalidCommand, $testMalformedCommand, $testRollback, $testRollbackFailure) {
        # Arrange
        $GLOBALS['argv'] = [
            "--do",
            "--testEmptyPlan=$testEmptyPlan",
            "--testEmptyStep=$testEmptyStep",
            "--testInvalidCommand=$testInvalidCommand",
            "--testMalformedCommand=$testMalformedCommand",
            "--testRollback=$testRollback",
            "--testRollbackFailure=$testRollbackFailure",
        ];
        $logger = (new Logger())->setDataTracker(new DataTracker('application/json'));

        # Act
        $rollbackExample = new RollbackExample($logger);
        $service = new Service($logger, $rollbackExample);
        ob_start();
        $result = $service->run();
        ob_end_clean();

        # Assert
        $this->assertFalse($result);
    }
    public function dataIntegrationFailureException() {
        return [
            'testEmptyPlan'       => ['true',  'false', 'false', 'false', 'false', 'false'],
            'testRollback'        => ['false', 'false', 'false', 'false', 'true',  'false'],
            'testRollbackFailure' => ['false', 'false', 'false', 'false', 'false', 'true'],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::setDataType()
     * @covers \Fie\Automation\Service::run()
     * @covers \Fie\Automation\Service::setup()
     * @covers \Fie\Automation\Shell\ExecutionPlan::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Shell\ExecutionPlan::setup()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::run()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::setupBaseClass()
     * @covers \Fie\Automation\Services\ServiceExamples\RollbackExample::help()
     * @covers \Fie\Automation\ServiceUtils\Logger::printData()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::print()
     * @throws \Exception
     */
    public function testIntegrationHelp() {
        # Arrange
        $GLOBALS['argv'] = ['--help'];
        $logger = (new Logger())->setDataTracker(new DataTracker('application/json'));

        # Act
        $rollbackExample = new RollbackExample($logger);
        $service = (new Service($logger, $rollbackExample))
            ->setDataType('application/json');
        ob_start();
        $result = $service->run();
        ob_end_clean();

        # assert
        $this->assertTrue($result);
    }
}
