<?php

namespace Fie\Test\Automation\Services\ServiceExamples;

use Fie\Automation\Service;
use Fie\Automation\Services\ServiceExamples\ExecutionPlanExample;
use Fie\Automation\ServiceUtils\Logger;
use PHPUnit\Framework\TestCase;

class ExecutionPlanExampleTestIntegration extends TestCase
{
    /**
     * @param $name
     * @return \ReflectionMethod
     * @throws \ReflectionException
     */
    private static function getMethod($name) {
        $class = new \ReflectionClass('Fie\Automation\Services\ServiceExamples\ExecutionPlanExample');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }

    /**
     * @covers \Fie\Automation\Service::run()
     * @covers \Fie\Automation\Shell\ExecutionPlan::validateExecutionPlan
     * @covers \Fie\Automation\Shell\ExecutionPlan::runExecutionPlan()
     * @covers \Fie\Automation\Services\ServiceExamples\ExecutionPlanExample::getServiceOptions()
     * @covers \Fie\Automation\Services\ServiceExamples\ExecutionPlanExample::help()
     * @covers \Fie\Automation\Services\ServiceExamples\ExecutionPlanExample::configure()
     * @covers \Fie\Automation\Services\ServiceExamples\ExecutionPlanExample::getExecutionPlan()
     * @covers \Fie\Automation\Services\ServiceExamples\ExecutionPlanExample::run()
     * @throws \Exception
     */
    public function testIntegration() {
        # Arrange
        $GLOBALS['argv'] = ['--do'];
        $getOptions = self::getMethod('getServiceOptions');
        $help = self::getMethod('help');
        $logger = (new Logger())->silent();

        # Act
        $basicExample = new ExecutionPlanExample($logger);
        $service = new Service($logger, $basicExample);
        $result = $service->run();
        $options = $getOptions->invoke($basicExample);
        $helpText = $help->invoke($basicExample);

        # Assert
        $this->assertInternalType('array', $options);
        $this->assertInternalType('string', $helpText);
        $this->assertTrue($result);
    }
}
