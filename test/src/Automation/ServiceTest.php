<?php

namespace Fie\Test\Automation;

use Fie\Automation\Service;
use Fie\Automation\ServiceUtils\DataTracker;
use Fie\Automation\ServiceUtils\Logger;
use PHPUnit\Framework\TestCase;

final class ServiceTest extends TestCase
{
    /**
     * @covers \Fie\Automation\Service::isWeb()
     * @covers \Fie\Automation\Service::isApi()
     * @dataProvider dataIsApi
     * @param $expected
     * @param $uri
     * @throws \Exception
     */
    public function testIsApi($expected, $uri) {
        # Arrange
        $_SERVER['HTTP_USER_AGENT'] = 'something';
        $_SERVER['REQUEST_URI'] = $uri;

        # Act
        $isApi = Service::isApi();

        # Assert
        $this->assertEquals($expected, $isApi);
    }
    public function dataIsApi() {
        return [
            'empty uri'        => [false, ''],
            'help uri'         => [false, '/help'],
            'api uri'          => [true,  '/api'],
            'api uri and more' => [true,  '/api?something'],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::isWeb()
     * @covers \Fie\Automation\Service::isApi()
     * @throws \Exception
     */
    public function testIsApiWithoutWeb() {
        # Arrange/Act
        $isApi = Service::isApi();

        # Assert
        $this->assertFalse($isApi);
    }

    /**
     * @covers \Fie\Automation\Service::isWeb()
     * @dataProvider dataIsWeb
     * @param bool|string $expected
     * @param bool|string $httpUserAgent
     */
    public function testIsWeb($expected, $httpUserAgent) {
        # Arrange
        $_SERVER['HTTP_USER_AGENT'] = $httpUserAgent;

        # Act
        $result = Service::isWeb();

        # Assert
        $this->assertEquals($expected, $result);
    }
    public function dataIsWeb() {
        return [
            'user agent is set'     => [true,  'something'],
            'user agent is not set' => [false, null],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::setServiceClassName()
     * @covers \Fie\Automation\Service::getServiceClassName()
     * @dataProvider dataSetServiceClassNameURI
     * @param $uri
     * @param $expected
     * @throws \Exception
     */
    public function testSetServiceClassNameURI($uri, $expected) {
        # Arrange
        $_SERVER['REQUEST_URI'] = $uri;
        $service = new Service(null, new SimpleService());

        # Act
        $serviceName = $service
            ->setServiceClassName()
            ->getServiceClassName();

        # Assert
        $this->assertSame($expected, $serviceName);
    }
    public function dataSetServiceClassNameURI() {
        return [
            'URI index'     => ['index',                     'index'],
            'URI index.php' => ['index.php',                 'index'],
            'URI given'     => ['ServiceExamples/Something', 'ServiceExamples\\Something'],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::setServiceClassName()
     * @covers \Fie\Automation\Service::getServiceClassName()
     * @dataProvider dataSetServiceClassNameARGV
     * @param $argv1
     * @param $expected
     * @throws \Exception
     */
    public function testSetServiceClassNameARGV($argv1, $expected) {
        # Arrange
        $GLOBALS['argv'] = ['script_name', $argv1];
        $service = new Service(null, new SimpleService());

        # Act
        $serviceName = $service
            ->setServiceClassName()
            ->getServiceClassName();

        # Assert
        $this->assertSame($expected, $serviceName);
    }
    public function dataSetServiceClassNameARGV() {
        return [
            'argv1 --help'     => ['--help',                    'help'],
            'argv1 index'      => ['index',                     'index'],
            'argv1 index.php'  => ['index.php',                 'index'],
            'argv1 given'      => ['ServiceExamples/Something', 'ServiceExamples\\Something'],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::setServiceClassName()
     * @covers \Fie\Automation\Service::getServiceClassName()
     * @dataProvider dataSetServiceClassNameBoth
     * @param $uri
     * @param $argv1
     * @param $expected
     * @throws \Exception
     */
    public function testSetServiceClassNameBoth($uri, $argv1, $expected) {
        # Arrange
        $_SERVER['REQUEST_URI'] = $uri;
        $GLOBALS['argv'] = ['script_name', $argv1];
        $service = new Service(null, new SimpleService());

        # Act
        $serviceName = $service
            ->setServiceClassName()
            ->getServiceClassName();

        # Assert
        $this->assertSame($expected, $serviceName);
    }
    public function dataSetServiceClassNameBoth() {
        return [
            'URI given and argv1 given' => ['ServiceExamples/Something', 'ServiceExamples/SomethingElse', 'ServiceExamples\\Something'],
            'prefix removed'            => ['src/Automation/Services/ServiceExamples/Something', null, 'ServiceExamples\\Something'],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::setServiceClassName()
     * @covers \Fie\Automation\Service::getServiceClassName()
     * @throws \Exception
     */
    public function testSetServiceClassNameNothing() {
        # Arrange
        $_SERVER['REQUEST_URI'] = null;
        $GLOBALS['argv'] = ['script_name'];
        $service = new Service(null, new SimpleService());

        # Act
        $serviceName = $service
            ->setServiceClassName()
            ->getServiceClassName();

        # Assert
        $this->assertSame('', $serviceName);
    }

    /**
     * @covers \Fie\Automation\Service::__construct()
     * @covers \Fie\Automation\Service::setServiceFinder()
     * @covers \Fie\Automation\Service::run()
     * @covers \Fie\Automation\Service::help()
     * @covers \Fie\Automation\Service::listServices()
     * @covers \Fie\Automation\Service::__destruct()
     * @dataProvider dataRunUnknownService
     * @param bool $override
     * @throws \Exception
     */
    public function testRunUnknownService($override) {
        # Arrange
        $_SERVER['REQUEST_URI'] = 'bogus';
        $logger = (new Logger())
            ->setDataTracker(new DataTracker('application/json'))
            ->setWebOverride($override);
        $service = (new Service($logger))
            ->setServiceFinder(new class() {
                public function find() { return []; }
            });

        # Act
        ob_start();
        $returnStatus = $service->run();
        ob_end_clean();

        # Assert
        $this->assertFalse($returnStatus);
    }
    public function dataRunUnknownService() {
        return [
            'web' => [true],
            'cli' => [false],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::setServiceFinder()
     * @covers \Fie\Automation\Service::setup()
     * @covers \Fie\Automation\Service::help()
     * @dataProvider dataHelp
     * @param $uri
     * @throws \Exception
     */
    public function testHelpWeb($uri) {
        # Arrange
        $_SERVER['HTTP_USER_AGENT'] = 'something';
        $_SERVER['REQUEST_URI'] = $uri;
        $logger = (new Logger())->silent();
        $service = (new Service($logger))
            ->setServiceFinder(new class() {
                public function find() { return []; }
            });

        # Act
        $returnStatus = $service->run();

        # Assert
        $this->assertFalse($returnStatus);
    }
    public function dataHelp() {
        return [
            'empty URI'        => [''],
            'help'             => ['help'],
            'help with prefix' => ['--help'],
            'help via api'     => ['/api'],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::setServiceFinder()
     * @covers \Fie\Automation\Service::setup()
     * @covers \Fie\Automation\Service::help()
     * @throws \Exception
     */
    public function testHelpApi() {
        # Arrange
        $_SERVER['REQUEST_URI'] = '/api';
        $logger = (new Logger())
            ->setWebOverride(true)
            ->silent();
        $service = (new Service($logger))
            ->setServiceFinder(new class() {
                public function find() { return []; }
            });

        # Act
        ob_start();
        $returnStatus = $service->run();
        ob_end_clean();

        # Assert
        $this->assertFalse($returnStatus);
    }

    /**
     * @covers \Fie\Automation\Service::setServiceFinder()
     * @covers \Fie\Automation\Service::setup()
     * @covers \Fie\Automation\Service::help()
     * @throws \Exception
     */
    public function testHelpOption() {
        # Arrange
        $GLOBALS['argv'] = ['--help'];
        $logger = (new Logger())->silent();
        $service = (new Service($logger))
            ->setServiceFinder(new class() {
                public function find() { return []; }
            });

        # Act
        $returnStatus = $service->run();

        # Assert
        $this->assertFalse($returnStatus);
    }

    /**
     * @covers \Fie\Automation\Service::getServiceClassName()
     * @covers \Fie\Automation\Service::setServiceClassName()
     * @throws \Exception
     */
    public function testGetServiceClassName() {
        # Arrange
        $service = new Service(null, new SimpleService());
        ob_start();
        $service->run();
        ob_end_clean();

        # Act
        $serviceClassName = $service->getServiceClassName();

        # Assert
        $this->assertEquals('', $serviceClassName);
    }

    /**
     * @covers \Fie\Automation\Service::__construct()
     * @covers \Fie\Automation\Service::listServices()
     * @dataProvider dataListServices
     * @param bool $serviceList
     * @throws \Exception
     */
    public function testListServices($serviceList) {
        # Arrange
        $GLOBALS['argv'] = ['--help'];
        $logger = (new Logger())->silent();
        $service = (new Service($logger))
            ->setServiceFinder(new class($serviceList) {
                private $serviceList;
                public function __construct($serviceList) { $this->serviceList = $serviceList; }
                public function find() { return $this->serviceList; }
            });

        # Act
        $returnStatus = $service->run();

        # Assert
        $this->assertFalse($returnStatus);
    }
    public function dataListServices() {
        return [
            'found services'        => [["a", "b", "c"]],
            'did not find services' => [[]],
        ];
    }

    /**
     * @covers \Fie\Automation\Service::__construct()
     * @covers \Fie\Automation\ServiceBase::setOptions()
     * @throws \Exception
     */
    public function testInitWithOptions() {
        # Arrange/Act
        $logger = (new Logger())->silent();
        $service = new Service($logger, new SimpleService(), ['help' => '']);

        # Assert
        $this->assertInstanceOf(Service::class, $service);
    }
}
