<?php

namespace Fie\Test\Automation;

use Exception;
use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\BuildConfig;
use Fie\Automation\ServiceUtils\Logger;
use Fie\Automation\ServiceUtils\ServiceException;
use PHPUnit\Framework\TestCase;

final class ServiceBaseTest extends TestCase
{
    /**
     * @covers \Fie\Automation\ServiceBase::__construct()
     * @covers \Fie\Automation\ServiceBase::setLogger()
     * @covers \Fie\Automation\ServiceBase::configureAndRun()
     * @covers \Fie\Automation\ServiceBase::setupBaseClass()
     * @covers \Fie\Automation\ServiceBase::getBuildConfig()
     * @covers \Fie\Automation\ServiceBase::getLogger()
     * @covers \Fie\Automation\ServiceBase::__destruct()
     * @throws \Exception
     */
    public function testInit() {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = (new SimpleService())
            ->setLogger($logger);

        # Act
        $result      = $simpleService->configureAndRun();
        $buildConfig = $simpleService->getBuildConfig();
        $logger      = $simpleService->getLogger();

        # Assert
        $this->assertTrue($result);
        $this->assertInstanceOf(ServiceBase::class, $simpleService);
        $this->assertInstanceOf(BuildConfig::class, $buildConfig);
        $this->assertInstanceOf(Logger::class, $logger);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::configureAndRun()
     * @throws \Exception
     */
    public function testConfigureAndRunIsRunning() {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = new class($logger) extends ServiceBase {
            protected function onSuccess(): void {}
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase {
                $this->configureAndRun();
                return $this;
            }
            public function run(): ServiceBase { return $this; }
        };

        # Act
        $result = $simpleService->configureAndRun();

        # Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::setupBaseClass()
     * @throws \Exception
     */
    public function testSetupBaseClassTwice() {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = new SimpleService($logger);

        # Act
        $simpleService
            ->setupBaseClass()
            ->setupBaseClass();

        # Assert
        $this->assertInstanceOf(ServiceBase::class, $simpleService);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::ensureDependency()
     * @covers \Fie\Automation\ServiceBase::exitFailure()
     * @covers \Fie\Automation\ServiceBase::terminate()
     * @covers \Fie\Automation\ServiceBase::exit()
     * @covers \Fie\Automation\ServiceBase::runEndingTriggers()
     * @throws \Exception
     */
    public function testEnsureDependencyFailure() {
        # Assert (expect)
        $this->expectExceptionMessage(1);

        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = (new SimpleService($logger))
            ->setupBaseClass();

        # Act
        $simpleService->ensureDependency('bogus');
    }

    /**
     * @covers \Fie\Automation\ServiceBase::ensureDependency()
     * @throws \Exception
     */
    public function testEnsureDependencySuccess() {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = (new SimpleService($logger))
            ->setupBaseClass();

        # Act
        $result = $simpleService->ensureDependency('sh');

        # Assert
        $this->assertInstanceOf(ServiceBase::class, $result);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::getExtendedOptions()
     * @covers \Fie\Automation\ServiceBase::getCommonOptions()
     * @throws \Exception
     */
    public function testGetExtendedOptionsNone() {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = new SimpleService($logger);

        # Act
        $simpleService->setupBaseClass();
        $knownOptionsBySection = $simpleService->getKnownOptions();

        # Assert
        $this->assertArrayNotHasKey('Simple Service', $knownOptionsBySection);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::getExtendedOptions()
     * @dataProvider dataGetExtendedOptions
     * @param bool $expected
     * @param array $ssOptions
     * @throws \Exception
     */
    public function testGetExtendedOptions($expected, $ssOptions) {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = (new SimpleService($logger))
            ->ssSetOptions(['Simple Service' => $ssOptions]);

        # Act
        $simpleService->setupBaseClass();
        $ssOptionsResult = $simpleService->getKnownOptions()['Simple Service'];
        $hasOptions = count($ssOptionsResult) > 0;

        # Assert
        $this->assertEquals($expected, $hasOptions);
    }
    public function dataGetExtendedOptions() {
        return [
            'empty options'         => [false, [] ],
            'malformed option list' => [false, ['bogus'] ],
            'int name'              => [false, [0 => 'some_desc'] ],
            'non-string desc'       => [false, ['some_name' => [] ] ],
            'proper'                => [true,  ['some_name' => 'some_desc'] ],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceBase::getExtendedOptions()
     * @throws \Exception
     */
    public function testGetExtendedOptionsNotArray() {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = (new SimpleService($logger))
            ->ssSetOptions(['Simple Service' => '']);

        # Act
        $simpleService->setupBaseClass();
        $knownOptionsBySection = $simpleService->getKnownOptions();

        # Assert
        $this->assertArrayNotHasKey('Simple Service', $knownOptionsBySection);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::getKnownOptions()
     * @throws \Exception
     */
    public function testGetOptionsKnown() {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = (new SimpleService($logger))
            ->setupBaseClass();

        # Act
        $result = $simpleService->getKnownOptions();

        # Assert
        $this->assertInternalType('array', $result);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::__construct()
     * @covers \Fie\Automation\ServiceBase::configureAndRun()
     * @covers \Fie\Automation\ServiceBase::setupBaseClass()
     * @covers \Fie\Automation\ServiceBase::getCommonOptions()
     * @covers \Fie\Automation\ServiceBase::getExtendedOptions()
     * @covers \Fie\Automation\ServiceBase::exitHelp()
     * @covers \Fie\Automation\ServiceBase::exitNoOp()
     * @covers \Fie\Automation\ServiceBase::terminate()
     * @covers \Fie\Automation\ServiceBase::runEndingTriggers()
     * @covers \Fie\Automation\ServiceBase::exit()
     * @dataProvider dataServiceHelp
     * @param array $ssOptions
     * @throws \Exception
     */
    public function testServiceHelp($ssOptions) {
        # Assert (expect)
        $this->expectExceptionMessage(0);

        # Arrange
        $GLOBALS['argv'] = ['--help'];
        $logger = (new Logger())->silent();
        $simpleService = (new SimpleService($logger))
            ->ssSetOptions(['Simple Service' => $ssOptions]);

        # Act
        $simpleService->configureAndRun();
    }
    public function dataServiceHelp() {
        return [
            'nothing'       => [ [] ],
            'empty options' => [ ['Simple Service' => []] ],
            'proper'        => [ ['Simple Service' => ['foo' => 'bar']] ],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceBase::__construct()
     * @covers \Fie\Automation\ServiceBase::configureAndRun()
     * @covers \Fie\Automation\ServiceBase::setupBaseClass()
     * @covers \Fie\Automation\ServiceBase::getCommonOptions()
     * @covers \Fie\Automation\ServiceBase::getExtendedOptions()
     * @covers \Fie\Automation\ServiceBase::exitNoOp()
     * @covers \Fie\Automation\ServiceBase::terminate()
     * @covers \Fie\Automation\ServiceBase::runEndingTriggers()
     * @covers \Fie\Automation\ServiceBase::exit()
     * @throws \Exception
     */
    public function testServiceHelpAmendedWeb() {
        # Assert (expect)
        $this->expectExceptionMessage(0);

        # Arrange
        $GLOBALS['argv'] = ['--help'];
        $_SERVER['HTTP_USER_AGENT'] = 'something';
        $logger = (new Logger())
            ->silent()
            ->setWebOverride(true);
        $simpleService = (new SimpleService($logger))
            ->ssSetOptions(['Simple Service' => ['foo' => 'bar']])
            ->ssSetHelpText('hello');

        # Act
        $simpleService->configureAndRun();
    }

    /**
     * @covers \Fie\Automation\ServiceBase::__construct()
     * @covers \Fie\Automation\ServiceBase::configureAndRun()
     * @covers \Fie\Automation\ServiceBase::setupBaseClass()
     * @covers \Fie\Automation\ServiceBase::getCommonOptions()
     * @covers \Fie\Automation\ServiceBase::getExtendedOptions()
     * @covers \Fie\Automation\ServiceBase::exitNoOp()
     * @covers \Fie\Automation\ServiceBase::terminate()
     * @covers \Fie\Automation\ServiceBase::runEndingTriggers()
     * @covers \Fie\Automation\ServiceBase::exit()
     * @throws \Exception
     */
    public function testServiceHelpAmendedWebApi() {
        # Assert (expect)
        $this->expectExceptionMessage(0);

        # Arrange
        $GLOBALS['argv'] = ['--help'];
        $_SERVER['HTTP_USER_AGENT'] = 'something';
        $_SERVER['REQUEST_URI'] = '/api';
        $logger = (new Logger())
            ->silent()
            ->setWebOverride(true);
        $simpleService = (new SimpleService($logger))
            ->ssSetOptions(['Simple Service' => ['foo' => 'bar']])
            ->ssSetHelpText('hello');

        # Act
        $simpleService->configureAndRun();
    }

    /**
     * @covers \Fie\Automation\ServiceBase::__construct()
     * @covers \Fie\Automation\ServiceBase::configureAndRun()
     * @covers \Fie\Automation\ServiceBase::setupBaseClass()
     * @covers \Fie\Automation\ServiceBase::getCommonOptions()
     * @covers \Fie\Automation\ServiceBase::getExtendedOptions()
     * @covers \Fie\Automation\ServiceBase::exitNoOp()
     * @covers \Fie\Automation\ServiceBase::terminate()
     * @covers \Fie\Automation\ServiceBase::runEndingTriggers()
     * @covers \Fie\Automation\ServiceBase::exit()
     * @throws \Exception
     */
    public function testServiceHelpAmendedCLI() {
        # Assert (expect)
        $this->expectExceptionMessage(0);

        # Arrange
        $GLOBALS['argv'] = ['--help'];
        $logger = (new Logger())
            ->silent()
            ->setWebOverride(false);
        $simpleService = (new SimpleService($logger))
            ->ssSetOptions(['Simple Service' => ['foo' => 'bar']])
            ->ssSetHelpText('hello');

        # Act
        $simpleService->configureAndRun();
    }

    /**
     * @covers \Fie\Automation\ServiceBase::__construct()
     * @covers \Fie\Automation\ServiceBase::configureAndRun()
     * @covers \Fie\Automation\ServiceBase::setupBaseClass()
     * @covers \Fie\Automation\ServiceBase::getCommonOptions()
     * @covers \Fie\Automation\ServiceBase::getExtendedOptions()
     * @covers \Fie\Automation\ServiceBase::terminate()
     * @covers \Fie\Automation\ServiceBase::runEndingTriggers()
     * @covers \Fie\Automation\ServiceBase::exit()
     * @throws \Exception
     */
    public function testServiceSuccess() {
        # Arrange
        $logger = (new Logger())
            ->silent()
            ->setWebOverride(false);
        $simpleService = (new SimpleService($logger))
            ->ssSetOptions(['Simple Service' => ['foo' => 'bar']])
            ->ssSetHelpText('hello');

        # Act
        $result = $simpleService->configureAndRun();

        # Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::setupBaseClass()
     * @covers \Fie\Automation\ServiceBase::exitHelp()
     * @throws \Exception
     */
    public function testSetupBaseClassUnknownOptions() {
        # Assert (expect)
        $this->expectExceptionMessage(0);

        # Arrange
        $GLOBALS['argv'] = ['--bogus'];
        $logger = (new Logger())->silent();
        $simpleService = new SimpleService($logger);

        # Act
        $simpleService->configureAndRun();
    }

    /**
     * @covers \Fie\Automation\ServiceBase::configureAndRun()
     * @covers \Fie\Automation\ServiceBase::exitHelp()
     * @throws \Exception
     */
    public function testConfigureAndRunOptionsException() {
        # Assert (expect)
        $this->expectExceptionMessage(0);

        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = new class($logger) extends ServiceBase {
            protected function onSuccess(): void {}
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase {
                $this->options->requireFlag('something');
                return $this;
            }
            public function run(): ServiceBase { return $this; }
        };

        # Act
        $simpleService->configureAndRun();
    }

    /**
     * @covers \Fie\Automation\ServiceBase::fatal()
     * @throws \Fie\Automation\ServiceUtils\ServiceException
     */
    public function testFatal() {
        # Assert (expect)
        $this->expectExceptionMessage(1);

        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = new class($logger) extends ServiceBase {
            protected function onSuccess(): void {}
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            public function run(): ServiceBase { return $this; }
        };

        # Act
        $simpleService->fatal();
    }

    /**
     * @covers \Fie\Automation\ServiceBase::spawnChildService()
     */
    public function testSpawnChildServiceInvalid() {
        # Arrange
        $logger = (new Logger())->silent();
        $serviceBase = new SimpleService($logger);

        # Act
        $result = $serviceBase->spawnChildService(
            new class($logger) extends ServiceBase {
                protected function onSuccess(): void {}
                protected function onFailure(): void {}
                protected function getServiceOptions(): array { return []; }
                protected function help(): string { return ''; }
                protected function configure(): ServiceBase { return $this; }
                public function run(): ServiceBase { return $this; }
            }
        );

        # Assert
        $this->assertFalse($result);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::spawnChildService()
     * @throws \Fie\Automation\ServiceUtils\ServiceException
     */
    public function testSpawnChildServiceOnSuccess() {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = new class($logger) extends ServiceBase {
            public $result;
            protected function onSuccess(): void {
                $this->result = $this->spawnChildService(
                    new SimpleService($this->logger)
                );
            }
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            public function run(): ServiceBase { return $this; }
        };

        # Act
        $simpleService->configureAndRun();

        # Assert
        $this->assertTrue($simpleService->result);
    }

    /**
     * @covers \Fie\Automation\ServiceBase::spawnChildService()
     */
    public function testSpawnChildServiceOnFailure() {
        # Arrange
        $logger = (new Logger())->silent();
        $simpleService = new class($logger) extends ServiceBase {
            public $result;
            protected function onSuccess(): void {}
            protected function onFailure(): void {
                $this->result = $this->spawnChildService(
                    new SimpleService($this->logger)
                );
            }
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            public function run(): ServiceBase {
                $this->exitFailure();
            }
        };

        # Act
        try {
            $simpleService->configureAndRun();
        } catch (ServiceException $e) {
        } catch (Exception $e) {
        }

        # Assert
        $this->assertTrue($simpleService->result);
    }
}
