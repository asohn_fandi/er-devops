<?php

namespace Fie\Test\Automation;

use Fie\Automation\ChildService;
use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\Logger;
use PHPUnit\Framework\TestCase;

final class ChildServiceTest extends TestCase
{
    /**
     * @covers \Fie\Automation\ChildService::__construct()
     */
    public function testInit() {
        # Arrange
        $logger = (new Logger())->silent();

        # Act
        $childService = new ChildService($logger);

        # Assert
        $this->assertInstanceOf(ChildService::class, $childService);
    }

    /**
     * @covers \Fie\Automation\ChildService::setServiceObject()
     * @covers \Fie\Automation\ChildService::getServiceObject()
     */
    public function testSetServiceObject() {
        # Arrange
        $logger = (new Logger())->silent();
        $childService = new ChildService($logger);
        $simpleService = new SimpleService($logger);

        # Act
        $childService->setServiceObject($simpleService);
        $serviceObject = $childService->getServiceObject();

        # Assert
        $this->assertInstanceOf(SimpleService::class, $serviceObject);
    }

    /**
     * @covers \Fie\Automation\ChildService::prepareOptions()
     * @covers \Fie\Automation\ChildService::run()
     * @throws \Fie\Automation\ServiceUtils\ServiceException
     */
    public function testSpawnChildService() {
        # Arrange
        $GLOBALS['argv'] = ['--verbose'];
        $logger = (new Logger())->silent();
        $simpleService = new class($logger) extends ServiceBase {
            public $result;
            protected function onSuccess(): void {
                $this->result = $this->spawnChildService(
                    new SimpleService($this->logger),
                    ['debug' => '']
                );
            }
            protected function onFailure(): void {}
            protected function getServiceOptions(): array { return []; }
            protected function help(): string { return ''; }
            protected function configure(): ServiceBase { return $this; }
            public function run(): ServiceBase { return $this; }
        };

        # Act
        $simpleService->configureAndRun();

        # Assert
        $this->assertTrue($simpleService->result);
    }
}
