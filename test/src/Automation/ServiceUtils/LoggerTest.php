<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\ServiceUtils\DataTracker;
use Fie\Automation\ServiceUtils\Logger;
use Fie\Automation\ServiceUtils\Logger\LogLevel;
use Fie\Automation\ServiceUtils\Options;
use PHPUnit\Framework\TestCase;

class LoggerTest extends TestCase
{
    private $didTerminate = false;

    public function teardown() {
        $this->didTerminate = false;
    }

    /**
     * ob_get_clean() will return an empty string which will create a single element array when split
     *
     * @return array
     */
    private static function obGetCleanArray() {
        $contents = ob_get_clean();
        if ($contents === '') {
            return [];
        } else {
            return explode("\n", rtrim($contents));
        }
    }

    public function fauxTerminator() {
        $this->didTerminate = true;
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::__construct()
     * @covers \Fie\Automation\ServiceUtils\Logger::__destruct()
     */
    public function testInit() {
        # Arrange/Act
        $logger = new Logger();

        # Assert
        $this->assertInstanceOf(Logger::class, $logger);
        $logger->__destruct();
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::level()
     */
    public function testLevel() {
        # Arrange
        $logger = new Logger();

        # Act
        $logLevel = $logger->level(123);

        # Assert
        $this->assertSame(123, $logLevel);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::setup()
     * @covers \Fie\Automation\ServiceUtils\Logger::level()
     * @dataProvider dataConfigureOptionsLogLevel
     * @param array $argv
     * @param int $expected
     * @throws \Exception
     */
    public function testConfigureOptionsLogLevel($argv, $expected) {
        # Arrange
        $GLOBALS['argv'] = $argv;
        $options = (new Options())->processOptions();

        # Act
        $logger = (new Logger())
            ->setWebOverride(true)
            ->setup($options);
        $logLevel = $logger->level();

        # Assert
        $this->assertSame($expected, $logLevel);
    }
    public function dataConfigureOptionsLogLevel() {
        return [
            'logLevel 0'             => [[],                       0],
            'logLevel 1 via verbose' => [['--verbose'],            1],
            'logLevel 1 via debug'   => [['--debug'],              2],
            'logLevel 2 via both'    => [['--verbose', '--debug'], 2],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::silent()
     * @covers \Fie\Automation\ServiceUtils\Logger::level()
     */
    public function testSilent() {
        # Arrange
        $logger = new Logger();

        # Act
        $logger = $logger->silent();
        $logLevel = $logger->level();

        # Assert
        $this->assertSame(LogLevel::SILENT, $logLevel);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::silentLock()
     * @covers \Fie\Automation\ServiceUtils\Logger::silent()
     * @covers \Fie\Automation\ServiceUtils\Logger::level()
     */
    public function testSilentLock() {
        # Arrange
        $logger = new Logger();

        # Act
        $logger = $logger->silentLock();
        $logLevel = $logger->level();

        # Assert
        $this->assertSame(LogLevel::SILENT, $logLevel);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::silentLock()
     * @covers \Fie\Automation\ServiceUtils\Logger::silent()
     * @covers \Fie\Automation\ServiceUtils\Logger::level()
     */
    public function testSilentLocked() {
        # Arrange
        $logger = (new Logger())->silentLock();

        # Act
        $logger->level(LogLevel::DEBUG);
        $logLevel = $logger->level();

        # Assert
        $this->assertSame(LogLevel::SILENT, $logLevel);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::level()
     * @covers \Fie\Automation\ServiceUtils\Logger::isVerbose()
     */
    public function testIsVerbose() {
        # Arrange
        $logger = new Logger();

        # Act
        $logger->level(LogLevel::INFO);
        $isVerbose = $logger->isVerbose();

        # Assert
        $this->assertTrue($isVerbose);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::level()
     * @covers \Fie\Automation\ServiceUtils\Logger::isDebug()
     */
    public function testIsDebug() {
        # Arrange
        $logger = new Logger();

        # Act
        $logger->level(LogLevel::DEBUG);
        $isDebug = $logger->isDebug();

        # Assert
        $this->assertTrue($isDebug);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::flush()
     * @covers \Fie\Automation\ServiceUtils\Logger::clearLogs()
     */
    public function testFlushEmpty() {
        # Arrange
        $logger = new Logger();

        # Act
        ob_start();
        $logger->flush();
        $output = self::obGetCleanArray();

        # Assert
        $this->assertCount(0, $output);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::flush()
     * @covers \Fie\Automation\ServiceUtils\Logger::clearLogs()
     */
    public function testFlushLogLevel() {
        # Arrange
        $logger = new Logger();

        # Act
        ob_start();
        $logger
            ->log('Hello world!')
            ->info('Hidden verbose')
            ->debug('Hidden debug')
            ->flush();
        $output = self::obGetCleanArray();

        # Assert
        $this->assertCount(1, $output);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\LogLevel::set()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::getSuccess()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::getWarning()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::getDebug()
     * @covers \Fie\Automation\ServiceUtils\Logger::logSuccess()
     * @covers \Fie\Automation\ServiceUtils\Logger::log()
     * @covers \Fie\Automation\ServiceUtils\Logger::logVar()
     * @covers \Fie\Automation\ServiceUtils\Logger::warn()
     * @covers \Fie\Automation\ServiceUtils\Logger::warnVar()
     * @covers \Fie\Automation\ServiceUtils\Logger::info()
     * @covers \Fie\Automation\ServiceUtils\Logger::infoVar()
     * @covers \Fie\Automation\ServiceUtils\Logger::debug()
     * @covers \Fie\Automation\ServiceUtils\Logger::debugVar()
     * @covers \Fie\Automation\ServiceUtils\Logger::lineBreak()
     * @covers \Fie\Automation\ServiceUtils\Logger::lineBreakVerbose()
     * @covers \Fie\Automation\ServiceUtils\Logger::lineBreakDebug()
     * @covers \Fie\Automation\ServiceUtils\Logger::getCleanVariable()
     * @covers \Fie\Automation\ServiceUtils\Logger::writeLn()
     * @covers \Fie\Automation\ServiceUtils\Logger::flush()
     * @dataProvider dataLogEntry
     * @param int $logLevel
     * @param string $entryMethod
     * @param int $linesPrinted
     */
    public function testLogEntry($logLevel, $entryMethod, $linesPrinted) {
        # Arrange
        $logger = new Logger();
        $logger->level($logLevel);

        # Act
        ob_start();
        call_user_func_array([$logger, $entryMethod], ['some message']);
        $logger->flush();
        $output = self::obGetCleanArray();

        # Assert
        $this->assertCount($linesPrinted, $output);
    }
    public function dataLogEntry() {
        return [
            'LogLevel::SILENT logSuccess()'       => [LogLevel::SILENT, 'logSuccess',       0],
            'LogLevel::SILENT log()'              => [LogLevel::SILENT, 'log',              0],
            'LogLevel::SILENT logVar()'           => [LogLevel::SILENT, 'logVar',           0],
            'LogLevel::SILENT warn()'             => [LogLevel::SILENT, 'warn',             0],
            'LogLevel::SILENT warnVar()'          => [LogLevel::SILENT, 'warnVar',          0],
            'LogLevel::SILENT info()'             => [LogLevel::SILENT, 'info',             0],
            'LogLevel::SILENT infoVar()'          => [LogLevel::SILENT, 'infoVar',          0],
            'LogLevel::SILENT debug()'            => [LogLevel::SILENT, 'debug',            0],
            'LogLevel::SILENT debugVar()'         => [LogLevel::SILENT, 'debugVar',         0],
            'LogLevel::SILENT lineBreak()'        => [LogLevel::SILENT, 'lineBreak',        0],
            'LogLevel::SILENT lineBreakVerbose()' => [LogLevel::SILENT, 'lineBreakVerbose', 0],
            'LogLevel::SILENT lineBreakDebug()'   => [LogLevel::SILENT, 'lineBreakDebug',   0],

            'LogLevel::PRINT logSuccess()'       => [LogLevel::PRINT, 'logSuccess',       1],
            'LogLevel::PRINT log()'              => [LogLevel::PRINT, 'log',              1],
            'LogLevel::PRINT logVar()'           => [LogLevel::PRINT, 'logVar',           1],
            'LogLevel::PRINT warn()'             => [LogLevel::PRINT, 'warn',             1],
            'LogLevel::PRINT warnVar()'          => [LogLevel::PRINT, 'warnVar',          1],
            'LogLevel::PRINT info()'             => [LogLevel::PRINT, 'info',             0],
            'LogLevel::PRINT infoVar()'          => [LogLevel::PRINT, 'infoVar',          0],
            'LogLevel::PRINT debug()'            => [LogLevel::PRINT, 'debug',            0],
            'LogLevel::PRINT debugVar()'         => [LogLevel::PRINT, 'debugVar',         0],
            'LogLevel::PRINT lineBreak()'        => [LogLevel::PRINT, 'lineBreak',        1],
            'LogLevel::PRINT lineBreakVerbose()' => [LogLevel::PRINT, 'lineBreakVerbose', 0],
            'LogLevel::PRINT lineBreakDebug()'   => [LogLevel::PRINT, 'lineBreakDebug',   0],

            'LogLevel::WARN logSuccess()'       => [LogLevel::WARN, 'logSuccess',       1],
            'LogLevel::WARN log()'              => [LogLevel::WARN, 'log',              1],
            'LogLevel::WARN logVar()'           => [LogLevel::WARN, 'logVar',           1],
            'LogLevel::WARN warn()'             => [LogLevel::WARN, 'warn',             1],
            'LogLevel::WARN warnVar()'          => [LogLevel::WARN, 'warnVar',          1],
            'LogLevel::WARN info()'             => [LogLevel::WARN, 'info',             0],
            'LogLevel::WARN infoVar()'          => [LogLevel::WARN, 'infoVar',          0],
            'LogLevel::WARN debug()'            => [LogLevel::WARN, 'debug',            0],
            'LogLevel::WARN debugVar()'         => [LogLevel::WARN, 'debugVar',         0],
            'LogLevel::WARN lineBreak()'        => [LogLevel::WARN, 'lineBreak',        1],
            'LogLevel::WARN lineBreakVerbose()' => [LogLevel::WARN, 'lineBreakVerbose', 0],
            'LogLevel::WARN lineBreakDebug()'   => [LogLevel::WARN, 'lineBreakDebug',   0],

            'LogLevel::INFO logSuccess()'       => [LogLevel::INFO, 'logSuccess',       1],
            'LogLevel::INFO log()'              => [LogLevel::INFO, 'log',              1],
            'LogLevel::INFO logVar()'           => [LogLevel::INFO, 'logVar',           1],
            'LogLevel::INFO warn()'             => [LogLevel::INFO, 'warn',             1],
            'LogLevel::INFO warnVar()'          => [LogLevel::INFO, 'warnVar',          1],
            'LogLevel::INFO info()'             => [LogLevel::INFO, 'info',             1],
            'LogLevel::INFO infoVar()'          => [LogLevel::INFO, 'infoVar',          1],
            'LogLevel::INFO debug()'            => [LogLevel::INFO, 'debug',            0],
            'LogLevel::INFO debugVar()'         => [LogLevel::INFO, 'debugVar',         0],
            'LogLevel::INFO lineBreak()'        => [LogLevel::INFO, 'lineBreak',        1],
            'LogLevel::INFO lineBreakVerbose()' => [LogLevel::INFO, 'lineBreakVerbose', 1],
            'LogLevel::INFO lineBreakDebug()'   => [LogLevel::INFO, 'lineBreakDebug',   0],

            'LogLevel::DEBUG logSuccess()'       => [LogLevel::DEBUG, 'logSuccess',       1],
            'LogLevel::DEBUG log()'              => [LogLevel::DEBUG, 'log',              1],
            'LogLevel::DEBUG logVar()'           => [LogLevel::DEBUG, 'logVar',           1],
            'LogLevel::DEBUG warn()'             => [LogLevel::DEBUG, 'warn',             1],
            'LogLevel::DEBUG warnVar()'          => [LogLevel::DEBUG, 'warnVar',          1],
            'LogLevel::DEBUG info()'             => [LogLevel::DEBUG, 'info',             1],
            'LogLevel::DEBUG infoVar()'          => [LogLevel::DEBUG, 'infoVar',          1],
            'LogLevel::DEBUG debug()'            => [LogLevel::DEBUG, 'debug',            1],
            'LogLevel::DEBUG debugVar()'         => [LogLevel::DEBUG, 'debugVar',         1],
            'LogLevel::DEBUG lineBreak()'        => [LogLevel::DEBUG, 'lineBreak',        1],
            'LogLevel::DEBUG lineBreakVerbose()' => [LogLevel::DEBUG, 'lineBreakVerbose', 1],
            'LogLevel::DEBUG lineBreakDebug()'   => [LogLevel::DEBUG, 'lineBreakDebug',   1],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\LogLevel::set()
     * @covers \Fie\Automation\ServiceUtils\Logger::logVar()
     * @covers \Fie\Automation\ServiceUtils\Logger::warnVar()
     * @covers \Fie\Automation\ServiceUtils\Logger::infoVar()
     * @covers \Fie\Automation\ServiceUtils\Logger::debugVar()
     * @covers \Fie\Automation\ServiceUtils\Logger::getCleanVariable()
     * @covers \Fie\Automation\ServiceUtils\Logger::writeLn()
     * @covers \Fie\Automation\ServiceUtils\Logger::flush()
     * @dataProvider dataLogEntryVar
     * @param int $logLevel
     * @param string $entryMethod
     * @param int $linesPrinted
     */
    public function testLogEntryVar($logLevel, $entryMethod, $linesPrinted) {
        # Arrange
        $logger = new Logger();
        $logger->level($logLevel);

        # Act
        ob_start();
        call_user_func_array([$logger, $entryMethod], [$logger]);
        $logger->flush();
        $output = self::obGetCleanArray();

        # Assert
        $this->assertCount($linesPrinted, $output);
    }
    public function dataLogEntryVar() {
        return [
            'LogLevel::SILENT logVar()'   => [LogLevel::SILENT, 'logVar',   0],
            'LogLevel::SILENT warnVar()'  => [LogLevel::SILENT, 'warnVar',  0],
            'LogLevel::SILENT infoVar()'  => [LogLevel::SILENT, 'infoVar',  0],
            'LogLevel::SILENT debugVar()' => [LogLevel::SILENT, 'debugVar', 0],

            'LogLevel::PRINT logVar()'   => [LogLevel::PRINT, 'logVar',   1],
            'LogLevel::PRINT warnVar()'  => [LogLevel::PRINT, 'warnVar',  1],
            'LogLevel::PRINT infoVar()'  => [LogLevel::PRINT, 'infoVar',  0],
            'LogLevel::PRINT debugVar()' => [LogLevel::PRINT, 'debugVar', 0],

            'LogLevel::WARN logVar()'   => [LogLevel::WARN, 'logVar',   1],
            'LogLevel::WARN warnVar()'  => [LogLevel::WARN, 'warnVar',  1],
            'LogLevel::WARN infoVar()'  => [LogLevel::WARN, 'infoVar',  0],
            'LogLevel::WARN debugVar()' => [LogLevel::WARN, 'debugVar', 0],

            'LogLevel::INFO logVar()'   => [LogLevel::INFO, 'logVar',   1],
            'LogLevel::INFO warnVar()'  => [LogLevel::INFO, 'warnVar',  1],
            'LogLevel::INFO infoVar()'  => [LogLevel::INFO, 'infoVar',  1],
            'LogLevel::INFO debugVar()' => [LogLevel::INFO, 'debugVar', 0],

            'LogLevel::DEBUG logVar()'   => [LogLevel::DEBUG, 'logVar',   1],
            'LogLevel::DEBUG warnVar()'  => [LogLevel::DEBUG, 'warnVar',  1],
            'LogLevel::DEBUG infoVar()'  => [LogLevel::DEBUG, 'infoVar',  1],
            'LogLevel::DEBUG debugVar()' => [LogLevel::DEBUG, 'debugVar', 1],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::setDataTracker()
     * @covers \Fie\Automation\ServiceUtils\Logger::getDataTracker()
     * @covers \Fie\Automation\ServiceUtils\Logger::logSuccess()
     * @covers \Fie\Automation\ServiceUtils\Logger::log()
     * @covers \Fie\Automation\ServiceUtils\Logger::warn()
     * @covers \Fie\Automation\ServiceUtils\Logger::error()
     * @covers \Fie\Automation\ServiceUtils\Logger::errorVar()
     * @covers \Fie\Automation\ServiceUtils\Logger::trace()
     * @covers \Fie\Automation\ServiceUtils\Logger::writeLn()
     * @covers \Fie\Automation\ServiceUtils\Logger::hookDataTracker()
     * @dataProvider dataDataTracker
     * @param $entryMethod
     * @param $numDataTracked
     */
    public function testDataTracker($entryMethod, $numDataTracked) {
        # Arrange
        $logger = (new Logger())->setDataTracker(new DataTracker('application/json'));

        # Act
        ob_start();
        call_user_func_array([$logger, $entryMethod], ['some message']);
        $logger->flush();
        $logLevel = $logger->level();
        $dataTracker = $logger->getDataTracker();
        $data = json_decode($dataTracker->toJson(), true)['messages'];
        ob_end_clean();

        # Assert
        $this->assertInstanceOf(DataTracker::class, $dataTracker);
        $this->assertSame(LogLevel::SILENT, $logLevel);
        $this->assertCount($numDataTracked, $data);
    }

    public function dataDataTracker() {
        return [
            'dataTracker lineBreak()'        => ['lineBreak',        0],
            'dataTracker lineBreakVerbose()' => ['lineBreakVerbose', 0],
            'dataTracker lineBreakDebug()'   => ['lineBreakDebug',   0],
            'dataTracker logSuccess()'       => ['logSuccess',       1],
            'dataTracker log()'              => ['log',              1],
            'dataTracker logVar()'           => ['logVar',           1],
            'dataTracker warn()'             => ['warn',             1],
            'dataTracker warnVar()'          => ['warnVar',          1],
            'dataTracker info()'             => ['info',             0],
            'dataTracker infoVar()'          => ['infoVar',          0],
            'dataTracker debug()'            => ['debug',            0],
            'dataTracker debugVar()'         => ['debugVar',         0],
            'dataTracker error()'            => ['error',            1],
            'dataTracker errorVar()'         => ['errorVar',         1],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::trace()
     */
    public function testTrace() {
        # Arrange
        $logger = (new Logger())->setDataTracker(new DataTracker('application/json'));

        # Act
        ob_start();
        $logger
            ->trace('some message')
            ->flush();
        $logLevel = $logger->level();
        $dataTracker = $logger->getDataTracker();
        $data = json_decode($dataTracker->toJson(), true)['messages'];
        ob_end_clean();

        # Assert
        $this->assertInstanceOf(DataTracker::class, $dataTracker);
        $this->assertSame(LogLevel::SILENT, $logLevel);
        $this->assertCount(1, $data);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::fatal()
     */
    public function testFatal() {
        # Arrange
        $logger = (new Logger())->setDataTracker(new DataTracker('application/json'));

        # Act
        ob_start();
        $logger->fatal('some message', [$this, 'fauxTerminator']);
        $logLevel = $logger->level();
        $dataTracker = $logger->getDataTracker();
        $data = json_decode($dataTracker->toJson(), true)['messages'];
        ob_end_clean();

        # Assert
        $this->assertInstanceOf(DataTracker::class, $dataTracker);
        $this->assertSame(LogLevel::SILENT, $logLevel);
        $this->assertCount(1, $data);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::setImmediateFlush()
     * @covers \Fie\Automation\ServiceUtils\Logger::writeLn()
     */
    public function testImmediateFlush() {
        # Arrange
        $logger = (new Logger())->setImmediateFlush(true);

        # Act
        ob_start();
        $logger
            ->log('some message')
            ->flush();
        $output = self::obGetCleanArray();

        # Assert
        $this->assertCount(1, $output);
    }
}
