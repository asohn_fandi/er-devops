<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\ServiceUtils\ServiceException;
use PHPUnit\Framework\TestCase;

final class ServiceExceptionTest extends TestCase
{
    /**
     * @covers \Fie\Automation\ServiceUtils\ServiceException::isSuccessful()
     * @dataProvider dataIsSuccessful
     * @param $message
     * @param $expected
     */
    public function testIsSuccessful($expected, $message) {
        # Arrange
        $exceptionObject = new ServiceException($message);

        # Act
        $isSuccessful = $exceptionObject->isSuccessful();

        # Assert
        $this->assertEquals($expected, $isSuccessful);
    }
    public function dataIsSuccessful() {
        return [
            'null'            => [false, null],
            'non-zero'        => [false, 1],
            'non-zero string' => [false, '1'],
            'character'       => [false, 'c'],
            'string'          => [false, 'string'],
            'zero'            => [true, 0],
            'zero string'     => [true, '0'],
        ];
    }
}
