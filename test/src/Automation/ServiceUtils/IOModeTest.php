<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\ServiceUtils\IOMode;
use PHPUnit\Framework\TestCase;

final class IOModeTest extends TestCase
{
    /**
     * @covers \Fie\Automation\ServiceUtils\IOMode::__construct()
     * @covers \Fie\Automation\ServiceUtils\IOMode::getMode()
     */
    public function testInit() {
        # Arrange/Act
        $ioMode = new IOMode();

        # Assert
        $this->assertInstanceOf(IOMode::class, $ioMode);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\IOMode::__construct()
     * @covers \Fie\Automation\ServiceUtils\IOMode::getMode()
     * @covers \Fie\Automation\ServiceUtils\IOMode::isInteractiveSTDIN()
     * @covers \Fie\Automation\ServiceUtils\IOMode::isInteractiveSTDOUT()
     * @covers \Fie\Automation\ServiceUtils\IOMode::isInteractiveSTDERR()
     * @dataProvider dataIsInteractiveMethods
     * @param string $methodSuffix
     * @param int $mode
     * @param bool $expected
     */
    public function testIsInteractiveMethods($methodSuffix, $mode, $expected) {
        # Arrange
        $ioMode = new IOMode($mode);

        # Act
        $isInteractive = call_user_func([$ioMode, "isInteractive$methodSuffix"]);

        # Assert
        $this->assertEquals($expected, $isInteractive);
    }
    public function dataIsInteractiveMethods() {
        return [
            'STDIN isFifo' => ['STDIN',  4096, true],
            'STDIN isChar' => ['STDIN',  8192, false],
            'STDIN isDir'  => ['STDIN', 16384, true],
            'STDIN isBlk'  => ['STDIN', 24576, false],
            'STDIN isReg'  => ['STDIN', 32768, true],
            'STDIN isLnk'  => ['STDIN', 40960, false],
            'STDIN isSock' => ['STDIN', 49152, true],
            'STDIN other'  => ['STDIN',  8592, false],

            'STDOUT isFifo' => ['STDOUT',  4096, true],
            'STDOUT isChar' => ['STDOUT',  8192, false],
            'STDOUT isDir'  => ['STDOUT', 16384, false],
            'STDOUT isBlk'  => ['STDOUT', 24576, false],
            'STDOUT isReg'  => ['STDOUT', 32768, true],
            'STDOUT isLnk'  => ['STDOUT', 40960, false],
            'STDOUT isSock' => ['STDOUT', 49152, false],
            'STDOUT other'  => ['STDOUT',  8592, false],

            'STDERR isFifo' => ['STDERR',  4096, true],
            'STDERR isChar' => ['STDERR',  8192, false],
            'STDERR isDir'  => ['STDERR', 16384, false],
            'STDERR isBlk'  => ['STDERR', 24576, false],
            'STDERR isReg'  => ['STDERR', 32768, true],
            'STDERR isLnk'  => ['STDERR', 40960, false],
            'STDERR isSock' => ['STDERR', 49152, false],
            'STDERR other'  => ['STDERR',  8592, false],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\IOMode::isInteractive()
     * @dataProvider dataIsInteractive
     * @param $webOverride
     * @param $expected
     */
    public function testIsInteractive($webOverride, $expected) {
        # Arrange
        $ioMode = new IOMode();

        # Act
        $isInteractive = $ioMode->isInteractive($webOverride);

        # Assert
        $this->assertSame($expected, $isInteractive);
    }
    public function dataIsInteractive() {
        return [
            'null web override'  => [null,  false],
            'true web override'  => [true,  false],
            'false web override' => [false, false],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\IOMode::isInteractive()
     */
    public function testIsInteractAlreadySet() {
        # Arrange
        $ioMode = new IOMode();
        $ioMode->isInteractive();

        # Act
        $isInteractive = $ioMode->isInteractive();

        # Assert
        $this->assertFalse($isInteractive);
    }
}
