<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\ServiceUtils\BuildConfig;
use PHPUnit\Framework\TestCase;

final class BuildConfigTest extends TestCase
{
    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::__construct()
     * @throws \Exception
     */
    public function testInit() {
        # Arrange/Act
        $buildConfig = new BuildConfig();

        # Assert
        $this->assertInstanceOf(BuildConfig::class, $buildConfig);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::setStrict()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::getValue()
     * @throws \Exception
     */
    public function testSetStrict() {
        # Assert (expect)
        $this->expectExceptionMessage('Unknown config value: level0.level0_0.level0_0_1');

        # Arrange
        $cache  = [
            'level0' => [
                'level0_0' => [
                    'level0_0_0' => 'level0_0_0_value',
                ],
            ],
        ];
        $buildConfig = (new BuildConfig())
            ->setCache($cache)
            ->setStrict();

        # Act
        $buildConfig->getValue('level0', 'level0_0', 'level0_0_1');
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::getCache()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::setCache()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::read()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::parseJsonFile()
     * @throws \Exception
     */
    public function testGetCache() {
        # Arrange
        $buildConfig = new BuildConfig();

        # Act
        $buildConfig->getCache();

        # Assert
        $this->assertInstanceOf(BuildConfig::class, $buildConfig);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::read()
     * @throws \Exception
     */
    public function testRead() {
        # Arrange
        $cache  = [
            'level0' => [
                'level0_0' => [
                    'level0_0_0' => 'level0_0_0_value',
                ],
                'level0_1' => 'level0_1_value',
            ],
        ];
        $buildConfig = (new BuildConfig())->setCache($cache);

        # Act
        $result = $buildConfig->read(true);

        # Assert
        $this->assertSame($cache, $result);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::getValue()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::getCache()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::setCache()
     * @dataProvider dataGetValue
     * @param mixed $get
     * @param mixed $expected
     * @throws \Exception
     */
    public function testGetValue($get, $expected) {
        # Arrange
        $buildConfig = (new BuildConfig())
            ->setCache([
                'level0' => [
                    'level0_0' => [
                        'level0_0_0' => 'level0_0_0_value',
                    ],
                    'level0_1' => 'level0_1_value',
                ],
            ]);

        # Act
        $result = call_user_func_array([$buildConfig, 'getValue'], $get);

        # Assert
        $this->assertSame($expected, $result);
    }
    public function dataGetValue() {
        return [
            'empty call'  => [
                [],
                [
                    'level0' => [
                        'level0_0' => [
                            'level0_0_0' => 'level0_0_0_value',
                        ],
                        'level0_1' => 'level0_1_value',
                    ],
                ],
            ],
            'invalid level0' => [
                ['levelX'],
                 null,
            ],
            'invalid level1' => [
                ['level0', 'level1_X'],
                null,
            ],
            'invalid level2' => [
                ['level0', 'level0_0', 'level2_X'],
                null,
            ],
            'level0 call' => [
                ['level0'],
                [
                    'level0_0' => [
                        'level0_0_0' => 'level0_0_0_value',
                    ],
                    'level0_1' => 'level0_1_value',
                ],
            ],
            'level1_0 call' => [
                ['level0', 'level0_0'],
                [
                    'level0_0_0' => 'level0_0_0_value',
                ],
            ],
            'level1_1 call' => [
                ['level0', 'level0_1'],
                'level0_1_value',
            ],
        ];
    }
}
