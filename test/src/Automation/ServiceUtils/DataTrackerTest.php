<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\ServiceUtils\DataTracker;
use PHPUnit\Framework\TestCase;

final class DataTrackerTest extends TestCase
{
    /**
     * @covers \Fie\Automation\ServiceUtils\DataTracker::__construct()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::setDataType()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::getDataType()
     */
    public function testDataType() {
        # Arrange
        $dataTracker = new DataTracker('application/json');

        # Act
        $dataType = $dataTracker->getDataType();

        # Assert
        $this->assertSame('application/json', $dataType);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\DataTracker::info()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::write()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::getData()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::toJson()
     */
    public function testInfo() {
        # Arrange
        $dataTracker = new DataTracker('application/json');

        # Act
        $dataTracker->info('some message');
        $data = json_decode($dataTracker->toJson(), true)['messages'];
        $log = $data[0];

        # Assert
        $this->assertCount(1, $data);
        $this->assertSame(DataTracker::TYPE_INFO, $log['type']);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\DataTracker::warning()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::write()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::getData()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::toJson()
     */
    public function testWarning() {
        # Arrange
        $dataTracker = new DataTracker('application/json');

        # Act
        $dataTracker->warning('some message');
        $data = json_decode($dataTracker->toJson(), true)['messages'];
        $log = $data[0];

        # Assert
        $this->assertCount(1, $data);
        $this->assertSame(DataTracker::TYPE_WARN, $log['type']);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\DataTracker::success()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::write()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::getData()
     * @covers \Fie\Automation\ServiceUtils\DataTracker::toJson()
     */
    public function testSuccess() {
        # Arrange
        $dataTracker = new DataTracker('application/json');

        # Act
        $dataTracker->success('some message');
        $data = json_decode($dataTracker->toJson(), true)['messages'];
        $log = $data[0];

        # Assert
        $this->assertCount(1, $data);
        $this->assertSame(DataTracker::TYPE_SUCC, $log['type']);
    }
}
