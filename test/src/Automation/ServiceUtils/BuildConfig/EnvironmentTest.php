<?php

namespace Fie\Test\Automation\ServiceUtils\BuildConfig;

use Fie\Automation\ServiceUtils\BuildConfig;
use Fie\Automation\ServiceUtils\BuildConfig\Environment;
use Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer;
use PHPUnit\Framework\TestCase;

final class EnvironmentTest extends TestCase
{
    private $buildConfig;

    /**
     * @param null|string $name
     * @param array $data
     * @param string $dataName
     * @throws \Exception
     */
    public function __construct(?string $name = null, array $data = [], string $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->buildConfig = (new BuildConfig())->setCache([
            'productEnvironments' => [
                'repo_0' => [
                    'env_0' => [
                        'serverList' => [
                            [
                                'env' => 'unit',
                                'type' => 'web',
                                'ipAddress' => '0.0.0.0',
                                'sshUsername' => 'username',
                                'deployDirectory' => '/home/ubuntu/repos/ER',
                            ],
                            [
                                'env' => 'unit',
                                'type' => 'task',
                                'ipAddress' => '123.123.123.123',
                                'sshUsername' => 'username',
                                'deployDirectory' => '/home/ubuntu/repos/ER',
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\Environment::__construct()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\Environment::validate()
     * @throws \Exception
     */
    public function testInit() {
        # Arrange/Act
        $environment = new Environment('repo_0', 'env_0', $this->buildConfig);

        # Assert
        $this->assertInstanceOf(Environment::class, $environment);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\Environment::validate()
     * @throws \Exception
     */
    public function testValidateRepo() {
        # Assert (expect)
        $this->expectExceptionMessage("Missing environment configuration for repository 'bogus'");

        # Arrange/Act
        new Environment('bogus', 'env_0', $this->buildConfig);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\Environment::validate()
     * @throws \Exception
     */
    public function testValidateEnv() {
        # Assert (expect)
        $this->expectExceptionMessage("Missing environment configuration for environment 'repo_0.bogus'");

        # Arrange/Act
        new Environment('repo_0', 'bogus', $this->buildConfig);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\Environment::getServerList()
     * @throws \Exception
     */
    public function testGetServerList() {
        # Arrange
        $environment = new Environment('repo_0', 'env_0', $this->buildConfig);

        # Act
        $serverList = (array) $environment->getServerList();

        # Assert
        $this->assertContainsOnlyInstancesOf(EnvironmentServer::class, $serverList);
        $this->assertCount(2, $serverList);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\Environment::getServerList()
     * @throws \Exception
     */
    public function testGetServerListError() {
        # Assert (expect)
        $this->expectExceptionMessageRegExp("/^Environment config error for 'repo_0.env_0':/");

        # Arrange
        $cache = $this->buildConfig->getCache();
        $cache['productEnvironments']['repo_0']['env_0']['serverList'][] = [];
        $buildConfig = $this->buildConfig->setCache($cache);
        $environment = new Environment('repo_0', 'env_0', $buildConfig);

        # Act
        $environment->getServerList();
    }
}
