<?php

namespace Fie\Test\Automation\ServiceUtils\BuildConfig;

use Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer;
use PHPUnit\Framework\TestCase;

final class EnvironmentServerTest extends TestCase
{
    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::__construct()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::validate()
     * @throws \Exception
     */
    public function testInit() {
        # Arrange
        $serverConfig = [
            'env' => 'unit',
            'type' => 'web',
            'ipAddress' => '0.0.0.0',
            'sshUsername' => 'username',
            'deployDirectory' => '/home/ubuntu/repos/ER',
        ];

        # Act
        $environmentServer = new EnvironmentServer($serverConfig);

        # Assert
        $this->assertInstanceOf(EnvironmentServer::class, $environmentServer);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::fromArray()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::__construct()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::validate()
     * @throws \Exception
     */
    public function testFromArray() {
        # Arrange
        $serverConfig = [
            'env' => 'unit',
            'type' => 'web',
            'ipAddress' => '0.0.0.0',
            'sshUsername' => 'username',
            'deployDirectory' => '/home/ubuntu/repos/ER',
        ];

        # Act
        $environmentServer = EnvironmentServer::fromArray($serverConfig);

        # Assert
        $this->assertInstanceOf(EnvironmentServer::class, $environmentServer);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::fromArray()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::__construct()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::validate()
     * @throws \Exception
     */
    public function testValidateInvalid() {
        # Assert (expect)
        $this->expectExceptionMessageRegExp("/^Invalid\/Missing server config value '/");

        # Arrange/Act
        new EnvironmentServer([]);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::getEnv()
     * @throws \Exception
     */
    public function testGetEnv() {
        # Arrange
        $serverConfig = [
            'env' => 'unit',
            'type' => 'web',
            'ipAddress' => '0.0.0.0',
            'sshUsername' => 'username',
            'deployDirectory' => '/home/ubuntu/repos/ER',
        ];
        $environmentServer = new EnvironmentServer($serverConfig);

        # Act
        $env = $environmentServer->getEnv();

        # Assert
        $this->assertEquals('unit', $env);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::getType()
     * @throws \Exception
     */
    public function testGetType() {
        # Arrange
        $serverConfig = [
            'env' => 'unit',
            'type' => 'web',
            'ipAddress' => '0.0.0.0',
            'sshUsername' => 'username',
            'deployDirectory' => '/home/ubuntu/repos/ER',
        ];
        $environmentServer = new EnvironmentServer($serverConfig);

        # Act
        $type = $environmentServer->getType();

        # Assert
        $this->assertEquals('web', $type);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::getIpAddress()
     * @throws \Exception
     */
    public function testGetIpAddress() {
        # Arrange
        $serverConfig = [
            'env' => 'unit',
            'type' => 'web',
            'ipAddress' => '0.0.0.0',
            'sshUsername' => 'username',
            'deployDirectory' => '/home/ubuntu/repos/ER',
        ];
        $environmentServer = new EnvironmentServer($serverConfig);

        # Act
        $ipAddress = $environmentServer->getIpAddress();

        # Assert
        $this->assertEquals('0.0.0.0', $ipAddress);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::getSshUsername()
     * @throws \Exception
     */
    public function testGetSshUsername() {
        # Arrange
        $serverConfig = [
            'env' => 'unit',
            'type' => 'web',
            'ipAddress' => '0.0.0.0',
            'sshUsername' => 'username',
            'deployDirectory' => '/home/ubuntu/repos/ER',
        ];
        $environmentServer = new EnvironmentServer($serverConfig);

        # Act
        $sshUsername = $environmentServer->getSshUsername();

        # Assert
        $this->assertEquals('username', $sshUsername);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig\EnvironmentServer::getDeployDirectory()
     * @throws \Exception
     */
    public function testGetDeployDirectory() {
        # Arrange
        $serverConfig = [
            'env' => 'unit',
            'type' => 'web',
            'ipAddress' => '0.0.0.0',
            'sshUsername' => 'username',
            'deployDirectory' => '/home/ubuntu/repos/ER',
        ];
        $environmentServer = new EnvironmentServer($serverConfig);

        # Act
        $deployDirectory = $environmentServer->getDeployDirectory();

        # Assert
        $this->assertEquals('/home/ubuntu/repos/ER', $deployDirectory);
    }
}
