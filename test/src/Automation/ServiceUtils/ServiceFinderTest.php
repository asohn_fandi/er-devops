<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\ServiceUtils\ServiceFinder;
use PHPUnit\Framework\TestCase;

final class ServiceFinderTest extends TestCase
{
    private $testDirectory = '/tmp/testServiceFinder';

    protected function setUp() {
        mkdir($this->testDirectory);
        mkdir($this->testDirectory."/D");
        touch($this->testDirectory."/A.txt");
        touch($this->testDirectory."/B.php");
        touch($this->testDirectory."/C.php");
        touch($this->testDirectory."/D/E.php");
    }

    protected function tearDown() {
        unlink($this->testDirectory."/D/E.php");
        unlink($this->testDirectory."/A.txt");
        unlink($this->testDirectory."/B.php");
        unlink($this->testDirectory."/C.php");
        rmdir($this->testDirectory."/D");
        rmdir($this->testDirectory);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\ServiceFinder::find()
     * @covers \Fie\Automation\Shell\ExecuteCommand::run()
     */
    public function testFind() {
        # Arrange
        $expected = ['B', 'C', 'D/E'];
        $serviceFinder = new ServiceFinder();

        # Act
        $result = $serviceFinder->find($this->testDirectory);
        sort($expected);
        sort($result);

        # Assert
        $this->assertSame($expected, $result);
    }
}
