<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\ServiceUtils\Options;
use PHPUnit\Framework\TestCase;

final class OptionsTest extends TestCase
{
    private $knownOptionsTest = [
        'Common' => [
            'f'  => 'Some flag.',
            'o'  => 'Some other flag.',
            'do' => 'Some option.',
        ],
        'Some Section' => [
            'goodOption=true|false'  => 'Some long option with value.',
            'goodOption2=true|false' => 'Some other long option with value.',
            'goodFlag'               => 'Some long flag',
            'goodFlag2'              => 'Some other long flag',
        ],
    ];

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::__construct()
     */
    public function testInit() {
        # Arrange
        $_SERVER['HTTP_USER_AGENT'] = 'something';
        $_SERVER['REQUEST_URI'] = '/api';

        # Act
        $options = new Options();

        # Assert
        $this->assertInstanceOf(Options::class, $options);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::getKnownOptionsGlobal()
     */
    public function testGetKnownOptionsGlobal() {
        # Arrange
        $options = new Options();

        # Act
        $knownOptionsGlobal = $options->getKnownOptionsGlobal();

        # Assert
        $this->assertSame(['help', 'verbose', 'debug'], $knownOptionsGlobal);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::getArgv()
     * @dataProvider dataGetArgv
     * @param $argv
     * @param $expected
     * @throws \Exception
     */
    public function testGetArgv($argv, $expected) {
        # Arrange
        $GLOBALS['argv'] = $argv;

        # Act
        $isPhpUnit = Options::getArgv();

        # Assert
        $this->assertSame($expected, $isPhpUnit);
    }
    public function dataGetArgv() {
        return [
            'empty argv'                   => [[],                                 []],
            'not phpunit'                  => [['bogus'],                          ['bogus']],
            'phpunit'                      => [['phpunit'],                        []],
            'phpunit vendor'               => [['vendor/bin/phpunit'],             []],
            'php-prefix not phpunit'       => [['php', 'bogus'],                   ['php', 'bogus']],
            'php-prefix phpunit'           => [['php', 'phpunit'],                 []],
            'php-prefix phpunit vendor'    => [['php', 'vendor/bin/phpunit'],      []],
            'php-prefix phpunit more args' => [['php', 'phpunit', 'more', 'args'], []],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::ensureKnownOptions()
     * @dataProvider dataProcessOptionsThrows
     * @param $argv
     * @param $unknownOptionName
     * @throws \Exception
     */
    public function testProcessOptionsThrows($argv, $unknownOptionName) {
        # Assert (expect)
        $GLOBALS['argv'] = $argv;
        $this->expectExceptionMessageRegExp("/'$unknownOptionName'/i");

        # Arrange
        $options = (new Options())->setKnownOptions($this->knownOptionsTest);

        # Act
        $options->processOptions();
    }
    public function dataProcessOptionsThrows() {
        return [
            'unknown option' => [['--unknownOption=someValue'], 'unknownOption'],
            'unknown flag'   => [['--unknownFlag'],             'unknownFlag'],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::ensureKnownOptions()
     * @throws \Exception
     */
    public function testProcessOptionsValidNoOptions() {
        # Arrange
        $options = new Options();

        # Act
        $result = $options->processOptions();

        # Assert
        $this->assertInstanceOf(Options::class, $result);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::ensureKnownOptions()
     * @dataProvider dataProcessOptionsValidWithGiven
     * @param $givenOptions
     * @throws \Exception
     */
    public function testProcessOptionsValidWithGiven($givenOptions) {
        # Arrange
        $options = new Options();

        # Act
        $result = $options->processOptions($givenOptions);

        # Assert
        $this->assertInstanceOf(Options::class, $result);
    }
    public function dataProcessOptionsValidWithGiven() {
        return [
            'known option' => [['goodOption=someValue']],
            'known flag'   => [['goodFlag']],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::ensureKnownOptions()
     * @dataProvider dataProcessOptionsValid
     * @param $argv
     * @throws \Exception
     */
    public function testProcessOptionsValid($argv) {
        # Arrange
        $GLOBALS['argv'] = $argv;
        $options = (new Options())->setKnownOptions($this->knownOptionsTest);

        # Act
        $result = $options->processOptions();

        # Assert
        $this->assertInstanceOf(Options::class, $result);
    }
    public function dataProcessOptionsValid() {
        return [
            'known option' => [['--goodOption=someValue']],
            'known flag'   => [['--goodFlag']],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @dataProvider dataProcessOptionsValidTwice
     * @param $argv
     * @throws \Exception
     */
    public function testProcessOptionsValidTwice($argv) {
        # Arrange
        $GLOBALS['argv'] = $argv;
        $options = (new Options())->setKnownOptions($this->knownOptionsTest);

        # Act
        $result = $options
            ->processOptions()
            ->processOptions();

        # Assert
        $this->assertInstanceOf(Options::class, $result);
    }
    public function dataProcessOptionsValidTwice() {
        return [
            'known option' => [['--goodOption=someValue']],
            'known flag'   => [['--goodFlag']],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::parseWebQueryStringIntoCliParameters()
     * @covers \Fie\Automation\ServiceUtils\Options::parseCommandLineOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::ensureKnownOptions()
     * @dataProvider dataProcessOptionsEnvironmentThrows
     * @param $queryString
     * @param $argv
     * @param $unknownOptionName
     * @throws \Exception
     */
    public function testProcessOptionsEnvironmentThrows($queryString, $argv, $unknownOptionName) {
        # Assert (expect)
        $this->expectExceptionMessageRegExp("/'$unknownOptionName'/i");

        # Arrange
        $_SERVER['QUERY_STRING'] = $queryString;
        $GLOBALS['argv'] = $argv;
        $options = (new Options())->setKnownOptions($this->knownOptionsTest);

        # Act
        $options->processOptions();
    }
    public function dataProcessOptionsEnvironmentThrows() {
        return [
            'query string unknown option'            => ['unknownOption=true',             [], 'unknownOption'],
            'query string unknown flag'              => ['unknownFlag',                    [], 'unknownFlag'],
            'query string unknown option and flag'   => ['unknownOption=true&unknownFlag', [], 'unknownOption'],
            'query string unknown option cli format' => ['--unknownOption=true',           [], 'unknownOption'],

            'argv unknown option'          => [null, ['--unknownOption=true'],                  'unknownOption'],
            'argv unknown flag'            => [null, ['--unknownFlag'],                         'unknownFlag'],
            'argv unknown option and flag' => [null, ['--unknownOption=true', '--unknownFlag'], 'unknownOption'],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::parseWebQueryStringIntoCliParameters()
     * @covers \Fie\Automation\ServiceUtils\Options::parseCommandLineOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::ensureKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::getRawOptions()
     * @dataProvider dataProcessOptionsEnvironmentValidQueryString
     * @param $queryString
     * @param $expected
     * @throws \Exception
     */
    public function testProcessOptionsEnvironmentValidQueryString($queryString, $expected) {
        # Arrange
        $_SERVER['QUERY_STRING'] = $queryString;
        $options = (new Options())->setKnownOptions($this->knownOptionsTest);

        # Act
        $options->processOptions();
        $rawOptions = $options->getRawOptions();

        # Assert
        $this->assertEquals($expected, $rawOptions, '', 0.0, 10, true, true);
    }
    public function dataProcessOptionsEnvironmentValidQueryString() {
        return [
            'query string empty'                   => [null,                       []],
            'query string empty string'            => ['',                         []],
            'query string valid option'            => ['goodOption=true',          ['goodOption' => 'true']],
            'query string valid flag'              => ['goodFlag',                 ['goodFlag' => null]],
            'query string valid flag cli format'   => ['-f',                       ['f' => null]],
            'query string valid option and flag'   => ['goodOption=true&goodFlag', ['goodOption' => 'true', 'goodFlag' => null]],
            'query string valid option cli format' => ['--goodOption=true',        ['goodOption' => 'true']],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::parseWebQueryStringIntoCliParameters()
     * @covers \Fie\Automation\ServiceUtils\Options::parseCommandLineOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::ensureKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::getRawOptions()
     * @dataProvider dataProcessOptionsEnvironmentValidArgv
     * @param $argv
     * @param $expected
     * @throws \Exception
     */
    public function testProcessOptionsEnvironmentValidArgv($argv, $expected) {
        # Arrange
        $GLOBALS['argv'] = $argv;
        $options = (new Options())->setKnownOptions($this->knownOptionsTest);

        # Act
        $options->processOptions();
        $rawOptions = $options->getRawOptions();

        # Assert
        $this->assertEquals($expected, $rawOptions, '', 0.0, 10, true, true);
    }
    public function dataProcessOptionsEnvironmentValidArgv() {
        return [
            'argv empty array'                           => [[], []],
            'argv parameter'                             => [['param'],                           [0 => 'param']],
            'argv valid long option'                     => [['--goodOption=true'],               ['goodOption' => 'true']],
            'argv valid long option empty value'         => [['--goodOption='],                   ['goodOption' => null]],
            'argv valid long option space delimiter'     => [['--goodOption', 'true'],            ['goodOption' => 'true']],
            'argv valid long option followed by another' => [['--goodOption', '--goodOption2'],   ['goodOption' => null, 'goodOption2' => null]],
            'argv valid long flag'                       => [['--goodFlag'],                      ['goodFlag' => null]],
            'argv valid flag with value'                 => [['-f', 'true'],                      ['f' => 'true']],
            'argv valid flag followed by another'        => [['-f', '-o'],                        ['f' => null, 'o' => null]],
            'argv valid flag'                            => [['-f'],                              ['f' => null]],
            'argv valid option and flag'                 => [['--goodOption=true', '--goodFlag'], ['goodOption' => 'true', 'goodFlag' => null]],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::parseWebQueryStringIntoCliParameters()
     * @covers \Fie\Automation\ServiceUtils\Options::parseCommandLineOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::ensureKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::getOptionFlagValue()
     * @dataProvider dataProcessOptionsEnvironmentValidArgvFlagIncrement
     * @param $argv
     * @param $expected
     * @throws \Exception
     */
    public function testProcessOptionsEnvironmentValidArgvFlagIncrement($argv, $expected) {
        # Arrange
        $GLOBALS['argv'] = $argv;
        $options = (new Options())->setKnownOptions($this->knownOptionsTest);

        # Act
        $options->processOptions();
        $flagValue = $options->getOptionFlagValue('goodFlag');

        # Assert
        $this->assertSame($expected, $flagValue);
    }
    public function dataProcessOptionsEnvironmentValidArgvFlagIncrement() {
        return [
            'flag initial'   => [['--goodFlag'],                             1],
            'flag increment' => [['--goodFlag', '--goodFlag', '--goodFlag'], 3],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::parseWebQueryStringIntoCliParameters()
     * @covers \Fie\Automation\ServiceUtils\Options::parseCommandLineOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::getOptionFlag()
     * @covers \Fie\Automation\ServiceUtils\Options::ensureKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::getRawFlagValues()
     * @dataProvider dataProcessOptionsDebugVerboseValid
     * @param $argv
     * @param $expected
     * @throws \Exception
     */
    public function testProcessOptionsDebugVerboseValid($argv, $expected) {
        # Arrange
        $GLOBALS['argv'] = $argv;
        $options = (new Options())->setKnownOptions($this->knownOptionsTest);

        # Act
        $options->processOptions();
        $rawFlagValues = $options->getRawFlagValues();

        # Assert
        $this->assertEquals($expected, $rawFlagValues, '', 0.0, 10, true, true);
    }
    public function dataProcessOptionsDebugVerboseValid() {
        return [
            'debug (implies verbose)' => [['--debug'],                           ['verbose' => 1, 'debug' => 1]],
            'verbose'                 => [['--verbose'],                         ['verbose' => 1]],
            'more verbose'            => [['--verbose', '--verbose'],            ['verbose' => 2]],
            'debug and more verbose'  => [['--verbose', '--verbose', '--debug'], ['verbose' => 3, 'debug' => 1]],
            'debug and verbose'       => [['--verbose', '--debug'],              ['verbose' => 2, 'debug' => 1]],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::getOption()
     * @throws \Exception
     */
    public function testGetOption() {
        # Arrange
        $GLOBALS['argv'] = ['--goodOption=true'];
        $options = (new Options())
            ->setKnownOptions($this->knownOptionsTest)
            ->processOptions();

        # Act
        $result = $options->getOption('goodOption');

        # Assert
        $this->assertSame('true', $result);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::getOptionBoolean()
     * @dataProvider dataGetOptionBoolean
     * @param $expected
     * @param $argv
     * @throws \Exception
     */
    public function testGetOptionBoolean($expected, $argv) {
        # Arrange
        $GLOBALS['argv'] = $argv;
        $options = (new Options())
            ->setKnownOptions($this->knownOptionsTest)
            ->processOptions();

        # Act
        $result = $options->getOptionBoolean('goodOption');

        # Assert
        $this->assertEquals($expected, $result);
    }
    public function dataGetOptionBoolean() {
        return [
            'empty'            => [null,  ['--goodOption']],
            'bogus'            => [null,  ['--goodOption=bogus']],
            'true lowercase'   => [true,  ['--goodOption=true']],
            'true mixed case'  => [true,  ['--goodOption=tRue']],
            'false lowercase'  => [false, ['--goodOption=false']],
            'false mixed case' => [false, ['--goodOption=faLse']],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::getOptionFlag()
     * @throws \Exception
     */
    public function testGetOptionFlag() {
        # Arrange
        $GLOBALS['argv'] = ['--do'];
        $options = (new Options())
            ->setKnownOptions($this->knownOptionsTest)
            ->processOptions();

        # Act
        $result = $options->getOptionFlag('do');

        # Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::getOptionFlagValue()
     * @throws \Exception
     */
    public function testGetOptionFlagValue() {
        # Arrange
        $_SERVER['QUERY_STRING'] = 'verbose&verbose&verbose';
        $options = (new Options())
            ->setKnownOptions($this->knownOptionsTest)
            ->processOptions();

        # Act
        $result = $options->getOptionFlagValue('verbose');

        # Assert
        $this->assertSame(3, $result);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::requireFlag()
     * @covers \Fie\Automation\ServiceUtils\Options::missingFlag()
     * @throws \Exception
     */
    public function testProcessRequireFlagThrows() {
        # Assert (expect)
        $this->expectExceptionMessage("Missing flag 'do'");

        # Arrange
        $options = new Options();

        # Act
        $options->requireFlag('do');
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::requireFlag()
     * @throws \Exception
     */
    public function testProcessRequireFlagValid() {
        # Arrange
        $GLOBALS['argv'] = ['--do'];
        $options = (new Options())
            ->setKnownOptions($this->knownOptionsTest)
            ->processOptions();

        # Act
        $result = $options->requireFlag('do');

        # Assert
        $this->assertTrue($result);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::requireOption()
     * @covers \Fie\Automation\ServiceUtils\Options::missingOption()
     * @throws \Exception
     */
    public function testProcessRequireOptionThrows() {
        # Assert (expect)
        $this->expectExceptionMessage("Missing key/value pair do=true|false");

        # Arrange
        $options = new Options();

        # Act
        $options->requireOption('do', 'true|false');
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::requireOption()
     * @throws \Exception
     */
    public function testProcessRequireOptionValid() {
        # Arrange
        $GLOBALS['argv'] = ['--do=true'];
        $options = (new Options())
            ->setKnownOptions($this->knownOptionsTest)
            ->processOptions();

        # Act
        $result = $options->requireOption('do', 'true|false');

        # Assert
        $this->assertSame('true', $result);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::requireOneOption()
     * @covers \Fie\Automation\ServiceUtils\Options::missingOption()
     * @throws \Exception
     */
    public function testProcessRequireOneOptionThrows() {
        # Assert (expect)
        $this->expectExceptionMessage("Missing key/value pair do=true|false or goodFlag=true|false");

        # Arrange
        $options = new Options();

        # Act
        $options->requireOneOption(['do', 'goodFlag'], 'true|false');
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::processOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::requireOneOption()
     * @throws \Exception
     */
    public function testProcessRequireOneOptionValid() {
        # Arrange
        $GLOBALS['argv'] = ['--goodFlag=true'];
        $options = (new Options())
            ->setKnownOptions($this->knownOptionsTest)
            ->processOptions();

        # Act
        $result = $options->requireOneOption(['do', 'goodFlag'], 'true|false');

        # Assert
        $this->assertSame('true', $result);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Options::setKnownOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::parseWebQueryStringIntoCliParameters()
     * @covers \Fie\Automation\ServiceUtils\Options::parseCommandLineOptions()
     * @covers \Fie\Automation\ServiceUtils\Options::sanitizeOptionValues()
     * @dataProvider dataSanitizeOptionValue
     * @param $queryString
     * @param $expected
     * @throws \Exception
     */
    public function testSanitizeOptionValues($queryString, $expected) {
        # Arrange
        $_SERVER['QUERY_STRING'] = $queryString;
        $options = (new Options())->setKnownOptions($this->knownOptionsTest);

        # Act
        $options->processOptions();
        $rawOptions = $options->getRawOptions();

        # Assert
        $this->assertEquals($expected, $rawOptions, '', 0.0, 10, true, true);
    }
    public function dataSanitizeOptionValue() {
        return [
            'normal'                   => ['goodOption=foo',         ['goodOption' => 'foo']],
            'whitespace'               => ['goodOption=foo bar',     ['goodOption' => 'foo']],
            'whitespaces'              => ['goodOption=foo bar baz', ['goodOption' => 'foo']],
            'whitespace encoded'       => ['goodOption=foo%20bar',   ['goodOption' => 'foo']],
            'whitespace encoded twice' => ['goodOption=foo%2520bar', ['goodOption' => 'foo']],
        ];
    }
}
