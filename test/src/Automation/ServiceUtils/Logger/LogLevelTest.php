<?php

namespace Fie\Test\Automation\ServiceUtils\Logger;

use Fie\Automation\ServiceUtils\Logger\LogLevel;
use PHPUnit\Framework\TestCase;

final class LogLevelTest extends TestCase
{
    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\LogLevel::set()
     * @covers \Fie\Automation\ServiceUtils\Logger\LogLevel::get()
     * @dataProvider dataSet
     * @param $setLevelTo
     * @param $expectedLevel
     * @param $expectedLevelAboveSilent
     */
    public function testSet($setLevelTo, $expectedLevel, $expectedLevelAboveSilent) {
        # Arrange
        $logLevel = new LogLevel();

        # Act
        $logLevel->set($setLevelTo);
        $level = $logLevel->get();
        $levelAboveSilent = $logLevel->get(false);

        # Assert
        $this->assertSame($expectedLevel, $level);
        $this->assertSame($expectedLevelAboveSilent, $levelAboveSilent);
    }
    public function dataSet() {
        return [
            'level -2' => [-2, -2, 0],
            'level -1' => [-1, -1, 0],
            'level 0'  => [0,   0, 0],
            'level 1'  => [1,   1, 1],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\LogLevel::lock()
     * @covers \Fie\Automation\ServiceUtils\Logger\LogLevel::set()
     * @covers \Fie\Automation\ServiceUtils\Logger\LogLevel::get()
     * @dataProvider dataSetWhileLocked
     * @param $setLevelTo
     * @param $expectedLevel
     * @param $expectedLevelAboveSilent
     */
    public function testSetWhileLocked($setLevelTo, $expectedLevel, $expectedLevelAboveSilent) {
        # Arrange
        $logLevel = (new LogLevel())->lock();

        # Act
        $logLevel->set($setLevelTo);
        $level = $logLevel->get();
        $levelAboveSilent = $logLevel->get(false);

        # Assert
        $this->assertSame($expectedLevel, $level);
        $this->assertSame($expectedLevelAboveSilent, $levelAboveSilent);
    }
    public function dataSetWhileLocked() {
        return [
            'level -2' => [-2, 0, 0],
            'level -1' => [-1, 0, 0],
            'level 0'  => [0,  0, 0],
            'level 1'  => [1,  0, 1],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\LogLevel::lock()
     * @covers \Fie\Automation\ServiceUtils\Logger\LogLevel::unlock()
     */
    public function testUnlock() {
        # Arrange
        $logLevel = new LogLevel();

        # Act
        $logLevel->set(123);
        $levelBefore = $logLevel->get();
        $logLevel->lock();
        $logLevel->set(456);
        $levelDuring = $logLevel->get();
        $logLevel->unlock();
        $logLevel->set(789);
        $levelAfter = $logLevel->get();

        # Assert
        $this->assertSame(123, $levelBefore, 'initial log level');
        $this->assertSame(123, $levelDuring, 'level during lock');
        $this->assertSame(789, $levelAfter, 'level after unlock');
    }
}
