<?php

namespace Fie\Test\Automation\ServiceUtils\Logger;

use Fie\Automation\ServiceUtils\Logger\ColorFormat;
use Fie\Automation\ServiceUtils\Logger;
use Fie\Automation\ServiceUtils\Options;
use PHPUnit\Framework\TestCase;

final class ColorFormatTest extends TestCase
{

    private $noColorWarning  = '/^%s$/';
    private $noColorSuccess  = '/^%s$/';
    private $noColorDebug    = '/^DEBUG/';
    private $webColorWarning = '/span.*:red;/';
    private $webColorSuccess = '/span.*:blue;/';
    private $webColorDebug   = '/span.*:gold;/';
    private $cliColorWarning = '/\[1;31m/';
    private $cliColorSuccess = '/\[1;36m/';
    private $cliColorDebug   = '/\[1;33m/';

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::__construct()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::setIoMode()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::setup()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::__construct()
     */
    public function testInit() {
        # Arrange/Act
        $colorFormat = new ColorFormat();

        # Assert
        $this->assertInstanceOf(ColorFormat::class, $colorFormat);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::setIoMode()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::setup()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::webColors()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::cliColors()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::getCurrentColorFormat()
     * @dataProvider dataSetupColor
     * @param bool $webOverride
     * @param bool $interactiveOverride
     * @param bool $setupForce
     * @param string $expectedRegExpWarning
     * @param string $expectedRegExpSuccess
     * @param string $expectedRegExpDebug
     */
    public function testSetupColor($setupForce, $webOverride, $interactiveOverride, $expectedRegExpWarning, $expectedRegExpSuccess, $expectedRegExpDebug) {
        # Arrange
        $fauxIoMode = new class($interactiveOverride) {
            private $interactiveOverride;
            public function __construct(?bool $interactiveOverride) {
                $this->interactiveOverride = $interactiveOverride;
            }
            public function isInteractive() {
                return $this->interactiveOverride;
            }
        };
        $colorFormat = (new ColorFormat())
            ->setWebOverride($webOverride)
            ->setIoMode($fauxIoMode);

        # Act
        $colorFormat->setup(true, $setupForce);

        # Assert
        $currentColorFormat = $colorFormat->getCurrentColorFormat();
        $this->assertRegExp($expectedRegExpWarning, $currentColorFormat['colorFormatWarning']);
        $this->assertRegExp($expectedRegExpSuccess, $currentColorFormat['colorFormatSuccess']);
        $this->assertRegExp($expectedRegExpDebug,   $currentColorFormat['colorFormatDebug']);
    }
    public function dataSetupColor() {
        # regex variables
        return [
            'force web'                            => [true,  true,   null,   $this->webColorWarning, $this->webColorSuccess, $this->webColorDebug],
            'force web-false'                      => [true,  false,  null,   $this->cliColorWarning, $this->cliColorSuccess, $this->cliColorDebug],
            'no force, web'                        => [false, true,   null,   $this->webColorWarning, $this->webColorSuccess, $this->webColorDebug],
            'no force, web-false, not interactive' => [false, false,  false,  $this->cliColorWarning, $this->cliColorSuccess, $this->cliColorDebug],
            'no force, web-false, interactive'     => [false, false,  true,   $this->noColorWarning,  $this->noColorSuccess,  $this->noColorDebug],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::setup()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::noColors()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::getCurrentColorFormat()
     */
    public function testSetupColorNoColor() {
        # Arrange
        $colorFormat = new ColorFormat();

        # Act
        $colorFormat->setup(false);

        # Assert
        $currentColorFormat = $colorFormat->getCurrentColorFormat();
        $this->assertRegExp($this->noColorWarning, $currentColorFormat['colorFormatWarning']);
        $this->assertRegExp($this->noColorSuccess, $currentColorFormat['colorFormatSuccess']);
        $this->assertRegExp($this->noColorDebug,   $currentColorFormat['colorFormatDebug']);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::setWebOverride()
     * @covers \Fie\Automation\ServiceUtils\Logger::setup()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::setup()
     * @throws \Exception
     */
    public function testConfigureOptionsColorEnabled() {
        # Arrange
        $GLOBALS['argv'] = ['--color=true'];
        $options = (new Options())->processOptions();

        # Act
        $logger = (new Logger())
            ->setWebOverride(true)
            ->setup($options);

        # Assert
        $colorFormat = $logger->colorFormat->getCurrentColorFormat();
        $this->assertContains('style=', $colorFormat['colorFormatWarning']);
        $this->assertContains('style=', $colorFormat['colorFormatSuccess']);
        $this->assertContains('style=', $colorFormat['colorFormatDebug']);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger::setup()
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::setup()
     * @throws \Exception
     */
    public function testConfigureOptionsColorDisabled() {
        # Arrange
        $GLOBALS['argv'] = ['--color=false'];
        $options = (new Options())->processOptions();

        # Act
        $logger = (new Logger())
            ->setWebOverride(true)
            ->setup($options);

        # Assert
        $currentColorFormat = $logger->colorFormat->getCurrentColorFormat();
        $this->assertNotContains('style=', $currentColorFormat['colorFormatWarning']);
        $this->assertNotContains('style=', $currentColorFormat['colorFormatSuccess']);
        $this->assertNotContains('style=', $currentColorFormat['colorFormatDebug']);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::isWeb()
     */
    public function testIsWebOverride() {
        # Arrange
        $colorFormat = (new ColorFormat())->setWebOverride(true);

        # Act
        $isWeb = $colorFormat->isWeb();

        # Assert
        $this->assertTrue($isWeb);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::isWeb()
     * @dataProvider dataIsWeb
     * @param bool|string $expected
     * @param bool|string $httpUserAgent
     */
    public function testIsWeb($expected, $httpUserAgent) {
        # Arrange
        $_SERVER['HTTP_USER_AGENT'] = $httpUserAgent;
        $colorFormat = new ColorFormat();

        # Act
        $isWeb = $colorFormat->isWeb();

        # Assert
        $this->assertEquals($expected, $isWeb);
    }
    public function dataIsWeb() {
        return [
            'user agent is set'     => [true,  'something'],
            'user agent is not set' => [false, null],
        ];
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Logger\ColorFormat::setWebOverride()
     * @dataProvider dataSetWebOverride
     * @param $override
     */
    public function testSetWebOverride($override) {
        # Arrange
        $colorFormat = (new ColorFormat())->setWebOverride($override);

        # Act
        $isWeb = $colorFormat->isWeb();

        # Assert
        $this->assertEquals($override, $isWeb);
    }
    public function dataSetWebOverride() {
        return [
            'web true'  => [true],
            'web false' => [false],
        ];
    }
}
