<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\Service;
use Fie\Automation\ServiceUtils\LockFile;
use Fie\Automation\ServiceUtils\Logger;
use Fie\Test\Automation\SimpleService;
use PHPUnit\Framework\TestCase;

final class LockFileTest extends TestCase
{
    private $lockfile = '/tmp/automation_servicename.lock';

    public function setUp() {
        if (file_exists($this->lockfile)) {
            unlink($this->lockfile);
        }
    }

    public function tearDown() {
        if (file_exists($this->lockfile)) {
            unlink($this->lockfile);
        }
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\LockFile::create()
     * @throws \Exception
     */
    public function testCreate() {
        # Arrange
        $lockFile = new LockFile();

        # Act
        $lockFile->create($this->lockfile);

        # Assert
        $this->assertFileExists($this->lockfile);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\LockFile::create()
     * @throws \Exception
     */
    public function testCreateOrphaned() {
        # Arrange
        $logger = (new Logger())->silent();
        $service = new Service($logger, new SimpleService());
        touch($this->lockfile);
        file_put_contents($this->lockfile, 'BadPID');

        # Act
        $returnStatus = $service->run();

        # Assert
        $this->assertTrue($returnStatus);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\LockFile::create()
     * @covers \Fie\Automation\Service::run()
     * @throws \Exception
     */
    public function testCreateRunning() {
        # Arrange
        $logger = (new Logger())->silent();
        $service = new Service($logger, new SimpleService());
        touch($this->lockfile);
        file_put_contents($this->lockfile, getmypid());

        # Act
        $returnStatus = $service->run();

        # Assert
        $this->assertFalse($returnStatus);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\LockFile::delete()
     * @throws \Exception
     */
    public function testDelete() {
        # Arrange
        $lockFile = (new LockFile())->create($this->lockfile);

        # Act
        $lockFile->delete();

        # Assert
        $this->assertFileNotExists($this->lockfile);
    }

    /**
     * @covers \Fie\Automation\Service::__construct()
     * @covers \Fie\Automation\ServiceUtils\LockFile::create()
     * @covers \Fie\Automation\Service::__destruct()
     * @throws \Exception
     */
    public function testDeletedByService() {
        # Arrange
        $logger = (new Logger())->silent();
        $service = new Service($logger, new SimpleService());

        # Act
        $service->run();
        unset($service);

        # Assert
        $this->assertFileNotExists($this->lockfile);
    }
}
