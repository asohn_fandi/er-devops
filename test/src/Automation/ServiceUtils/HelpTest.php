<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\ServiceUtils\Help;
use PHPUnit\Framework\TestCase;

final class HelpTest extends TestCase
{
    /**
     * @covers \Fie\Automation\ServiceUtils\Help::getText()
     */
    public function testPrintWebAPI() {
        # Arrange
        $_SERVER['HTTP_USER_AGENT'] = 'something';
        $_SERVER['REQUEST_URI'] = '/api';
        $help = new Help();

        # Act
        $output = $help->getText();
        $output = explode("\n", $output);

        # Assert
        $this->assertCount(1, $output);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Help::getText()
     */
    public function testPrintWeb() {
        # Arrange
        $_SERVER['HTTP_USER_AGENT'] = 'something';
        $help = new Help();

        # Act
        $output = $help->getText();
        $output = explode("\n", $output);
        $numLines = count($output);

        # Assert
        $this->assertGreaterThan(2, $numLines);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Help::getText()
     */
    public function testPrintCli() {
        # Arrange
        $help = new Help();

        # Act
        $output = $help->getText();
        $output = explode("\n", $output);
        $numLines = count($output);

        # Assert
        $this->assertGreaterThan(2, $numLines);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Help::setOptionsCommon()
     * @covers \Fie\Automation\ServiceUtils\Help::setOptionsService()
     * @covers \Fie\Automation\ServiceUtils\Help::setServiceHelpText()
     * @covers \Fie\Automation\ServiceUtils\Help::getText()
     * @covers \Fie\Automation\ServiceUtils\Help::getFormattedOptions()
     * @covers \Fie\Automation\ServiceUtils\Help::getServiceDescription()
     */
    public function testPrintOptions() {
        # Arrange
        $help = (new Help())
            ->setOptionsCommon([ 'Common'  => ['some_option' => 'some_description']])
            ->setOptionsService(['Service' => ['some_option' => 'some_description']])
            ->setServiceHelpText('some text');

        # Act
        $output = $help->getText();
        $output = explode("\n", $output);
        $numLines = count($output);

        # Assert
        $this->assertGreaterThan(2, $numLines);
    }

    /**
     * @covers \Fie\Automation\ServiceUtils\Help::setOptionsCommon()
     * @covers \Fie\Automation\ServiceUtils\Help::setOptionsService()
     * @covers \Fie\Automation\ServiceUtils\Help::setServiceHelpText()
     * @covers \Fie\Automation\ServiceUtils\Help::getText()
     * @covers \Fie\Automation\ServiceUtils\Help::getFormattedOptions()
     * @covers \Fie\Automation\ServiceUtils\Help::getServiceDescription()
     */
    public function testPrintOptionsNone() {
        # Arrange
        $help = (new Help())
            ->setOptionsCommon([ 'Common' => []])
            ->setOptionsService(['Service' => []])
            ->setServiceHelpText('');

        # Act
        $output = $help->getText();
        $output = explode("\n", $output);
        $numLines = count($output);

        # Assert
        $this->assertGreaterThan(2, $numLines);
    }
}
