<?php

namespace Fie\Test\Automation\ServiceUtils;

use Fie\Automation\ServiceUtils\BuildConfig;
use PHPUnit\Framework\TestCase;

final class BuildConfigTestIntegration extends TestCase
{
    /**
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::__construct()
     * @covers \Fie\Automation\ServiceUtils\BuildConfig::validateConfig()
     * @throws \Exception
     */
    public function testValidateConfig() {
        # Arrange/Act
        $buildConfig = new BuildConfig(true);

        # Assert
        $this->assertInstanceOf(BuildConfig::class, $buildConfig);
    }
}
