# ER Devops

DevOps for F&I Express. Trial run in Express Recoveries. We'll expand from there.

Services run mutually exclusive with service-level lockfiles to prevent race conditions (e.g. tagging a merge with the next patch version).
## Help

General Help and List of Services:  
CLI: `php app/CliService.php --help`  
Web: `http://<hostname>/index.php?help`  

Service Help:  
CLI Example: `php app/CliService.php Git/BranchDeploy --help`  
Web HTML Example: `http://<hostname>/Git/BranchDeploy?help`  
Web JSON Example: `http://<hostname>/api/Git/BranchDeploy?help`  

## Service Flags

These may be used in a web query string in the format of `&parameter` or `&parameter=value` where applicable.  
Parameters without `=value` suffix will be considered `true` when the service runs.

Documentation: `--help`  
Verbose Output: `--verbose`  
Debug Output (implies verbose): `--debug`  
Color Output: `--color=true|false`  

## Example Usage
  
CLI: `php app/CliService.php Git/BranchDeploy --repository=express-recoveries --branch=AutomationTest --environment=test_v2`  
Web HTML: `http://<hostname>/Git/BranchDeploy?repository=express-recoveries&branch=AutomationTest&environment=test_v2`  
Web JSON: `http://<hostname>/api/Git/BranchDeploy?repository=express-recoveries&branch=AutomationTest&environment=test_v2`  