<?php

namespace Fie\Automation\App;

use Exception;
use Fie\Automation\ServiceUtils\BuildConfig;
use SimpleXMLElement;

require_once(__DIR__ . '/../bootstrap.php');

$junitXML = __DIR__ . '/../Health/TestExecutionTime/junit.xml';
if (!file_exists($junitXML)) {
    error_log("Could not find junit.xml\n");
    exit(1);
}
$junit = new SimpleXMLElement(file_get_contents($junitXML));

try {
    $buildConfig = (new BuildConfig())->setStrict();
    $timeThresholdTest            = $buildConfig->getValue('health', 'timeThreshold', 'Test');
    $timeThresholdTestException   = $buildConfig->getValue('health', 'timeThreshold', 'TestException');
    $timeThresholdTestIntegration = $buildConfig->getValue('health', 'timeThreshold', 'TestIntegration');
} catch (Exception $e) {
    error_log(sprintf("BuildConfig ERROR! %s\n", $e->getMessage()));
    $timeThresholdTest            = 0.005;
    $timeThresholdTestException   = 0.01;
    $timeThresholdTestIntegration = 0.02;
}

function timeThresholdExceeded(string $className, float $time) {
    return $time >= timeThreshold($className);
}

function timeThreshold(string $className) {
    global $timeThresholdTest, $timeThresholdTestException, $timeThresholdTestIntegration;
    if (preg_match('/TestIntegration$/', $className)) {
        return $timeThresholdTestIntegration;
    }
    if (preg_match('/TestException$/', $className)) {
        return $timeThresholdTestException;
    }
    return $timeThresholdTest;
}

function iterateTestSuite(string $className, SimpleXMLElement $parent) {
    if ($parent->getName() == 'testsuite') {
        foreach ($parent->children() as $child) {
            if ($child->getName() == 'testsuite') {
                iterateTestSuite($className, $child); # usually, dataProviders
            } else {
                $testName = $child['name'];
                $hasAssertions = isset($child['assertions']) && $child['assertions'] > 0;
                $timeExceeded = timeThresholdExceeded($className, (float) $child['time']);
                $hasError   = isset($child->error);
                $hasFailure = isset($child->failure) || isset($child->warning);
                $hasSkipped = isset($child->skipped);
                ?>
                <tr class="<?php if (!$hasAssertions || $timeExceeded || $hasError || $hasFailure || $hasSkipped): ?>red<?php endif ?>">
                    <td><?= $testName ?></td>
                    <td class="dataPoint<?php if ($timeExceeded  ): ?> red<?php endif ?>"><?= $child['time']           ?>s</td>
                    <td class="dataPoint<?php if (!$hasAssertions): ?> red<?php endif ?>"><?= $child['assertions']     ?></td>
                    <td class="dataPoint<?php if ($hasError      ): ?> red<?php endif ?>"><?= $hasError   ? 'Yes' : '' ?></td>
                    <td class="dataPoint<?php if ($hasFailure    ): ?> red<?php endif ?>"><?= $hasFailure ? 'Yes' : '' ?></td>
                    <td class="dataPoint<?php if ($hasSkipped    ): ?> red<?php endif ?>"><?= $hasSkipped ? 'Yes' : '' ?></td>
                </tr>
                <?php
            }
        }
    }
}
?>
<!doctype html>
<html>
    <head>
        <title>DevOps - Test Execution Time</title>
        <style>
            h3 {margin-bottom: 3px;}
            table {border-spacing: 0;}
            table tfoot tr td.red {
                font-weight: bold;
                color: red;
            }
            th, td {
                text-align: left;
                font-family: "Courier New", Courier, monospace;
                padding: 1px 5px;
            }
            span.config {
                font-size: 10pt;
                font-weight: normal;
            }
            table.config td {font-size: 8pt;}
            table.data {
                border: 1px solid #dddddd;
                border-right-width: 0;
                border-bottom-width: 0;
            }
            table.data th,
            table.data td {
                border: 1px solid #dddddd;
                border-top-width: 0;
                border-left-width: 0;
            }
            table.data thead tr     th           {background-color: #dddddd;}
            table.data tbody tr     td           {background-color: #dff0d8;}
            table.data tbody tr.red td           {background-color: #f2dede;}
            table.data tbody tr.red td.red       {background-color: #d9534f;}
            table.data tfoot tr     td           {font-weight: bold;}
            table.data       tr     td.dataPoint {text-align: right;}
        </style>
    </head>
    <body>
        <table class="config">
            <tbody>
                <tr><td>Time threshold for Test:</td><td><?=            $timeThresholdTest            ?></td></tr>
                <tr><td>Time threshold for TestException:</td><td><?=   $timeThresholdTestException   ?></td></tr>
                <tr><td>Time threshold for TestIntegration:</td><td><?= $timeThresholdTestIntegration ?></td></tr>
            </tbody>
        </table>

        <?php foreach ($junit as $testSuite): ?>
            <h2>Test Suite: <?= $testSuite['name'] ?></h2>
            <table class="data">
                <thead><tr><th>Tests</th><th>Duration</th><th>Assertions</th><th>Errors</th><th>Failures</th><th>Skipped</th></tr></thead>
                <tfoot>
                    <tr>
                        <td class="dataPoint"><?= $testSuite['tests']      ?></td>
                        <td class="dataPoint"><?= $testSuite['time']       ?>s</td>
                        <td class="dataPoint"><?= $testSuite['assertions'] ?></td>
                        <td class="dataPoint<?php if ($testSuite['errors']   > 0): ?> red<?php endif ?>"><?= $testSuite['errors']   ?></td>
                        <td class="dataPoint<?php if ($testSuite['failures'] > 0): ?> red<?php endif ?>"><?= $testSuite['failures'] ?></td>
                        <td class="dataPoint<?php if ($testSuite['skipped']  > 0): ?> red<?php endif ?>"><?= $testSuite['skipped']  ?></td>
                    </tr>
                </tfoot>
            </table>
            <br /><br />

            <?php
            $orderedByTestClassName = [];
            foreach ($testSuite->children() as $testClass) {
                $orderedByTestClassName[] = $testClass;
            }
            usort($orderedByTestClassName, function($a, $b) {
                return strcmp(strtolower($a['name']), strtolower($b['name']));
            });

            foreach ($orderedByTestClassName as $testClass): ?>
                <h3>Test Class: <?= $testClass['name'] ?> <span class="config">Threshold: <?= timeThreshold($testClass['name']) ?></span></h3>
                <table class="data">
                    <thead>
                        <tr>
                            <th><?= $testClass['tests'] ?> Test<?= $testClass['tests'] == 1 ? '' : 's' ?></th>
                            <th>Duration</th><th>Assertions</th><th>Errors</th><th>Failures</th><th>Skipped</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php iterateTestSuite($testClass['name'], $testClass) ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td></td>
                            <td class="dataPoint"><?= $testClass['time'] ?>s</td>
                            <td class="dataPoint<?php if ($testClass['assertions'] == 0): ?> red<?php endif ?>"><?= $testClass['assertions'] ?></td>
                            <td class="dataPoint<?php if ($testClass['errors']      > 0): ?> red<?php endif ?>"><?= $testClass['errors']     ?></td>
                            <td class="dataPoint<?php if ($testClass['failures']    > 0): ?> red<?php endif ?>"><?= $testClass['failures']   ?></td>
                            <td class="dataPoint<?php if ($testClass['skipped']     > 0): ?> red<?php endif ?>"><?= $testClass['skipped']    ?></td>
                        </tr>
                    </tfoot>
                </table>
            <?php endforeach ?>
        <?php endforeach ?>
    </body>
</html>
