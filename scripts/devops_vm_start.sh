#!/bin/bash
# Usage: source devops_vm_start.sh

server_pid_file='/tmp/automation-server.pid'
if [ -f ${server_pid_file} ]; then
    server_pid=$(cat ${server_pid_file})
    kill ${server_pid} 2>/dev/null
    rm ${server_pid_file}
    unset server_pid
fi

trap "cd ~" EXIT HUP INT QUIT PIPE TERM
cd ~/repos/er-devops && \
    clear && \
    git -C ~/repos/er-devops clean -f && \
    git -C ~/repos/er-devops checkout . && \
    git -C ~/repos/er-devops pull && \
    perl -E 'say;say"="x80' && \
    composer install && \
    perl -E 'say;say"="x80'

php -S localhost:8011 &
echo $! > ${server_pid_file}
unset server_pid_file

cd ~ && \
    firefox localhost:8011 &