#!/bin/bash

# cd to root if necessary
echo "$(dirname "${BASH_SOURCE[0]}")" | grep scripts >/dev/null || cd ../

if [ ! "$(php -i | grep xdebug)" ]; then
    echo "xdebug not found!"
    echo "consider running \`brew install autoconf\` and \`pecl install xdebug\`"
    exit 1;
fi

for vendorDependency in phpcs phpcbf phpunit; do
    if [ ! -f vendor/bin/${vendorDependency} ]; then
        echo "vendor/bin/${vendorDependency} not found!"
        echo "consider running \`composer install\`"
        exit 1;
    fi
done

directoryHealth='Health'
directoryPhpMetrics="${directoryHealth}/PhpMetrics"
directoryCodeSniffer="${directoryHealth}/CodeSniffer"
directoryCodeCoverage="${directoryHealth}/CodeCoverage"
directoryCodeTestExecutionTime="${directoryHealth}/TestExecutionTime"

mkdir ${directoryPhpMetrics} 2>/dev/null
echo "Generating PhpMetics at '${directoryPhpMetrics}/index.html'..."
vendor/bin/phpmetrics --report-html=${directoryPhpMetrics} .

perl -E 'say;say;say"="x80'

mkdir ${directoryCodeSniffer} 2>/dev/null
for section in app src test; do
    echo "Generating code sniffer report at '${directoryCodeSniffer}/${section}.txt'..."
    [[ ${section} == "test" ]] && xml="CodeSnifferUnitTest.xml" || xml="CodeSniffer.xml"
    vendor/bin/phpcs -s --standard=$(pwd)/configs/${xml} --report-file=${directoryCodeSniffer}/${section}.txt ${section}
    if [ $? -eq 0 ] && [ $(wc -c < ${directoryCodeSniffer}/${section}.txt) -eq 1 ]; then
        echo "Clean!" > ${directoryCodeSniffer}/${section}.txt
    fi
    #vendor/bin/phpcbf -s --standard=$(pwd)/configs/${xml} ${section}
done
echo "Code sniffer reports generated:"
echo "    http://[hostname]/${directoryCodeSniffer}/app.txt"
echo "    http://[hostname]/${directoryCodeSniffer}/src.txt"
echo "    http://[hostname]/${directoryCodeSniffer}/test.txt"

perl -E 'say;say;say"="x80'

echo "Generating code coverage report at '${directoryCodeCoverage}'..."
echo
rm -rf ${directoryCodeCoverage} 2>/dev/null
rm -rf ${directoryCodeTestExecutionTime} 2>/dev/null
mkdir ${directoryCodeTestExecutionTime}
exec 5>&1
testResult=$( \
    vendor/bin/phpunit \
        --log-junit=${directoryCodeTestExecutionTime}/junit.xml \
        --coverage-html=${directoryCodeCoverage} \
        -c configs/phpunit.xml | \
    tee >(cat - >&5) \
)
echo "Code coverage report generated: http://[hostname]/${directoryCodeCoverage}/index.html"

perl -E 'say;say;say"="x80'

echo "Generating test execution time report at '${directoryCodeTestExecutionTime}'..."
echo
rm ${directoryCodeTestExecutionTime}/index.php 2>/dev/null
php scripts/parseJUnit.php 1> ${directoryCodeTestExecutionTime}/index.html
touch ${directoryCodeTestExecutionTime}/index.html
echo "Test execution time report generated: http:/[hostname]/${directoryCodeTestExecutionTime}/index.html"

perl -E 'say;say;say"="x80'

echo "Health Report URL: http://[hostname]/${directoryHealth}/index.php"
echo
echo "PHPUnit Summary:"
echo ${testResult} | grep -o --color=never 'Tests:.*Assertions:[^.]*\.'
echo ${testResult} | grep -o --color=never 'OK, but.*tests!'
echo ${testResult} | grep -o --color=never 'OK.*assertions)'
