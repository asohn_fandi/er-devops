<?php

namespace Fie\Bridge;

use Exception;
use Fie\Automation\ServiceUtils\BuildConfig;
use JiraRestApi\Configuration\ArrayConfiguration;
use JiraRestApi\Issue\Issue;
use JiraRestApi\Issue\IssueField;
use JiraRestApi\Issue\IssueService;
use JiraRestApi\Issue\Transition;

class Jira
{
    private $config;
    private $issueService;
    private $projectName;
    private $issueCustomFields;
    private $issueTransitions;
    private $issueTransitionsFields;
    private $subTaskCompletedStatuses;

    /**
     * @param string $repositoryName
     * @throws \Exception
     */
    public function __construct(string $repositoryName) {
        $buildConfig = new BuildConfig();
        $this->config = new ArrayConfiguration([
            'jiraHost'     => $buildConfig->getValue('jira', 'hostname'),
            'jiraUser'     => $buildConfig->getValue('jira', 'username'),
            'jiraPassword' => $buildConfig->getValue('jira', 'password'),
        ]);
        $this->issueService = new IssueService($this->config);

        $this->projectName = $buildConfig->getValue('git', 'repositoryConfigs', $repositoryName, 'jiraProject');

        if (!is_string($this->projectName)) {
            throw new Exception(sprintf("Could not determine JIRA project from given repository name '%s'", $repositoryName));
        }

        $this->issueCustomFields        = $buildConfig->getValue('jira', 'projectConfigs', $this->projectName, 'issueCustomFields');
        $this->issueTransitions         = $buildConfig->getValue('jira', 'projectConfigs', $this->projectName, 'issueTransitions');
        $this->issueTransitionsFields   = $buildConfig->getValue('jira', 'projectConfigs', $this->projectName, 'issueTransitionsFields');
        $this->subTaskCompletedStatuses = $buildConfig->getValue('jira', 'projectConfigs', $this->projectName, 'issueSubTask', 'completedStatuses');
    }

    /**
     * @param string $issueKey
     * @throws \JiraRestApi\JiraException
     * @throws \JsonMapper_Exception
     * @return \JiraRestApi\Issue\Issue
     */
    public function getIssueById(string $issueKey) {
        return $this->issueService->get($issueKey, [
            'expand' => ['issueTransitions'],
        ]);
    }

    /**
     * @param string $issueKey
     * @param string $transitionTo
     * @throws \Exception
     * @throws \JiraRestApi\JiraException
     * @throws \JsonMapper_Exception
     * @return \JiraRestApi\Issue\Issue
     */
    private function transitionIssue(string $issueKey, string $transitionTo) {
        $issue = $this->getIssueById($issueKey);

        if (!$this->transitionIsValid($issue, $transitionTo)) {
            throw new Exception(sprintf("Invalid transition '%s' for issue key '%s'. Valid transitions '%s'", $transitionTo, $issueKey, join("', '", $this->getPossibleTransitions($issue))));
        }

        $transition = new Transition();
        $transition->setTransitionName($transitionTo);
        $this->issueService->transition($issueKey, $transition);

        return $this->getIssueById($issueKey);
    }

    /**
     * @param string $issueKey
     * @throws \JiraRestApi\JiraException
     * @throws \JsonMapper_Exception
     * @return \JiraRestApi\Issue\Issue
     */
    public function transitionIssueNoCodeChanges(string $issueKey) {
        return $this->transitionIssue($issueKey, $this->issueTransitions['noCodeChanges']);
    }

    /**
     * @param string $issueKey
     * @param string $gitTag
     * @throws \Exception
     * @throws \JiraRestApi\JiraException
     * @throws \JsonMapper_Exception
     * @return \JiraRestApi\Issue\Issue
     */
    public function transitionIssueMerged(string $issueKey, string $gitTag) {
        $transitionTo = $this->issueTransitions['merged'];
        $issue = $this->getIssueById($issueKey);

        if (!$this->transitionIsValid($issue, $transitionTo)) {
            throw new Exception(sprintf("Invalid transition '%s' for issue key '%s'. Valid transitions '%s'", $transitionTo, $issueKey, join("', '", $this->getPossibleTransitions($issue))));
        }

        $issueField = (new IssueField(true))
            ->addCustomField($this->issueCustomFields['mergeTag'], $gitTag);

        foreach ($this->issueTransitionsFields as $transitionName => $fields) {
            if ($transitionName == $transitionTo) {
                foreach ($fields as $field => $value) {
                    $fieldName = $field;
                    if (array_key_exists($fields, $this->issueCustomFields)) {
                        $fieldName = $this->issueCustomFields[$fieldName];
                    }

                    $issueField->addCustomField($fieldName, $value);
                }
            }
        }

        $this->issueService->update($issueKey, $issueField);

        $openSubTasks = 0;
        foreach ($issue->fields->subtasks as $subTask) {
            if (!in_array($subTask->fields->status->name, $this->subTaskCompletedStatuses)) {
                $openSubTasks++;
            }
        }
        if ($openSubTasks) {
            throw new Exception("Not transitioning because all SubTasks must be closed");
        }

        return $this->transitionIssue($issueKey, $transitionTo);
    }

    /**
     * @param Issue $issue
     * @param string $transitionName
     * @return bool
     */
    public function transitionIsValid(Issue $issue, string $transitionName) {
        return in_array($transitionName, $this->getPossibleTransitions($issue));
    }

    /**
     * @param Issue $issue
     * @return array
     */
    public function getPossibleTransitions(Issue $issue) {
        $validTransitions = [];
        foreach ($issue->transitions as $transition) {
            $validTransitions[] = $transition->to->name;
        }
        return $validTransitions;
    }
}
