<?php

namespace Fie\Automation\ServiceUtils;

use \Exception;

class ServiceException extends Exception
{
    /**
     * @return bool
     */
    public function isSuccessful() {
        $message = preg_replace("/\D/", '', $this->getMessage());
        if (strlen($message) !== 1) {
            return false;
        }
        return intval($message) === 0 ? true : false;
    }
}
