<?php

namespace Fie\Automation\ServiceUtils;

use Fie\Automation\Service;

final class Help
{
    private $optionsCommon = [];
    private $optionsService = [];
    private $serviceHelpText = '';

    /**
     * @param array $optionsCommon
     * @return Help $this
     */
    public function setOptionsCommon(array $optionsCommon) {
        $this->optionsCommon = $optionsCommon;
        return $this;
    }

    /**
     * @param array $optionsBySection
     * @return Help $this
     */
    public function setOptionsService(array $optionsBySection) {
        $this->optionsService = $optionsBySection;
        return $this;
    }

    /**
     * @param string $serviceHelpText
     * @return Help $this
     */
    public function setServiceHelpText(string $serviceHelpText) {
        $this->serviceHelpText = $serviceHelpText;
        return $this;
    }

    /**
     * @return string
     */
    public function getText() {
        if (Service::isWeb()) {
            $hostname = array_key_exists('HTTP_HOST', $_SERVER) ? $_SERVER['HTTP_HOST'] : '[hostname]';

            if (Service::isApi()) {
                $uri = preg_replace(['!^/api/!', '/\?.*$/'], '', $_SERVER['REQUEST_URI']);
                return sprintf('Help documentation available at http://%s/%s?help', $hostname, $uri);
            }

            $output = <<<EOL
Web Documentation

Usage:
    http://$hostname/service_name?[flags|key-value-pairs]
EOL;
        } else {
            $script = $_SERVER['argv'][0];
            $output = <<<EOL
Command-Line Interface Documentation

Usage:
    php $script service_name [flags|key-value-pairs]
EOL;
        }

        $output .= "\n\n".
            $this->getFormattedOptions($this->optionsCommon).
            "\n\nService-Specific Documentation\n\n".
            $this->getServiceDescription().
            $this->getFormattedOptions($this->optionsService);

        return $output;
    }

    private function getServiceDescription() {
        $indentLength = 4;
        $indent = str_repeat(" ", $indentLength);

        $description = trim($this->serviceHelpText);

        if (strlen($description) == 0) {
            return '';
        }

        $descriptionWrapped = explode("\n", wordwrap($description, 80-$indentLength));
        $descriptionFormatted = $indent.implode("\n$indent", $descriptionWrapped);
        return "Service Description:\n$descriptionFormatted\n\n";
    }

    /**
     * @param array $optionsBySection
     * @return string
     */
    private function getFormattedOptions(array $optionsBySection) {
        $nameIndentLength  = 4;
        $descIndentLength = 10;
        $nameIndent = str_repeat(" ", $nameIndentLength);
        $descIndent = str_repeat(" ", $descIndentLength);
        $prefix = Service::isWeb() ? '' : '--';

        $optionsText = [];
        $sections = count($optionsBySection);
        foreach ($optionsBySection as $sectionName => $options) {
            $optionsText[] = sprintf("%s flags and key-value pairs:", $sectionName);

            if (is_array($options) && count($options) > 0) {
                foreach ($options as $optionName => $optionDesc) {
                    if (!is_int($optionName) && is_string($optionDesc)) {
                        $descWrapped = explode("\n", wordwrap($optionDesc, 80-$descIndentLength));
                        $optionsText[] = $nameIndent.$prefix.$optionName;
                        $optionsText[] = $descIndent.implode("\n$descIndent", $descWrapped);
                    }
                }
            } else {
                $optionsText[] = $nameIndent."(None)";
            }

            if (--$sections) {
                $optionsText[] = '';
            }
        }

        return implode("\n", $optionsText)."\n";
    }
}
