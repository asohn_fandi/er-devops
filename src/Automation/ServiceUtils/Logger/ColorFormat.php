<?php

namespace Fie\Automation\ServiceUtils\Logger;

use Fie\Automation\Service;
use Fie\Automation\ServiceUtils\IOMode;

final class ColorFormat
{
    /** @var IOMode $ioMode */
    private $ioMode;

    private $web;
    private $warning = '';
    private $success = '';
    private $debug   = '';

    /**
     * @param IOMode $ioMode
     */
    public function __construct(IOMode $ioMode = null) {
        $this->setIoMode($ioMode);

        $this->setup();
    }

    /**
     * @param null $ioMode
     * @return ColorFormat $this
     */
    public function setIoMode($ioMode = null) {
        if (is_object($ioMode) && method_exists($ioMode, 'isInteractive')) {
            $this->ioMode = $ioMode;
        } else {
            $this->ioMode = new IOMode();
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isWeb() {
        if (!isset($this->web)) {
            $this->web = Service::isWeb();
        }
        return $this->web;
    }

    /**
     * Set property $web to given value. Useful for SILENT level unit tests - causes throw instead of exiting program.
     *
     * @param bool|null $override
     * @return ColorFormat $this
     */
    public function setWebOverride(?bool $override = true) {
        $this->web = $override;
        return $this;
    }

    /**
     * Set string format variables.
     *
     * @param bool $useColor
     * @param bool $force - override ioMode->isInteractive()
     * @return ColorFormat $this
     */
    public function setup(bool $useColor = true, bool $force = false) {
        $this->noColors();
        if (!$useColor) {
            return $this;
        }
        if ($force) {
            if ($this->isWeb()) {
                $this->webColors();
            } else {
                $this->cliColors();
            }
        } else {
            if ($this->isWeb()) {
                $this->webColors();
            } elseif (!$this->ioMode->isInteractive($this->isWeb())) {
                $this->cliColors();
            }
        }
        return $this;
    }

    /**
     * @return ColorFormat $this
     */
    public function noColors() {
        $this->warning = "%s";
        $this->success = "%s";
        $this->debug   = "DEBUG - %s%s";
        return $this;
    }

    /**
     * @return ColorFormat $this
     */
    public function webColors() {
        $this->warning = "<span style='color:red;'>"      ."%s</span>";
        $this->success = "<span style='color:blue;'>"     ."%s</span>";
        $this->debug   = "<span style='color:gold;'>DEBUG - %s</span>" . "%s";
        return $this;
    }

    /**
     * @return ColorFormat $this
     */
    public function cliColors() {
        $this->warning = "\e[1;31m"     ."%s\e[00m";
        $this->success = "\e[1;36m"     ."%s\e[00m";
        $this->debug   = "\e[1;33mDEBUG - %s\e[00m" . "%s";
        return $this;
    }

    /**
     * @return array
     */
    public function getCurrentColorFormat() {
        return [
            'colorFormatWarning' => $this->warning,
            'colorFormatSuccess' => $this->success,
            'colorFormatDebug'   => $this->debug,
        ];
    }

    /**
     * @return string
     */
    public function getWarning() {
        return $this->warning;
    }

    /**
     * @return string
     */
    public function getSuccess() {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getDebug() {
        return $this->debug;
    }
}
