<?php

namespace Fie\Automation\ServiceUtils\Logger;

final class LogLevel
{
    const SILENT = -1;
    const PRINT  = 0;
    const WARN   = 0;
    const INFO   = 1;
    const DEBUG  = 2;
    const ERROR  = -99; # always print
    const TRACE  = -99; # always print
    const FATAL  = -99; # always print

    private $level = 0;
    private $levelAboveSilent = 0; # last known log level above SILENT
    private $locked = false;

    /**
     * @param int $logLevel
     * @return LogLevel $this
     */
    public function set(int $logLevel) {
        if ($logLevel > self::SILENT) {
            $this->levelAboveSilent = $logLevel;
        }
        if ($this->locked) {
            return $this;
        }
        $this->level = $logLevel;
        return $this;
    }

    /**
     * @param bool $ignoreSilent
     * @return int
     */
    public function get(bool $ignoreSilent = true) {
        if ($ignoreSilent) {
            return $this->level;
        } else {
            return $this->levelAboveSilent;
        }
    }

    /**
     * @return LogLevel $this
     */
    public function lock() {
        $this->locked = true;
        return $this;
    }

    /**
     * @return LogLevel $this
     */
    public function unlock() {
        $this->locked = false;
        return $this;
    }
}
