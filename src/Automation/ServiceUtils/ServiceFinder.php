<?php

namespace Fie\Automation\ServiceUtils;

use Fie\Automation\Shell\ExecuteCommand;

final class ServiceFinder
{
    /**
     * List files recursively in a directory and return their pseudo-namespace.
     * TODO: discover by namespace instead?
     *
     * @param string $directory
     * @return array
     */
    public function find($directory) {
        $executeCommand = (new ExecuteCommand())->run("find '$directory' -type f -name '*.php'");

        $services = explode("\n", trim($executeCommand->getSTDOUT()));
        $services = array_map(function ($value) use ($directory) {
            return preg_replace(["!^$directory/!", "!\.php$!"], '', $value);
        }, $services);
        return $services;
    }
}
