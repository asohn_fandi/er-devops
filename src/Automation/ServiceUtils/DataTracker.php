<?php

namespace Fie\Automation\ServiceUtils;

use Fie\Automation\Service;

final class DataTracker
{
    const TYPE_INFO = "info";
    const TYPE_WARN = "warning";
    const TYPE_SUCC = "success";

    private $dataType;
    private $data = [];

    /**
     * @param string $dataType
     */
    public function __construct(string $dataType) {
        $this->setDataType($dataType);
    }

    /**
     * @param string $dataType
     * @return DataTracker $this
     */
    public function setDataType(string $dataType) {
        $this->dataType = $dataType;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataType() {
        return $this->dataType;
    }

    /**
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * @return string
     */
    public function toJson() {
        return json_encode([
            "messages" => $this->getData(),
        ]);
    }

    /**
     * @param bool $status
     * @return DataTracker $this
     */
    public function print(bool $status) {
        if (!Service::isPhpUnit()) {
            // @codeCoverageIgnoreStart
            http_response_code($status ? 200 : 400);
            header(sprintf('Content-Type: %s', $this->getDataType()));
            // @codeCoverageIgnoreEnd
        }

        if (preg_match('/json/', $this->getDataType())) {
            print $this->toJson();
        }

        return $this;
    }

    /**
     * @param string $message
     * @return DataTracker $this
     */
    public function info(string $message) {
        return $this->write(self::TYPE_INFO, $message);
    }

    /**
     * @param string $message
     * @return DataTracker $this
     */
    public function warning(string $message) {
        return $this->write(self::TYPE_WARN, $message);
    }

    /**
     * @param string $message
     * @return DataTracker $this
     */
    public function success(string $message) {
        return $this->write(self::TYPE_SUCC, $message);
    }

    /**
     * @param string $type
     * @param string $message
     * @return DataTracker $this
     */
    private function write(string $type, string $message) {
        $this->data[] = [
            "type"    => $type,
            "details" => $message,
        ];
        return $this;
    }
}
