<?php

namespace Fie\Automation\ServiceUtils;

use Exception;
use Fie\Automation\Shell\ExecuteCommand;

final class LockFile
{
    private $lockfile;

    /**
     * @param string $lockfile
     * @throws \Exception
     * @return LockFile $this
     */
    public function create(string $lockfile) {
        # get pid from file to prune lockfile
        if (file_exists($lockfile)) {
            if (($pid = file_get_contents($lockfile)) === false) {
                throw new Exception("Unable to open lockfile to resolve mutex.");  // @codeCoverageIgnore
            } else {
                $executeCommand = (new ExecuteCommand())->run(sprintf("ps -p '%d'", $pid));
                if ($executeCommand->getExitStatus() != 0) {
                    # clean up orphan: pid does not exist, delete old lock file
                    if (unlink($lockfile) === false) {
                        throw new Exception("Unable to remove old lockfile!");  // @codeCoverageIgnore
                    }
                }

                if (file_exists($lockfile)) {
                    throw new Exception(sprintf("Service in use (PID '%d' found). Please wait for it to complete or delete the lockfile if service is not running. (lockfile: '%s')", $pid, $lockfile));
                }
            }
        }

        if (($lockfileHandle = fopen($lockfile, "w")) === false) {
            throw new Exception("Unable to create lockfile!");  // @codeCoverageIgnore
        }

        # TODO: if web, this will always be the web service's pid. causes a problem if the service orphans the lockfile on a fatal error.
        fwrite($lockfileHandle, getmypid());
        fclose($lockfileHandle);

        $this->lockfile = $lockfile;
        return $this;
    }

    /**
     * @throws \Exception
     * @return LockFile $this
     */
    public function delete() {
        if (isset($this->lockfile) && unlink($this->lockfile) === false) {
            throw new Exception("Unable to clean up old lockfile!");  // @codeCoverageIgnore
        }
        return $this;
    }
}
