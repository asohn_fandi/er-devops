<?php
/**
 * JsonSchema classes seem to have multiple definitions. We'll suppress this warning for now:
 * @noinspection PhpUndefinedClassInspection
 */

namespace Fie\Automation\ServiceUtils;

use Exception;
use Fie\Automation\Service;
use JsonSchema\Constraints\Constraint;
use JsonSchema\Validator;

final class BuildConfig
{
    private $jsonFilename;
    private $jsonSchemaFilename;
    private $cache = null;
    private $strict = false;

    /**
     * @param bool $forceValidate
     * @throws \Exception
     */
    public function __construct(bool $forceValidate = false) {
        $baseDirectory = preg_replace('!/src/.*$!', '', __DIR__) . '/configs/';
        $this->jsonFilename       = "$baseDirectory/buildConfig.json";
        $this->jsonSchemaFilename = "$baseDirectory/buildConfig.schema.json";

        if ($forceValidate || !Service::isPhpUnit()) {
            $this->validateConfig();
        }
    }

    /**
     * @return BuildConfig $this
     */
    public function setStrict() {
        $this->strict = true;
        return $this;
    }

    /**
     * @param array $cache
     * @throws \Exception
     * @return BuildConfig $this
     */
    public function setCache(array $cache = null) {
        if (empty($cache)) {
            $cache = $this->read();
        }
        $this->cache = $cache;
        return $this;
    }

    /**
     * @throws \Exception
     * @return array|null
     */
    public function getCache() {
        if (empty($this->cache)) {
            $this->setCache();
        }
        return $this->cache;
    }

    /**
     * Read buildConfig.json from the file system
     *
     * @param bool $getCached
     * @throws \Exception
     * @return mixed
     */
    public function read(bool $getCached = true) {
        if (!empty($this->cache) && $getCached) {
            return $this->getCache();
        }
        $config = $this->parseJsonFile($this->jsonFilename);
        return $config;
    }

    /**
     * @param string $jsonFilename
     * @throws \Exception
     * @return object
     */
    private function parseJsonFile(string $jsonFilename) {
        $jsonData = file_get_contents($jsonFilename);
        $json = json_decode($jsonData, true);
        if (is_null($json)) {
            throw new Exception(json_last_error_msg());  // @codeCoverageIgnore
        }
        return $json;
    }

    /**
     * Validate each section of buildConfig.json
     *
     * @throws \Exception
     * @return BuildConfig $this
     */
    private function validateConfig() {
        $schema = $this->parseJsonFile($this->jsonSchemaFilename);

        $validator = new Validator();
        @$validator->validate($this->getCache(), $schema, Constraint::CHECK_MODE_TYPE_CAST); # leading '@' suppresses warnings

        if (!$validator->isValid()) {
            // @codeCoverageIgnoreStart
            $errorMessages = [];
            foreach ($validator->getErrors() as $error) {
                $errorMessages[] = sprintf("buildConfig.json ERROR: [%s] %s", $error['property'], $error['message']);
            }
            throw new Exception("\n".implode("\n", $errorMessages));
            // @codeCoverageIgnoreEnd
        }

        return $this;
    }

    /**
     * Return config or config value.
     *
     * @throws \Exception
     * @return mixed
     */
    final public function getValue() {
        $args = func_get_args();
        $fullPath = [];
        $config = $this->getCache();
        if (count($args) > 0) {
            $traverse = $config;
            foreach ($args as $key) {
                $fullPath[] = $key;
                if (array_key_exists($key, $traverse)) {
                    $traverse = $traverse[$key];
                    if (!is_array($traverse)) {
                        break;
                    }
                } else {
                    if ($this->strict) {
                        throw new Exception(sprintf("Unknown config value: %s", implode(".", $fullPath)));
                    } else {
                        return null;
                    }
                }
            }
            return $traverse;
        }
        return $config;
    }
}
