<?php

namespace Fie\Automation\ServiceUtils\BuildConfig;

use Exception;

final class EnvironmentServer
{
    private $env = '';
    private $type = '';
    private $ipAddress = '';
    private $sshUsername = '';
    private $deployDirectory = '';

    /**
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config) {
        $this->validate($config);
    }

    /**
     * @param array $config
     * @throws \Exception
     * @return EnvironmentServer
     */
    public static function fromArray(array $config) {
        return new self($config);
    }

    /**
     * @param array $config
     * @throws \Exception
     */
    private function validate(array $config) {
        foreach (['env', 'type', 'ipAddress', 'sshUsername', 'deployDirectory'] as $key) {
            if (array_key_exists($key, $config) && is_string($config[$key]) && strlen($config[$key])) {
                $this->$key = $config[$key];
            } else {
                throw new Exception(sprintf("Invalid/Missing server config value '%s'", $key));
            }
        }
    }

    public function getEnv() {
        return $this->env;
    }

    public function getType() {
        return $this->type;
    }

    public function getIpAddress() {
        return $this->ipAddress;
    }

    public function getSshUsername() {
        return $this->sshUsername;
    }

    public function getDeployDirectory() {
        return $this->deployDirectory;
    }
}
