<?php

namespace Fie\Automation\ServiceUtils\BuildConfig;

use Exception;
use Fie\Automation\ServiceUtils\BuildConfig;
use Fie\Automation\Shell\ExecuteCommand;

final class Environment
{
    const SSH_TIMEOUT = 3;

    /** @var BuildConfig $buildConfig */
    private $buildConfig;

    private $repositoryName;
    private $productEnvironmentName;

    /**
     * @param string $repositoryName
     * @param string $productEnvironmentName
     * @param BuildConfig $buildConfig
     * @throws \Exception
     */
    public function __construct(string $repositoryName, string $productEnvironmentName, BuildConfig $buildConfig = null) {
        $this->repositoryName = $repositoryName;
        $this->productEnvironmentName = $productEnvironmentName;
        $this->buildConfig = $buildConfig ?: new BuildConfig();

        $this->validate();
    }

    /**
     * @throws \Exception
     */
    private function validate() {
        if ($this->buildConfig->getValue('productEnvironments', $this->repositoryName) === null) {
            throw new Exception(sprintf("Missing environment configuration for repository '%s'", $this->repositoryName));
        }
        if ($this->buildConfig->getValue('productEnvironments', $this->repositoryName, $this->productEnvironmentName) === null) {
            throw new Exception(sprintf("Missing environment configuration for environment '%s.%s'", $this->repositoryName, $this->productEnvironmentName));
        }
    }

    /**
     * @throws \Exception
     * @return EnvironmentServer[]
     */
    public function getServerList() {
        $serverList = $this->buildConfig->getValue('productEnvironments', $this->repositoryName, $this->productEnvironmentName, 'serverList');

        $serverListObjects = [];
        $errors = [];
        foreach ($serverList as $serverConfig) {
            try {
                $serverListObjects[] = EnvironmentServer::fromArray($serverConfig);
            } catch (Exception $e) {
                $errors[] = sprintf("Environment config error for '%s.%s': %s", $this->repositoryName, $this->productEnvironmentName, $e->getMessage());
            }
        }

        if (count($errors) > 0) {
            throw new Exception(join("\n", $errors));
        }

        return $serverListObjects;
    }

    /**
     * @codeCoverageIgnore
     * @throws \Exception
     * @returns bool
     */
    public function testConnection() {
        $serverList = $this->getServerList();

        $errors = [];
        foreach ($serverList as $serverConfig) {
            $ipAddress   = $serverConfig->getIpAddress();
            $sshUsername = $serverConfig->getSshUsername();

            $executeCommand = (new ExecuteCommand())->run(
                sprintf(ExecuteCommand::SSH." -o ConnectTimeout=%s %s@%s -- 'echo \"hi mom\"'", self::SSH_TIMEOUT, $sshUsername, $ipAddress)
            );

            $stderr = $executeCommand->getSTDERR();
            if (strlen($stderr)) {
                if (!preg_match('/Warning: Permanently added .* to the list of known hosts/', $stderr)) {
                    $errors[] = sprintf("  %15s:  %s", $ipAddress, $stderr);
                }
            }
        }

        if (count($errors)) {
            throw new Exception(sprintf("Failed to connect to environment%s:\n%s", count($errors) > 1 ? 's' : '', implode("\n", $errors)));
        }

        return true;
    }
}
