<?php

namespace Fie\Automation\ServiceUtils;

use Fie\Automation\Service;
use Fie\Automation\ServiceUtils\Logger\LogLevel;
use Fie\Automation\ServiceUtils\Logger\ColorFormat;

final class Logger
{
    /** @var DataTracker $dataTracker */
    private $dataTracker;

    /** @var LogLevel $logLevel */
    public $logLevel;

    /** @var ColorFormat $colorFormat */
    public $colorFormat;

    private $logs = [];
    private $immediateFlush;

    public function __construct() {
        $this->logLevel    = new LogLevel();
        $this->colorFormat = new ColorFormat();

        if (Service::isPhpUnit()) {
            # faster unit tests.
            $this->immediateFlush = false;
        }

        register_shutdown_function([$this, 'flush']);
    }

    public function __destruct() {
        $this->flush();
    }

    /**
     * @param bool $immediateFlush
     * @return Logger $this
     */
    public function setImmediateFlush(bool $immediateFlush) {
        $this->immediateFlush = $immediateFlush;
        return $this;
    }

    /**
     * @param DataTracker $dataTracker
     * @return Logger $this
     */
    public function setDataTracker(DataTracker $dataTracker) {
        $this->dataTracker = $dataTracker;
        $this->silentLock();
        $this->colorFormat->noColors();
        return $this;
    }

    /**
     * @return DataTracker
     */
    public function getDataTracker() {
        return $this->dataTracker;
    }

    /**
     * Configure Logger from ServiceBase
     *
     * @param Options $options
     * @return Logger $this
     */
    public function setup(Options $options) {
        # here, "flag" (boolean) should not be confused with level of verbosity.
        $verboseFlag = $options->getOptionFlag('verbose');
        $debugFlag   = $options->getOptionFlag('debug');
        $this->level((int)$verboseFlag + (int)$debugFlag);

        $color = $options->getOptionBoolean('color');
        if ($color === true) {
            $this->colorFormat->setup(true, true);
        } elseif ($color === false) {
            $this->colorFormat->setup(false);
        }

        return $this;
    }

    /**
     * Get/Set value for $level property.
     *
     * @param int|null $logLevel
     * @return int
     */
    public function level(?int $logLevel = null) {
        if (isset($logLevel)) {
            $this->logLevel->set($logLevel);
        }
        return $this->logLevel->get();
    }

    /**
     * No output at all.
     *
     * @return Logger $this
     */
    public function silent() {
        $this->logLevel->set(LogLevel::SILENT);
        return $this;
    }

    /**
     * No output at all EVER.
     *
     * @return Logger $this
     */
    public function silentLock() {
        $this->silent();
        $this->logLevel->lock();
        return $this;
    }

    /**
     * Determine if the log level has been increased to verbose.
     *
     * @return bool
     */
    public function isVerbose() {
        return $this->logLevel->get() >= LogLevel::INFO;
    }

    /**
     * Determine if the log level has been increased to debug level.
     *
     * @return bool
     */
    public function isDebug() {
        return $this->logLevel->get() >= LogLevel::DEBUG;
    }

    /**
     * Set property $web to given value. Useful for SILENT level unit tests - causes throw instead of exiting program.
     *
     * @param bool|null $override
     * @return Logger $this
     */
    public function setWebOverride(?bool $override = true) {
        $this->colorFormat->setWebOverride($override);
        return $this;
    }

    /**
     * Format var_export to be a little bit prettier.
     *
     * @param mixed $var
     * @return string
     */
    private function getCleanVariable($var) {
        $varArray = [];
        $varData = var_export($var, true);
        foreach (explode("\n", $varData) as $line) {
            $varArray[] = trim($line);
        }
        $cleanVar = implode(" ", $varArray);
        $cleanVar = str_replace(["array (", ", )"], ["array(", " )"], $cleanVar);
        $cleanVar = preg_replace("/ \d+ => /", " ", $cleanVar);
        if (strpos($cleanVar, '__set_state') > -1) {
            $cleanVar = preg_replace(["/^[^:]+::__set_state\(/", "/\)$/"], "", $cleanVar);
        }
        return $cleanVar;
    }

    /**
     * Append $this->log[] and flush() if necessary.
     *
     * @param int $level
     * @param string $logMessage
     * @param string $prefix
     * @return Logger $this
     */
    private function writeLn(int $level, string $logMessage, string $prefix = '') {
        $log = [
            'level'         => $level,
            'logMessage'    => strlen($prefix) == 0 ? $logMessage : "$prefix $logMessage",
            'callingMethod' => debug_backtrace()[1]['function'],
        ];

        $this->logs[] = $log;
        $this->hookDataTracker($log);

        if ($this->immediateFlush) {
            $this->flush();
        }
        return $this;
    }

    /**
     * Send to DataTracker.
     *
     * @param array $log
     * @return Logger $this
     */
    private function hookDataTracker(array $log) {
        if (!isset($this->dataTracker) || preg_match('/^lineBreak/', $log['callingMethod'])) {
            return $this;
        } elseif (in_array($log['level'], [LogLevel::PRINT, LogLevel::WARN])) {
            if (preg_match('/^warn/', $log['callingMethod'])) {
                $this->dataTracker->warning($log['logMessage']);
            } elseif ($log['callingMethod'] == 'logSuccess') {
                $this->dataTracker->success($log['logMessage']);
            } else {
                $this->dataTracker->info($log['logMessage']);
            }
        } elseif ($log['level'] <= $this->logLevel->get(false)) {
            $this->dataTracker->info($log['logMessage']);
        }
        return $this;
    }

    /**
     * Print the contests of the log array, truncate the array, and flush the output.
     *
     * @return Logger $this
     */
    public function flush() {
        if (count($this->logs) == 0) {
            return $this;
        }
        if (!isset($this->dataTracker)) {
            foreach ($this->logs as $log) {
                if ($log['level'] <= $this->level()) {
                    printf("%s\n", rtrim($log['logMessage']));
                }
            }
            if (!Service::isPhpUnit() && count(ob_get_status()) > 0) {
                ob_flush();  // @codeCoverageIgnore
            }
            flush();
        }
        $this->clearLogs();
        return $this;
    }

    /**
     * Truncate the log array.
     *
     * @return Logger $this
     */
    public function clearLogs() {
        $this->logs = [];
        return $this;
    }

    /**
     * @param bool $status
     * @return Logger $this
     */
    public function printData(bool $status) {
        if ($dataTracker = $this->getDataTracker()) {
            $dataTracker->print($status);
        }
        return $this;
    }

    /* STDERR... */

    /**
     * @codeCoverageIgnore
     * @param string $logMessage
     */
    public function stderr(string $logMessage) {
        error_log($logMessage);
    }

    /* Line Breaks... */

    /** @return Logger $this */
    public function lineBreak() {
        return $this->writeLn(LogLevel::PRINT, '')->flush();
    }

    /** @return Logger $this */
    public function lineBreakVerbose() {
        return $this->writeLn(LogLevel::INFO, '')->flush();
    }

    /** @return Logger $this */
    public function lineBreakDebug() {
        return $this->writeLn(LogLevel::DEBUG, '')->flush();
    }

    /* Level: PRINT (0)... */

    /**
     * @param string $logMessage
     * @return Logger $this
     */
    public function logSuccess(string $logMessage) {
        return $this->writeLn(LogLevel::PRINT, sprintf($this->colorFormat->getSuccess(), $logMessage));
    }

    /**
     * @param string $logMessage
     * @return Logger $this
     */
    public function log(string $logMessage) {
        return $this->writeLn(LogLevel::PRINT, $logMessage);
    }

    /**
     * @param mixed $var
     * @param string $prefix
     * @return Logger $this
     */
    public function logVar($var, string $prefix = '') {
        return $this->writeLn(LogLevel::PRINT, $this->getCleanVariable($var), $prefix);
    }

    /* Level: WARN (0)... */

    /**
     * @param string $logMessage
     * @return Logger $this
     */
    public function warn(string $logMessage) {
        return $this->writeLn(LogLevel::WARN, sprintf($this->colorFormat->getWarning(), $logMessage));
    }

    /**
     * @param mixed $var
     * @param string $prefix
     * @return Logger $this
     */
    public function warnVar($var, string $prefix = '') {
        return $this->writeLn(LogLevel::WARN, $this->getCleanVariable($var), $prefix);
    }

    /* Level: INFO (1)... */

    /**
     * @param string $logMessage
     * @return Logger $this
     */
    public function info(string $logMessage) {
        return $this->writeLn(LogLevel::INFO, $logMessage);
    }

    /**
     * @param mixed $var
     * @param string $prefix
     * @return Logger $this
     */
    public function infoVar($var, string $prefix = '') {
        return $this->writeLn(LogLevel::INFO, $this->getCleanVariable($var), $prefix);
    }

    /* Level: DEBUG (2)... */

    /**
     * @param string $logMessage
     * @param string $prefix
     * @return Logger $this
     */
    public function debug(string $logMessage, string $prefix = '') {
        $prefix .= strlen($prefix) > 0 ? ' ' : '';
        return $this->writeLn(LogLevel::DEBUG, sprintf($this->colorFormat->getDebug(), $prefix, $logMessage));
    }

    /**
     * @param mixed $var
     * @param string $prefix
     * @return Logger $this
     */
    public function debugVar($var, string $prefix = '') {
        $prefix .= strlen($prefix) > 0 ? ' ' : '';
        return $this->writeLn(LogLevel::DEBUG, $this->getCleanVariable($var), $prefix);
    }

    /* Level: ERROR (always)... */

    /**
     * @param string $logMessage
     * @return Logger $this
     */
    public function error(string $logMessage) {
        return $this->writeLn(LogLevel::ERROR, sprintf($this->colorFormat->getWarning(), 'ERROR: ', $logMessage));
    }

    /**
     * @param string $var
     * @return Logger $this
     */
    public function errorVar(string $var) {
        return $this->writeLn(LogLevel::ERROR, $this->getCleanVariable($var), 'ERROR: ');
    }

    /* Level: TRACE (always)... */

    /**
     * @param string $logMessage
     * @return Logger $this
     */
    public function trace(string $logMessage = '') {
        ob_start();
        debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $backtrace = ob_get_clean();
        if (strlen($logMessage) > 0) {
            $backtrace = "$logMessage\n$backtrace";
        }
        return $this->writeLn(LogLevel::TRACE, $backtrace);
    }

    /* Level: FATAL (always)... */

    /**
     * @param string $logMessage
     * @param callable $terminator
     */
    public function fatal(string $logMessage, callable $terminator = null) {
        $this
            ->writeLn(LogLevel::FATAL, sprintf($this->colorFormat->getWarning(), 'FATAL ERROR: ', $logMessage))
            ->flush();
        if (isset($terminator)) {
            call_user_func($terminator);
        } else {
            exit(1);  // @codeCoverageIgnore
        }
    }
}
