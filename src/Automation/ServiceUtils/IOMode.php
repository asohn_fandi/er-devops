<?php

# https://stackoverflow.com/questions/11327367/detect-if-a-php-script-is-being-run-interactively-or-not

namespace Fie\Automation\ServiceUtils;

use Fie\Automation\Service;
use \stdClass;

final class IOMode
{
    private $interactive;
    private $mode;
    public $stdin;
    public $stdout;
    public $stderr;

    /**
     * @param null $mode
     */
    public function __construct($mode = null) {
        $this->mode = $mode;

        if (defined('STDIN')) {
            $this->getMode($this->stdin, STDIN);
        }
        if (defined('STDOUT')) {
            $this->getMode($this->stdout, STDOUT);
        }
        if (defined('STDERR')) {
            $this->getMode($this->stderr, STDERR);
        }
    }

    /**
     * @param $dev
     * @param string $fp
     */
    private function getMode(&$dev, $fp) {
        if (isset($this->mode)) {
            $mode = $this->mode;
        } elseif (is_resource($fp)) {
            $stat = fstat($fp);
            $mode = $stat['mode'] & 0170000; # S_IFMT
        } else {
            return;  // @codeCoverageIgnore
        }

        $dev = new StdClass;
        $dev->isFifo = $mode == 0010000; # S_IFIFO
        $dev->isChr  = $mode == 0020000; # S_IFCHR
        $dev->isDir  = $mode == 0040000; # S_IFDIR
        $dev->isBlk  = $mode == 0060000; # S_IFBLK
        $dev->isReg  = $mode == 0100000; # S_IFREG
        $dev->isLnk  = $mode == 0120000; # S_IFLNK
        $dev->isSock = $mode == 0140000; # S_IFSOCK
    }

    /**
     * @return bool
     */
    public function isInteractiveSTDIN() {
        if (isset($this->stdin) && ($this->stdin->isFifo || $this->stdin->isReg || $this->stdin->isDir || $this->stdin->isSock)) {
            # Input piped from another command, regular file, directory, or socket (ssh)
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isInteractiveSTDOUT() {
        if (isset($this->stdout) && ($this->stdout->isFifo || $this->stdout->isReg)) {
            # Output piped to another command or regular file
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isInteractiveSTDERR() {
        if (isset($this->stderr) && ($this->stderr->isFifo || $this->stderr->isReg)) {
            # stderr is redirected to stdout AND is piped to another command, or stderr is redirect to a regular file
            return true;
        }
        return false;
    }

    /**
     * Determine if the service is being run from the web or piped to another command on the command line, if so, don't
     * apply color codes to logMessage.
     *
     * @param null|bool $webOverride
     * @return bool
     */
    public function isInteractive(?bool $webOverride = null) {
        if (isset($this->interactive)) {
            return $this->interactive;
        }

        $this->interactive = false;

        if (!Service::isPhpUnit()) {
            // @codeCoverageIgnoreStart
            $isWeb = Service::isWeb();
            if (!is_null($webOverride)) {
                $isWeb = $webOverride;
            }

            if (defined('STDOUT') && !posix_isatty(STDOUT)) {
                $this->interactive = true;
            } elseif ($this->isInteractiveSTDIN() || $this->isInteractiveSTDOUT() || $this->isInteractiveSTDERR()) {
                $this->interactive = true;
            } elseif ($isWeb || php_sapi_name() != 'cli') {
                $this->interactive = true;
            }
            // @codeCoverageIgnoreEnd
        }

        return $this->interactive;
    }
}
