<?php

namespace Fie\Automation;

use Fie\Automation\ServiceUtils\Logger;
use Fie\Automation\ServiceUtils\Options;

final class ChildService
{
    /** @var ServiceBase $serviceObject */
    private $serviceObject;

    /** @var Logger $logger */
    private $logger;

    private $options = [];

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * @param ServiceBase $serviceObject
     * @return $this
     */
    public function setServiceObject(ServiceBase $serviceObject) {
        $this->serviceObject = $serviceObject;
        return $this;
    }

    /**
     * @return ServiceBase
     */
    public function getServiceObject() {
        return $this->serviceObject;
    }

    /**
     * @param array $parentOptions
     * @param array $childOptions
     * @return $this
     */
    public function prepareOptions(array $parentOptions, array $childOptions) {
        $optionsGlobal = (new Options())->getKnownOptionsGlobal();

        foreach ($parentOptions as $option => $value) {
            if (in_array($option, $optionsGlobal)) {
                $this->options[$option] = $value;
            }
        }

        foreach ($childOptions as $option => $value) {
            $this->options[$option] = $value;
        }

        return $this;
    }

    /**
     * Run the child service.
     *
     * @return $this
     */
    public function run() {
        $this->logger
            ->lineBreak()
            ->info(str_repeat("=", 23))
            ->info("Spawning child service.")
            ->info(str_repeat("=", 23))
            ->lineBreakVerbose();

        (new Service($this->logger, $this->getServiceObject(), $this->options))->run();

        $this->logger
            ->lineBreakVerbose()
            ->info(str_repeat("=", 23))
            ->info("Child service complete.")
            ->info(str_repeat("=", 23));

        return $this;
    }
}
