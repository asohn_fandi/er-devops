<?php

namespace Fie\Automation;

use Exception;
use Fie\Automation\ServiceUtils\BuildConfig;
use Fie\Automation\ServiceUtils\Help;
use Fie\Automation\ServiceUtils\Logger;
use Fie\Automation\ServiceUtils\Options;
use Fie\Automation\ServiceUtils\OptionsException;
use Fie\Automation\ServiceUtils\ServiceException;
use Fie\Automation\Shell\ExecuteCommand;

abstract class ServiceBase
{
    /**
     * @throws \Exception
     * @throws OptionsException
     * @throws ServiceException
     * @return ServiceBase
     */
    abstract protected function configure(): ServiceBase;

    /**
     * @throws \Exception
     * @throws ServiceException
     * @return ServiceBase
     */
    abstract public function run(): ServiceBase;

    /** @return void */
    abstract protected function onSuccess(): void;

    /** @return void */
    abstract protected function onFailure(): void;

    /** @return array */
    abstract protected function getServiceOptions(): array;

    /** @return string */
    abstract protected function help(): string;

    /** @var Logger $logger */
    protected $logger;

    /** @var Options $options */
    protected $options;

    /** @var BuildConfig $buildConfig */
    protected $buildConfig;

    private $knownOptions = [];
    private $isSetup = false;
    private $isRunning = false;
    private $endingTriggersExecuted = false;
    private $givenOptions = null;

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger = null) {
        $this->logger  = $logger ?: new Logger();
        $this->options = new Options();
    }

    public function __destruct() {
        $this->logger->flush();
    }

    /**
     * @param BuildConfig $buildConfig
     * @return ServiceBase $this
     */
    final public function setBuildConfig(BuildConfig $buildConfig) {
        $this->buildConfig = $buildConfig;
        return $this;
    }

    /**
     * @return BuildConfig
     */
    final public function getBuildConfig() {
        return $this->buildConfig;
    }

    /**
     * @param Logger $logger
     * @return ServiceBase $this
     */
    final public function setLogger(Logger $logger) {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return Logger
     */
    final public function getLogger() {
        return $this->logger;
    }

    /**
     * @return array
     */
    final public function getKnownOptions() {
        return $this->knownOptions;
    }

    /**
     * @param array $options
     * @return $this
     */
    final public function setOptions(array $options) {
        $this->givenOptions = $options;
        return $this;
    }

    /**
     * @throws \Exception
     * @throws ServiceException
     * @returns bool
     */
    final public function configureAndRun() {
        if ($this->isRunning) {
            # prevent subclass from calling this method.
            return false;
        }
        $this->isRunning = true;

        try {
            $this
                ->setupBaseClass() # throws
                ->configure(); # throws
            $this->logger->flush();
            $this
                ->run() # throws
                ->runEndingTriggers(0);
        } catch (OptionsException $e) {
            $this->logger
                ->warn($e->getMessage())
                ->lineBreak()
                ->lineBreak();
            $this->exitHelp();
        } catch (ServiceException $e) {
            $this->runEndingTriggers(0);
            throw $e;
        }

        $this->logger->flush();

        return true;
    }

    /**
     * @throws \Exception
     * @throws ServiceException
     */
    final public function setupBaseClass() {
        if ($this->isSetup) {
            # prevent subclass from calling this method.
            return $this;
        }

        if (!isset($this->buildConfig)) {
            $this->setBuildConfig(new BuildConfig());
        }

        $this->knownOptions = array_merge($this->getCommonOptions(), $this->getExtendedOptions());
        try {
            $this->options
                ->setKnownOptions($this->knownOptions)
                ->processOptions($this->givenOptions);
        } catch (OptionsException $e) {
            if (!Service::isPhpUnit()) {
                $this->logger->setup($this->options);  // @codeCoverageIgnore
            }
            $this->logger
                ->warn(sprintf("Service Options Error! %s", $e->getMessage()))
                ->lineBreak()
                ->lineBreak();
            $this->exitHelp();
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            if (!Service::isPhpUnit()) {
                $this->logger->setup($this->options);
            }
            $this->logger->warn(sprintf("Unknown Service Options Error! %s", $e->getMessage()));
            $this->exitFailure();
            // @codeCoverageIgnoreEnd
        }

        if (!Service::isPhpUnit()) {
            $this->logger->setup($this->options);  // @codeCoverageIgnore
        }

        if ($this->options->getOptionFlag('help')) {
            $this->exitHelp();
        }

        $this->logger
            ->debug(var_export($this->options->getRawOptions(),    true), 'Options:')
            ->debug(var_export($this->options->getRawFlagValues(), true), 'Flag Values:')
            ->lineBreakDebug()
            ->lineBreakDebug();

        $this->isSetup = true;
        return $this;
    }

    /**
     * Make sure the return value of $this->getServiceOptions() is the correct structure.
     *
     * @return array
     */
    private function getExtendedOptions() {
        $optionsBySection = $this->getServiceOptions();

        $extendedOptions = [];
        foreach ($optionsBySection as $sectionName => $options) {
            if (!is_array($options)) {
                continue;
            }
            $extendedOptions[$sectionName] = [];
            foreach ($options as $optionName => $optionDesc) {
                if (is_int($optionName) || !is_string($optionDesc)) {
                    $this->logger->warnVar($options[$optionName], sprintf("Invalid service option details in section '%s'!", $sectionName));
                    continue;
                }
                $extendedOptions[$sectionName][$optionName] = $optionDesc;
            }
        }
        return $extendedOptions;
    }

    /**
     * @return array
     */
    private function getCommonOptions(): array {
        $options = [
            'Common' => [
                'help'    => 'Display this text plus any help text for services, if defined.',
                'verbose' => 'Verbose output.',
            ],
        ];
        if (!Service::isApi()) {
            $options['Common']['debug']            = 'Debug output. Not available via API.';
            $options['Common']['color=true|false'] = 'Turns color output on or off. Not available via API.';
        }
        return $options;
    }

    /**
     * Make sure the environment has the resource (e.g. binary) we need to complete a future task.
     *
     * @param string $resource
     * @throws ServiceException
     * @return ServiceBase $this
     */
    final public function ensureDependency(string $resource) {
        $this->logger->debug(sprintf("Checking dependency '%s'", $resource));

        $executeCommand = new ExecuteCommand($this->logger);
        $executeCommand->run(sprintf("which '%s'", $resource));

        if ($executeCommand->getExitStatus() != 0) {
            $this->logger->warn(sprintf("Missing dependency: %s", $resource));
            $this->exitFailure();
        }

        return $this;
    }

    /**
     * @param ServiceBase $childServiceObject
     * @param array $childOptions
     * @return bool
     */
    final public function spawnChildService(ServiceBase $childServiceObject, array $childOptions = []) {
        if (!in_array(debug_backtrace()[1]['function'], ['onSuccess', 'onFailure'])) {
            $this->logger->warn("Attempted to spawn child service. Child service may only be spawned from onSuccess() or onFailure()");
            return false;
        }

        if (static::class === get_class($childServiceObject)) {
            $this->logger->warn("Attempting to spawn child service. Child service is same as parent. Exiting to prevent infinite loop.");
            return false;
        }

        (new ChildService($this->logger))
            ->setServiceObject($childServiceObject)
            ->prepareOptions($this->options->getRawOptions(), $childOptions)
            ->run();

        return true;
    }

    /**
     * Finish and print help documentation.
     *
     * @throws ServiceException
     */
    final private function exitHelp() {
        $helpOutput = (new Help())
            ->setOptionsCommon($this->getCommonOptions())
            ->setOptionsService($this->getServiceOptions())
            ->setServiceHelpText($this->help())
            ->getText();
        $this->logger
            ->log($helpOutput)
            ->flush();
        $this->exitNoOp();
    }  // @codeCoverageIgnore

    /**
     * Finish successfully with no operations required.
     *
     * @throws ServiceException
     */
    final protected function exitNoOp() {
        $this
            ->terminate(0, true)
            ->exit(0);
    }  // @codeCoverageIgnore

    /**
     * Finish successfully.
     *
     * @throws ServiceException
     */
    final protected function exitSuccess() {
        $this
            ->terminate(0)
            ->exit(0);
    }  // @codeCoverageIgnore

    /**
     * Finish unsuccessfully.
     *
     * @throws ServiceException
     */
    final protected function exitFailure() {
        $this
            ->terminate(1)
            ->exit(1);
    }  // @codeCoverageIgnore

    /**
     * Finish unsuccessfully without ending triggers.
     *
     * @throws ServiceException
     */
    final public function fatal() {
        $this->exit(1);
    }  // @codeCoverageIgnore

    /**
     * @param int $exitStatus
     * @param bool $noOp - no operation required, used by child classes
     * @return ServiceBase
     */
    final public function terminate(int $exitStatus, bool $noOp = false): ServiceBase {
        $this->runEndingTriggers($exitStatus, $noOp);
        return $this;
    }

    /**
     * Throw ServiceException to exit back to Service class
     *
     * @param int $exitStatus
     * @throws ServiceException
     */
    final protected function exit(int $exitStatus) {
        $this->logger->flush();
        throw new ServiceException($exitStatus);
    }

    /**
     * @param int $exitStatus
     * @param bool $noOp
     * @return ServiceBase $this
     */
    private function runEndingTriggers(int $exitStatus, bool $noOp = false) {
        if ($this->endingTriggersExecuted) {
            return $this;
        }
        $this->endingTriggersExecuted = true;

        $this->logger->flush();

        if ($noOp) {
            return $this;
        }

        $this->logger->lineBreakVerbose();

        try {
            if ($exitStatus == 0) {
                $this->logger
                    ->debug("Executing onSuccess()...")
                    ->flush();
                $this->onSuccess();
            } else {
                $this->logger
                    ->debug("Executing onFailure()...")
                    ->flush();
                $this->onFailure();
            }
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            $this->logger->warn($e->getMessage());
            // @codeCoverageIgnoreEnd
        }

        $this->logger
            ->lineBreak()
            ->info("Process Complete.")
            ->flush();

        return $this;
    }
}
