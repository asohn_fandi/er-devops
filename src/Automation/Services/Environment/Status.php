<?php

namespace Fie\Automation\Services\Environment;

use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\BuildConfig\Environment;
use Fie\Automation\Shell\ExecuteCommand;

/** @codeCoverageIgnore */
final class Status extends ServiceBase
{
    /** @var Environment $environmentConfig */
    private $environmentConfig;

    private $repositoryName;
    private $environmentName;

    protected function onSuccess(): void {
    }

    protected function onFailure(): void {
    }

    protected function getServiceOptions(): array {
        return [
            'EnvironmentStatus' => [
                'environment=environment_name' => 'Specifies the name of the target environment.',
                'repository=repository_name'   => 'Specifies the name of the target repository.',
            ],
        ];
    }

    protected function help(): string {
        return <<<EOL
EOL;
    }

    /**
     * @throws \Exception
     * @return ServiceBase $this
     */
    protected function configure(): ServiceBase {
        $this->repositoryName  = $this->options->requireOption('repository', 'repository_name');
        $this->environmentName = $this->options->requireOption('environment', 'environment_name');

        $this->environmentConfig = new Environment($this->repositoryName, $this->environmentName);
        $this->environmentConfig->testConnection();

        return $this;
    }

    /**
     * @throws \Exception
     * @return ServiceBase $this
     */
    public function run(): ServiceBase {
        $serverList = $this->environmentConfig->getServerList();

        $details = [];
        foreach ($serverList as $serverConfig) {
            $ipAddress       = $serverConfig->getIpAddress();
            $sshUsername     = $serverConfig->getSshUsername();
            $deployDirectory = $serverConfig->getDeployDirectory();

            $details[$ipAddress][0] = "Resource: ";

            $executeCommand = (new ExecuteCommand())->run(
                sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && (git symbolic-ref --short -q HEAD || git describe --tags --exact-match)'", $sshUsername, $ipAddress, $deployDirectory)
            );

            $stderr = $executeCommand->getSTDERR();
            if (strlen($stderr)) {
                $details[$ipAddress][0] .= "Unknown!";
                continue;
            } else {
                $details[$ipAddress][0] .= $executeCommand->getSTDOUT();
            }

            # get array of file statuses. pattern: 'XY file_name'
            # https://git-scm.com/docs/git-status#_short_format
            $executeCommand = (new ExecuteCommand())->run(
                sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && git status -s --ignored'", $sshUsername, $ipAddress, $deployDirectory)
            );
            $gitStatus = $executeCommand->getSTDOUT();

            $codeMap = [
                ' ' => 'unmodified',
                'M' => 'modified',
                'A' => 'added',
                'D' => 'deleted',
                'R' => 'renamed',
                'C' => 'copied',
                'U' => 'updated',
                '?' => 'untracked',
                '!' => 'ignored',
            ];
            list($xCode, $yCode) = [[], []];
            foreach (array_keys($codeMap) as $code) {
                $xCode[$code] = 0;
                $yCode[$code] = 0;
            }
            foreach (explode("\n", $gitStatus) as $fileStatus) {
                $xCode[$fileStatus[0]]++;
                $yCode[$fileStatus[1]]++;
            }

            if (array_sum($yCode)) {
                $yCodeFiles = [];
                foreach ($codeMap as $code => $codeString) {
                    if ($yCode[$code] > 0) {
                        $yCodeFiles[] = sprintf("%s %s", $yCode[$code], $codeString);
                    }
                }
                $details[$ipAddress][] = "Files: ".implode(", ", $yCodeFiles);
            }
        }

        foreach ($details as $ipAddress => $detail) {
            $this->logger->log(sprintf("%s:\n    %s", $ipAddress, implode("\n    ", $detail)));
        }

        return $this;
    }
}
