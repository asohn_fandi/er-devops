# Service Abstract Classes
`ServiceBase` - provide your own `run(): ServiceBase` method.  
`ExecutionPlan` - provide a `getExecutionPlan(): array` method to return an array of shell commands or `ExecutionStep` objects to execute.  

## Abstract Class `ServiceBase` Examples

### Example 1
filename: `src/Automation/Services/ServiceExamples/BasicExample.php`
```php
<?php

namespace Fie\Automation\Services\ServiceExamples;

use Fie\Automation\ServiceBase;
use Fie\Automation\Shell\ExecuteCommand;

final class BasicExample extends ServiceBase
{
    protected function onSuccess(): void {
    }

    protected function onFailure(): void {
    }

    protected function getServiceOptions(): array {
        return [];
    }

    protected function help(): string {
        return '';
    }

    protected function configure(): ServiceBase {
        return $this;
    }

    public function run(): ServiceBase {
        $executeCommand = (new ExecuteCommand($this->logger))->run('echo "Hello World!"');

        $this->logger
            ->lineBreakVerbose()
            ->log(sprintf('$exitStatus: %s', $executeCommand->getExitStatus()))
            ->logVar($executeCommand->getOutput(), '$output:');
        
        return $this;
    }
}
```
Output:
```text
$ php ./app/CliService.php ServiceExamples/BasicExample
$exitStatus: 0
$output: array( 'stdout' => 'Hello World!', 'stderr' => '' )
```

## Abstract Class `ExecutionPlan` Examples

### Example 1
filename: `src/Automation/Services/ServiceExamples/ExecutionPlanExample.php`
```php
<?php

namespace Fie\Automation\Services\ServiceExamples;

use Fie\Automation\ServiceBase;
use Fie\Automation\Shell\ExecutionPlan;
use Fie\Automation\Shell\ExecutionPlan\ExecutionStep;

final class ExecutionPlanExample extends ExecutionPlan
{
    protected function onSuccess(): void {
        $this->logger->log("Success!");
    }

    protected function onFailure(): void {
        $this->logger->log("Failure!");
    }

    protected function getServiceOptions(): array {
        return [];
    }

    protected function help(): string {
        return '';
    }

    protected function configure(): ServiceBase {
        return $this;
    }

    protected function getExecutionPlan(): array {
        return [
            [
                'command'  => "ls -l",
            ],
            new ExecutionStep(['command' => 'ls -la']),
        ];
    }

    public function run(): ServiceBase {
        # do something
        $this->logger->log("Hello World!");

        # run it
        $this->runExecutionPlan();
        
        return $this;
    }
}
```
Output:
```text
$ php ./app/CliService.php ServiceExamples/ExecutionPlanExample
Hello World!
Execution step 001 successful!
Execution step 002 successful!
```
Output (verbose):
```text
$ php ./app/CliService.php ServiceExamples/ExecutionPlanExample --verbose
Hello World!
Validating execution plan...
Valid.

Execution Plan:
001)  'ls -l'
002)  'ls -la'


command:       'ls -l'
rollback:      array( )
clearRollback: false
Exit Status: 0
Execution step 001 successful!

command:       'ls -la'
rollback:      array( )
clearRollback: false
Exit Status: 0
Execution step 002 successful!

Success!

Process Complete.
```

### Example 2
```php
<?php

namespace Fie\Automation\Services\ServiceExamples;

use Fie\Automation\ServiceBase;
use Fie\Automation\Shell\ExecutionPlan;
use Fie\Automation\Shell\ExecutionPlan\ExecutionStep;

final class ExecutionPlanExample extends ExecutionPlan
{
    protected function onSuccess(): void {
        $this->logger->log("Success!");
    }

    protected function onFailure(): void {
        $this->logger->log("Failure!");
    }

    protected function getServiceOptions(): array {
        return [];
    }

    protected function help(): string {
        return '';
    }

    protected function configure(): ServiceBase {
        return $this;
    }

    public function getExecutionPlan(): array {
        return [
            [
                'command'  => "ls -l",
            ],
            new ExecutionStep(['command' => 'ls -la']),
            [
                'command'  => "ls",
                'rollback' => ["echo something went wrong 0"],
            ],
            [
                'command'       => "ls -l",
                'rollback'      => ["echo something went wrong 1"],
                'clearRollback' => true,
            ],
            [
                'command'  => "lsFAIL -la",
                'rollback' => ["echo something went wrong 2"],
            ],
        ];
    }
}
```
Output:
```text
$ php ./app/CliService.php ServiceExamples/ExecutionPlanExample
Execution step 001 successful!
Execution step 002 successful!
Execution step 003 successful!
Rollback plan truncated.
Execution step 004 successful!
Execution step 005 failed!
Rollback command 001 successful.
Rollback command 002 successful.
```
Output (verbose):
```text
$ php ./app/CliService.php ServiceExamples/ExecutionPlanExample --verbose
Validating execution plan...
Valid.

Execution Plan:
001)  'ls -l'
002)  'ls -la'
003)  'ls'
004)  'ls -l'
005)  'lsFAIL -la'


command:       'ls -l'
rollback:      array( )
clearRollback: false
Exit Status: 0
Execution step 001 successful!

command:       'ls -la'
rollback:      array( )
clearRollback: false
Exit Status: 0
Execution step 002 successful!

command:       'ls'
rollback:      array( 'echo something went wrong 0' )
clearRollback: false
Exit Status: 0
Execution step 003 successful!

command:       'ls -l'
rollback:      array( 'echo something went wrong 1' )
clearRollback: true
Rollback plan truncated.
Exit Status: 0
Execution step 004 successful!

command:       'lsFAIL -la'
rollback:      array( 'echo something went wrong 2' )
clearRollback: false
Exit Status: 127
Execution step 005 failed!


Rollback plan:
001)  'echo something went wrong 2'
002)  'echo something went wrong 1'

Executing rollback plan...
001)  'echo something went wrong 2'
Exit Status: 0
Rollback command 001 successful.

002)  'echo something went wrong 1'
Exit Status: 0
Rollback command 002 successful.

Failure!

Process Complete.
```