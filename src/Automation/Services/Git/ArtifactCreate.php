<?php

namespace Fie\Automation\Services\Git;

use Fie\Automation\ServiceBase;
use Fie\Automation\Shell\ExecutionPlan;
use Fie\Automation\Shell\Git;

/** @codeCoverageIgnore */
final class ArtifactCreate extends ExecutionPlan
{
    /** @var Git $git */
    private $git;

    private $repositoryName;
    private $resourceName;
    private $skipPHPUnit;
    private $artifactName;

    protected function onSuccess(): void {
        $this->logger->log("Artifact created.");
    }

    protected function onFailure(): void {
    }

    protected function getServiceOptions(): array {
        return array_replace_recursive(Git::getOptions(), [
            'ArtifactCreate' => [
                'skip-phpunit' => 'Specifies that phpunit validation should be skipped when building the artifact.',
            ],
        ]);
    }

    protected function help(): string {
        return <<<EOL
EOL;
    }

    /**
     * @throws \Exception
     */
    protected function configure(): ServiceBase {
        $this->git = new Git($this->getLogger(), $this->getBuildConfig());

        $this->repositoryName = $this->options->requireOption('repository', 'repository_name');
        $this->resourceName   = $this->options->requireOneOption(['branch', 'tag'], 'name');
        $this->skipPHPUnit    = $this->options->getOptionFlag('skip-phpunit');

        /*
        $this->git->clone($this->repositoryName);
        if (!$this->git->resourceExists($this->resourceName)) {
            $this->logger->warn(sprintf("Branch or Tag '%s' does not exist in '%s'.", $this->resourceName, $this->repositoryName));
            $this->exitFailure();
        }
        */

        $this->git->removeOldArtifacts(5);

        $this->artifactName = $this->git->getArtifactName($this->repositoryName, $this->resourceName);
        $this->logger->info(sprintf("Creating artifact '%s' for '%s'", $this->artifactName, $this->repositoryName));

        return $this;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getExecutionPlan(): array {
        $plan = [
            [
                'command' => sprintf("git checkout '%s'", $this->resourceName),
            ],
            [
                'command' => "php composer.phar install",
            ],
        ];

        if (!$this->skipPHPUnit) {
            $plan = array_merge($plan, [
                [
                    # TODO: usually hangs
                    # TODO: disable nyan cat... https://phpunit.de/manual/4.8/en/appendixes.configuration.html
                    'command' => 'vendor/bin/phpunit',
                ],
            ]);
        }

        $plan = array_merge($plan, [
            [
                'command' => sprintf("cd %s && tar -cz%sf %s/%s --exclude .git .", $this->git->getRepositoryRoot(), $this->git->getTarVerboseFlag(), $this->git->getArtifactDirectory(), $this->artifactName),
            ],
        ]);

        return $plan;
    }
}
