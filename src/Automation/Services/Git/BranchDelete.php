<?php

namespace Fie\Automation\Services\Git;

use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\ServiceException;
use Fie\Automation\Shell\ExecutionPlan;
use Fie\Automation\Shell\Git;

/** @codeCoverageIgnore */
final class BranchDelete extends ExecutionPlan
{
    /** @var Git $git */
    private $git;

    private $repositoryName;
    private $branchName;
    private $sourceBranchName;
    private $force = false;

    protected function onSuccess(): void {
        $this->logger->log("Branch deleted.");
    }

    protected function onFailure(): void {
    }

    protected function getServiceOptions(): array {
        return array_replace_recursive(Git::getOptions(), [
            'BranchCreate' => [
                'force' => 'Forces branch deletion even if unmerged code changes are detected.',
            ],
            'Git' => [
                'source=source_branch_name' => 'Specifies the name of the source branch in the repository the branch was created from. Used for detecting unmerged code changes. (default=master).',
            ],
        ]);
    }

    protected function help(): string {
        return <<<EOL
EOL;
    }

    /**
     * @throws \Exception
     * @throws ServiceException
     */
    protected function configure(): ServiceBase {
        $this->git = new Git($this->getLogger(), $this->getBuildConfig());

        $this->repositoryName   = $this->options->requireOption('repository', 'repository_name');
        $this->branchName       = $this->options->requireOption('branch', 'branch_name');
        $this->sourceBranchName = $this->options->getOption('source', 'master');
        $this->force            = $this->options->getOptionBoolean('force', $this->force);

        $this->git->clone($this->repositoryName);

        if (!$this->git->branchExists($this->branchName)) {
            $this->logger->warn("Branch does not exist.");
            $this->exitFailure();
        }

        $hasUnmergedChanges = $this->git->hasCodeChanges($this->branchName, $this->sourceBranchName);
        if ($hasUnmergedChanges && !$this->force) {
            $this->logger->warn("Unmerged changes detected. Not deleting branch.");
            $this->exitFailure();
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getExecutionPlan(): array {
        return [
            [
                'command' => sprintf("git push origin --delete '%s'", $this->branchName),
            ],
        ];
    }
}
