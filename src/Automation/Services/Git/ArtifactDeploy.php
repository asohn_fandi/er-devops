<?php

namespace Fie\Automation\Services\Git;

use Exception;
use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\BuildConfig\Environment;
use Fie\Automation\Shell\ExecutionPlan;
use Fie\Automation\Shell\Git;

/** @codeCoverageIgnore */
final class ArtifactDeploy extends ExecutionPlan
{
    /** @var Git $git */
    private $git;

    /** @var Environment $environmentConfig */
    private $environmentConfig;

    private $repositoryName;
    private $resourceName;
    private $environmentName;
    private $artifactDirectory;
    private $artifactName;

    protected function onSuccess(): void {
        $this->logger->log("Artifact deployed.");
    }

    protected function onFailure(): void {
    }

    protected function getServiceOptions(): array {
        return array_replace_recursive(Git::getOptions(), [
            'ArtifactDeploy' => [
                'environment=environment_name' => 'Specifies the name of the target environment.',
            ],
        ]);
    }

    protected function help(): string {
        return <<<EOL
EOL;
    }

    /**
     * @throws \Exception
     */
    protected function configure(): ServiceBase {
        $this->git = new Git($this->getLogger(), $this->getBuildConfig());

        $this->repositoryName  = $this->options->requireOption('repository', 'repository_name');
        $this->resourceName    = $this->options->requireOneOption(['branch', 'tag'], 'name');
        $this->environmentName = $this->options->requireOption('environment', 'environment_name');

        $this->artifactDirectory = $this->git->getArtifactDirectory();
        $this->artifactName = $this->git->getArtifactName($this->repositoryName, $this->resourceName);
        if (!file_exists($this->artifactDirectory . '/' . $this->artifactName)) {
            throw new Exception(sprintf("Could not find artifact '%s/%s'", $this->artifactName));
        }

        $this->environmentConfig = new Environment($this->repositoryName, $this->environmentName);
        $this->environmentConfig->testConnection();

        $this->git->clone($this->repositoryName);

        $this->logger->info(sprintf("Deploying artifact '%s' for repository '%s' to environment '%s'", $this->artifactName, $this->repositoryName, $this->environmentName));

        return $this;
    }

    /**
     * @return array
     * @throws Exception
     */
    protected function getExecutionPlan(): array {
        $serverList = $this->environmentConfig->getServerList();

        $executionPlan = [];
        foreach ($serverList as $serverConfig) {
            $ipAddress       = $serverConfig->getIpAddress();
            $sshUsername     = $serverConfig->getSshUsername();
            $deployDirectory = $serverConfig->getDeployDirectory();

            $executionPlan[] = [
                'command' => sprintf(ExecuteCommand::SCP." %s/%s %s@%s:%s", $this->artifactDirectory, $this->artifactName, $sshUsername, $ipAddress, $this->artifactName),
            ];
            $executionPlan[] = [
                'command' => sprintf(ExecuteCommand::SSH." %s@%s -- 'tar -xf %s %s'", $sshUsername, $ipAddress, $this->artifactName, $deployDirectory),
            ];
            $executionPlan[] = [
                'command' => sprintf(ExecuteCommand::SSH." %s@%s -- 'rm %s'", $sshUsername, $ipAddress, $this->artifactName),
            ];
        }

        return $executionPlan;
    }
}
