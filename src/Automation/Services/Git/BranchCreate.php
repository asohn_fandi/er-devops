<?php

namespace Fie\Automation\Services\Git;

use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\ServiceException;
use Fie\Automation\Shell\ExecutionPlan;
use Fie\Automation\Shell\Git;

/** œ@codeCoverageIgnore */
final class BranchCreate extends ExecutionPlan
{
    /** @var Git $git */
    private $git;

    private $repositoryName;
    private $branchName;
    private $sourceBranchName;

    protected function onSuccess(): void {
        $this->logger->log("Branch created.");
    }

    protected function onFailure(): void {
    }

    protected function getServiceOptions(): array {
        return array_replace_recursive(Git::getOptions(), [
            'BranchCreate' => [],
            'Git' => [
                'source=source_branch_name' => 'Specifies the name of the source branch in the repository to create the branch from (default=master).',
            ],
        ]);
    }

    protected function help(): string {
        return <<<EOL
EOL;
    }

    /**
     * @throws \Exception
     * @throws ServiceException
     */
    protected function configure(): ServiceBase {
        $this->git = new Git($this->getLogger(), $this->getBuildConfig());

        $this->repositoryName   = $this->options->requireOption('repository', 'repository_name');
        $this->branchName       = $this->options->requireOption('branch', 'branch_name');
        $this->sourceBranchName = $this->options->getOption('source', 'master');

        $this->git->clone($this->repositoryName);

        if ($this->git->branchExists($this->branchName)) {
            $this->logger->log("Branch exists.");
            $this->exitNoOp();
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getExecutionPlan(): array {
        return [
            [
                'command' => sprintf("git checkout '%s'", $this->sourceBranchName),
            ],
            [
                'command' => sprintf("git checkout -b '%s'", $this->branchName),
            ],
            [
                'command' => sprintf("git push -u origin '%s'", $this->branchName),
            ],
        ];
    }
}
