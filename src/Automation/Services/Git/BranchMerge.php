<?php

namespace Fie\Automation\Services\Git;

use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\ServiceException;
use Fie\Automation\Shell\Git;
use Fie\Bridge\Jira;

/** @codeCoverageIgnore */
final class BranchMerge extends ServiceBase
{
    /** @var Git $git */
    private $git;

    /** @var Jira $jira */
    private $jira;

    private $repositoryName;
    private $branchName;
    private $jiraIssue;
    private $notifications = [];

    protected function onSuccess(): void {
        # TODO: act on $this->notifications
    }

    protected function onFailure(): void {
        # TODO: act on $this->notifications
    }

    protected function getServiceOptions(): array {
        return array_replace_recursive(Git::getOptions(), [
            'BranchMerge' => [],
        ]);
    }

    protected function help(): string {
        return <<<EOL
EOL;
    }

    /**
     * @throws \Exception
     * @throws ServiceException
     */
    protected function configure(): ServiceBase {
        $this->git = new Git($this->getLogger(), $this->getBuildConfig());

        $this->repositoryName = $this->options->requireOption('repository', 'repository_name');
        $this->branchName     = $this->options->requireOption('branch', 'branch_name');

        $this->git->clone($this->repositoryName);

        if (!$this->git->branchExists($this->branchName)) {
            $this->logger->warn("Branch does not exist.");
            $this->exitFailure();
        }

        $this->jira = new Jira($this->repositoryName);
        $this->jiraIssue = $this->jira->getIssueById($this->branchName);

        return $this;
    }

    /**
     * @throws \Exception
     * @throws \JiraRestApi\JiraException
     * @throws \JsonMapper_Exception
     */
    public function run(): ServiceBase {
        if (!$this->git->hasCodeChanges($this->branchName, 'master')) {
            $this->logger->log("No code changes detected");

            $this->jira->transitionIssueNoCodeChanges($this->branchName);
            $this->logger->logSuccess(sprintf("Jira issue updated."));
            return $this;
        }

        $this->git->merge($this->branchName, 'master');
        $this->logger->logSuccess(sprintf("Merged '%s' into '%s'", $this->branchName, 'master'));

        $nextTag = $this->git->getNextTag();
        $this->git->tag($nextTag);
        $this->logger->logSuccess(sprintf("Tagged '%s' with '%s'", 'master', $nextTag));

        $this->jira->transitionIssueMerged($this->branchName, $nextTag);
        $this->logger->logSuccess("Jira issue updated.");

        # TODO: merge master into open story branches and notify of conflicts

        return $this;
    }
}
