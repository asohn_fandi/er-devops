<?php

namespace Fie\Automation\Services\Git;

use Exception;
use Fie\Automation\ServiceBase;
use Fie\Automation\Services\Environment\Status;
use Fie\Automation\ServiceUtils\BuildConfig\Environment;
use Fie\Automation\Shell\ExecuteCommand;
use Fie\Automation\Shell\ExecutionPlan;
use Fie\Automation\Shell\Git;

/** @codeCoverageIgnore */
final class BranchDeploy extends ExecutionPlan
{
    /** @var Git $git */
    private $git;

    /** @var Environment $environmentConfig */
    private $environmentConfig;

    private $repositoryName;
    private $resourceName;
    private $environmentName;
    private $gitClean;
    private $doComposer;
    private $doWwwUntar;

    protected function onSuccess(): void {
        $this->logger->log("Branch deployed.");

        $this->spawnChildService(
            new Status(),
            [
                'repository'  => $this->repositoryName,
                'environment' => $this->environmentName,
            ]
        );
    }

    protected function onFailure(): void {
    }

    protected function getServiceOptions(): array {
        return array_replace_recursive(Git::getOptions(), [
            'BranchDeploy' => [
                'environment=environment_name' => 'Specifies the name of the target environment.',
                'clean'                        => "Force `git clean' before `git checkout'.",
                'do-composer=true|false'       => "Default true. Run `composer install` after `git checkout'.",
                'do-www-untar=true|false'      => "Default false. Untar the www archive after `git checkout'.",
            ],
        ]);
    }

    protected function help(): string {
        return <<<EOL
Expects an existing git repository in the destination environment and performs a checkout followed by a composer install.
EOL;
    }

    /**
     * @throws \Exception
     */
    protected function configure(): ServiceBase {
        $this->git = new Git($this->getLogger(), $this->getBuildConfig());

        $this->repositoryName  = $this->options->requireOption('repository', 'repository_name');
        $this->resourceName    = $this->options->requireOneOption(['branch', 'tag'], 'name');
        $this->environmentName = $this->options->requireOption('environment', 'environment_name');
        $this->gitClean        = $this->options->getOptionBoolean('clean', false);
        $this->doComposer      = $this->options->getOptionBoolean('do-composer', true);
        $this->doWwwUntar      = $this->options->getOptionBoolean('do-www-untar', false);

        $this->environmentConfig = new Environment($this->repositoryName, $this->environmentName);
        $this->environmentConfig->testConnection();

        /*
        $this->git->clone($this->repositoryName);
        if (!$this->git->resourceExists($this->resourceName)) {
            $this->logger->warn("Branch does not exist.");
            $this->exitFailure();
        }
        */

        $this->logger->info(sprintf("Deploying branch '%s' for repository '%s' to environment '%s'", $this->resourceName, $this->repositoryName, $this->environmentName));
        $this->logger->lineBreakVerbose();

        return $this;
    }

    /**
     * @throws \Exception
     * @return array
     */
    protected function getExecutionPlan(): array {
        $serverList = $this->environmentConfig->getServerList();

        $rollbackResources = [];
        $errors = [];
        foreach ($serverList as $serverConfig) {
            $ipAddress       = $serverConfig->getIpAddress();
            $sshUsername     = $serverConfig->getSshUsername();
            $deployDirectory = $serverConfig->getDeployDirectory();

            $executeCommand = (new ExecuteCommand())->run(
                sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && (git symbolic-ref --short -q HEAD || git describe --tags --exact-match)'", $sshUsername, $ipAddress, $deployDirectory)
            );
            $rollbackResources[$ipAddress] = $executeCommand->getSTDOUT();

            $stderr = $executeCommand->getSTDERR();
            if (strlen($stderr)) {
                $errors[] = sprintf("  %15s:  %s", $ipAddress, $stderr);
            }
        }

        if (count($errors)) {
            throw new Exception(sprintf("Failed to find current branch/tag on environment%s:\n%s", count($errors) > 1 ? 's' : '', implode("\n", $errors)));
        }

        $this->logger->lineBreakVerbose();
        $this->logger->info("Rollback resources:");
        foreach ($rollbackResources as $ipAddress => $rollbackResource) {
            $this->logger->info(sprintf("  %15s: %s", $ipAddress, $rollbackResource));
        }
        $this->logger->lineBreakVerbose();

        $executionPlan = [];
        foreach ($serverList as $serverConfig) {
            $ipAddress       = $serverConfig->getIpAddress();
            $sshUsername     = $serverConfig->getSshUsername();
            $deployDirectory = $serverConfig->getDeployDirectory();
            $type            = $serverConfig->getType();

            if ($this->gitClean) {
                $executionPlan[] = [
                    'command' => sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && git checkout . && git clean -f'", $sshUsername, $ipAddress, $deployDirectory),
                ];
            }
            $executionPlan[] = [
                'command' => sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && git fetch -q && git checkout %s && git pull'", $sshUsername, $ipAddress, $deployDirectory, $this->resourceName),
                'rollback' => [
                    sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && git fetch -q && git checkout %s && git pull'",  $sshUsername, $ipAddress, $deployDirectory, $rollbackResources[$ipAddress]),
                    sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && composer install'", $sshUsername, $ipAddress, $deployDirectory),
                ],
            ];
            if ($this->doComposer) {
                $executionPlan[] = [
                    'command' => sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && composer install'", $sshUsername, $ipAddress, $deployDirectory),
                ];
            }
            if ($this->doWwwUntar && $type === 'web') {
                $executionPlan[] = [
                    'command' => sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && rm -rf www'", $sshUsername, $ipAddress, $deployDirectory),
                ];
                $executionPlan[] = [
                    'command' => sprintf(ExecuteCommand::SSH." %s@%s -- 'cd %s && tar -xf www.tar.gz www'", $sshUsername, $ipAddress, $deployDirectory),
                ];
            }
        }

        return $executionPlan;
    }
}
