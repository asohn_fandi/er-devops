<?php

namespace Fie\Automation\Services\Git;

use Fie\Automation\ServiceBase;
use Fie\Automation\Shell\Git;

/** @codeCoverageIgnore */
final class BranchExists extends ServiceBase
{
    /** @var Git $git */
    private $git;

    private $repositoryName;
    private $branchName;

    protected function onSuccess(): void {
    }

    protected function onFailure(): void {
    }

    protected function getServiceOptions(): array {
        return array_replace_recursive(Git::getOptions(), [
            'BranchExists' => [],
        ]);
    }

    protected function help(): string {
        return <<<EOL
EOL;
    }

    /**
     * @throws \Exception
     */
    protected function configure(): ServiceBase {
        $this->git = new Git($this->getLogger(), $this->getBuildConfig());

        $this->repositoryName = $this->options->requireOption('repository', 'repository_name');
        $this->branchName     = $this->options->requireOption('branch', 'branch_name');

        $this->git->clone($this->repositoryName);

        return $this;
    }

    public function run(): ServiceBase {
        if ($this->git->branchExists($this->branchName)) {
            http_response_code(200);
            $this->logger->log("Branch exists.");
        } else {
            http_response_code(404);
            $this->logger->log("Branch does not exist.");
        }

        return $this;
    }
}
