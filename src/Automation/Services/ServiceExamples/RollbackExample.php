<?php

namespace Fie\Automation\Services\ServiceExamples;

use Fie\Automation\ServiceBase;
use Fie\Automation\Shell\ExecutionPlan;

final class RollbackExample extends ExecutionPlan
{
    private $testEmptyPlan        = false;
    private $testEmptyStep        = false;
    private $testInvalidCommand   = false;
    private $testMalformedCommand = false;
    private $testRollback         = true;
    private $testRollbackFailure  = true;

    protected function getServiceOptions(): array {
        return [
            'RollbackExample' => [
                'do'                              => 'Basic implementation of a required flag.',
                'testEmptyPlan=true|false'        => 'Test failure via empty execution plan.',
                'testEmptyStep=true|false'        => 'Test failure via empty execution step.',
                'testInvalidCommand=true|false'   => 'Test failure via invalid command in execution step.',
                'testMalformedCommand=true|false' => 'Test failure via malformed command in execution step.',
                'testRollback=true|false'         => 'Test rollback.',
                'testRollbackFailure=true|false'  => 'Test rollback failure.',
            ],
        ];
    }

    protected function help(): string {
        return <<<EOL
Example:
    php app/CliService.php ServiceExamples/RollbackExample --do
EOL;
    }

    protected function onSuccess(): void {
        if ($dataTracker = $this->logger->getDataTracker()) {
            $dataTracker->success("Example completed successfully");
        }
        $this->logger->log("onSuccess() logger example");
    }

    protected function onFailure(): void {
        if ($dataTracker = $this->logger->getDataTracker()) {
            $dataTracker->warning("Example completed unsuccessfully");
        }
        $this->logger->log("onFailure() logger example");
    }

    /**
     * @throws \Exception
     */
    public function configure(): ServiceBase {
        $this->options->requireFlag('do');

        # convenience: allow override with options
        $testFlags = ['testEmptyPlan', 'testEmptyStep', 'testInvalidCommand', 'testMalformedCommand', 'testRollback', 'testRollbackFailure'];
        foreach ($testFlags as $testFlag) {
            if ($this->options->getOption($testFlag)) {
                if (($bool = $this->options->getOptionBoolean($testFlag)) !== null) {
                    $this->$testFlag = $bool;
                    $this->logger->log(sprintf("Property override. '%s' value set to: %s", $testFlag, $bool ? 'true' : 'false'));
                }
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    protected function getExecutionPlan(): array {
        $plan = [];

        if ($this->testEmptyPlan) {
            return $plan;
        }

        if ($this->testEmptyStep) {
            $plan[] = array_merge($plan, []);
        }

        if ($this->testInvalidCommand) {
            $plan = array_merge($plan, [
                [
                    'bogus' => 'bogus'
                ],
            ]);
        }

        if ($this->testMalformedCommand) {
            $plan = array_merge($plan, [
                [
                    'command'  => [["ls", "-la"], ["ls", "-l"]],
                    'rollback' => ["echo", "something went wrong"],
                ],
            ]);
        }

        $plan = array_merge($plan, [
            [
                'command'  => "ls",
                'rollback' => ["echo something went wrong 0"],
            ],
            [
                'command'       => "ls -l",
                'rollback'      => ["echo something went wrong 1"],
                'clearRollback' => true,
            ],
            [
                'command'  => "ls -la",
                'rollback' => ["echo something went wrong 2"],
            ],
        ]);

        if ($this->testRollback || $this->testRollbackFailure) {
            $cmd = $this->testRollbackFailure ? 'echoFAIL' : 'echo';
            $plan = array_merge($plan, [
                [
                    'command'  => "lsFAIL -lah",
                    'rollback' => [
                        "echo something went wrong 3a",
                        "$cmd something went wrong 3b",
                    ],
                ],
            ]);
        }

        return $plan;
    }
}
