<?php

namespace Fie\Automation\Services\ServiceExamples;

use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\ServiceException;
use Fie\Automation\Shell\ExecutionPlan;
use Fie\Automation\Shell\ExecutionPlan\ExecutionStep;

final class ExecutionPlanExample extends ExecutionPlan
{
    protected function onSuccess(): void {}
    protected function onFailure(): void {}

    protected function getServiceOptions(): array {
        return [
            'BasicExample' => [
                'do' => 'Basic implementation of a required flag.',
            ],
        ];
    }

    protected function help(): string {
        return <<<EOL
Example:
    php app/CliService.php ServiceExamples/ExecutionPlanExample --do
EOL;
    }

    /**
     * @throws \Exception
     */
    protected function configure(): ServiceBase {
        $this->options->requireFlag('do');
        return $this;
    }

    /**
     * @throws \Exception
     * @return array
     */
    protected function getExecutionPlan(): array {
        return [
            [
                'command'  => "ls -l",
            ],
            new ExecutionStep(['command' => 'ls -la']),
        ];
    }

    /**
     * @throws ServiceException
     */
    public function run(): ServiceBase {
        # do something
        $this->logger->log("Hello World!");

        # run it
        return $this->runExecutionPlan();
    }
}
