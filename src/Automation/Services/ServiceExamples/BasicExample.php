<?php

namespace Fie\Automation\Services\ServiceExamples;

use Fie\Automation\ServiceBase;
use Fie\Automation\Shell\ExecuteCommand;

final class BasicExample extends ServiceBase
{
    protected function onSuccess(): void {}
    protected function onFailure(): void {}

    protected function getServiceOptions(): array {
        return [
            'BasicExample' => [
                'do' => 'Basic implementation of a required flag.',
            ],
        ];
    }

    protected function help(): string {
        return <<<EOL
Example:
    php app/CliService.php ServiceExamples/BasicExample --do
EOL;
    }

    /**
     * @throws \Exception
     */
    protected function configure(): ServiceBase {
        $this->options->requireFlag('do');
        return $this;
    }

    /**
     * @return ServiceBase
     */
    public function run(): ServiceBase {
        $executeCommand = (new ExecuteCommand($this->logger))->run('echo "Hello World!"');

        $this->logger
            ->lineBreakVerbose()
            ->log(sprintf('$exitStatus: %s', $executeCommand->getExitStatus()))
            ->logVar($executeCommand->getOutput(), '$output:');

        return $this;
    }
}
