<?php

namespace Fie\Automation\Shell;

use Fie\Automation\ServiceUtils\Logger;

final class ExecuteCommand
{
    const SSH = 'ssh -oStrictHostKeyChecking=no ';
    const SCP = 'scp -oStrictHostKeyChecking=no ';

    /** @var Logger $logger */
    private $logger;

    private $exitStatus = 1; # failure until overwritten
    private $output = [
        'stdout' => '',
        'stderr' => '',
    ];

    /**
     * @param Logger|null $logger
     */
    public function __construct(?Logger $logger = null) {
        $this->logger = $logger;
    }

    /**
     * Execute a shell command silently and return the exit status and contents of STDOUT and STDERR file handles
     *
     * @param string $command
     * @return ExecuteCommand
     */
    public function run(string $command) {
        $exitStatus = $this->exitStatus;
        $allOutput  = $this->output;

        if (empty($command) || !is_string($command)) {
            ($this->logger ?: new Logger())
                ->warn("ExecuteCommand: No command to execute!")
                #->trace()
                ->flush();
            return $this;
        }

        if (isset($this->logger)) {
            $this->logger->debug($command, 'command:');
        }

        $pipeNames = ['stdin', 'stdout', 'stderr'];
        $process = proc_open(
            $command,
            [
                ["pipe", "r"], # stdin
                ["pipe", "w"], # stdout
                ["pipe", "w"], # stderr
            ],
            $pipes
        );

        if (is_resource($process)) {
            # https://stackoverflow.com/questions/31194152/proc-open-hangs-when-trying-to-read-from-a-stream?answertab=votes#tab-top
            # array_reverse() is needed so that the diagnostic stream (stderr) is closed first, otherwise some commands will hang forever.
            foreach (array_reverse(array_keys($pipes)) as $i) {
                if (array_key_exists($pipeNames[$i], $allOutput)) {
                    $output = stream_get_contents($pipes[$i]); # clear the pipe
                    $allOutput[ $pipeNames[$i] ] = trim($output, "\r\n");
                }
                fclose($pipes[$i]);
            }
            $exitStatus = proc_close($process);

            if (isset($this->logger)) {
                # log debug in original order
                foreach ($allOutput as $outputName => $output) {
                    $prefix = sprintf("%-7s:", $outputName);
                    if (strpos($output, "\n") > -1) {
                        $this->logger->debug(sprintf("[begin]\n%s", $output), $prefix);
                        $this->logger->debug('[end]', $prefix);
                    } else {
                        $this->logger->debug($output, $prefix);
                    }
                }
            }
        }

        if (isset($this->logger)) {
            $this->logger->info(sprintf("Exit Status: %s", $exitStatus));
        }

        $this->exitStatus = $exitStatus;
        $this->output = $allOutput;
        return $this;
    }

    public function getExitStatus() {
        return $this->exitStatus;
    }

    public function getOutput() {
        return $this->output;
    }

    public function getSTDOUT() {
        return array_key_exists('stdout', $this->output) ? $this->output['stdout'] : '';
    }

    public function getSTDERR() {
        return array_key_exists('stderr', $this->output) ? $this->output['stderr'] : '';
    }
}
