<?php

namespace Fie\Automation\Shell;

use Exception;
use Fie\Automation\Service;
use Fie\Automation\ServiceUtils\BuildConfig;
use Fie\Automation\ServiceUtils\Logger;

final class Git
{
    private $buildConfig;
    private $logger;
    private $executeCommandObjects = [];
    private $repositoryInitialized = false;
    private $repositoryName;
    private $repositoryRoot;

    /**
     * @param Logger $logger
     * @param BuildConfig $buildConfig
     * @throws \Exception
     */
    public function __construct(Logger $logger = null, BuildConfig $buildConfig = null) {
        $this->buildConfig = $buildConfig ?: new BuildConfig();
        $this->logger      = $logger      ?: new Logger();
    }

    /**
     * Remove temporary repository.
     * Executes after ExecutionPlan's onSuccess() or onFailure()
     *
     * @throws \Exception
     */
    public function __destruct() {
        $this->removeTemporaryRepository();
        $this->logger->flush();
    }

    /**
     * @return string
     */
    public function getRepositoryRoot() {
        return $this->repositoryRoot;
    }

    /**
     * @return string
     */
    public function getTarVerboseFlag() {
        return $this->logger->isVerbose() ? 'v' : '';
    }

    /** @throws \Exception */
    public function getArtifactDirectory() {
        return $this->buildConfig->getValue('git', 'artifacts', 'buildDirectory');
    }

    /** @throws \Exception */
    public function getArtifactPrefix() {
        return $this->buildConfig->getValue('git', 'artifacts', 'buildPrefix');
    }

    /** @throws \Exception */
    public function getRepositoryDirectory() {
        return $this->buildConfig->getValue('git', 'repositories', 'buildDirectory');
    }

    /** @throws \Exception */
    public function getRepositoryPrefix() {
        return $this->buildConfig->getValue('git', 'repositories', 'buildPrefix');
    }

    /** @throws \Exception */
    public function getClonePrefix() {
        return $this->buildConfig->getValue('git', 'repositories', 'clonePrefix');
    }

    /**
     * @return array
     */
    public static function getOptions(): array {
        return [
            'Git' => [
                'repository=repository_name' => 'Specifies the name of the target repository.',
                'branch=branch_name'         => 'Specifies the name of the target branch in the repository.',
                'source=source_branch_name'  => 'Specifies the name of the source branch in the repository.',
                'tag=tag_name'               => 'Specifies the name of the target tag in the repository.',
            ],
        ];
    }

    /**
     * Die unless repository is initialized.
     *
     * @throws \Exception
     */
    private function ensureRepositoryInitialized() {
        if (!$this->repositoryInitialized && count($this->executeCommandObjects) == 0) {
            throw new Exception("Repository not initialized.");
        }
    }

    /**
     * Unit test utility.
     *
     * @param array $executeCommandObjects
     * @return Git $this
     */
    public function setExecuteCommandObjects(array $executeCommandObjects) {
        if (Service::isPhpUnit()) {
            $this->executeCommandObjects = $executeCommandObjects;
        }
        return $this;
    }

    /**
     * Allow unit tests to mock calls to ExecuteCommand.
     *
     * @return ExecuteCommand
     */
    private function getExecuteCommand() {
        if (count($this->executeCommandObjects) > 0) {
            $executeCommand = array_shift($this->executeCommandObjects);
            if (is_object($executeCommand) && method_exists($executeCommand, 'run')) {
                return $executeCommand;
            } else {
                return $this->getExecuteCommand();
            }
        } else {
            return new ExecuteCommand($this->logger);
        }
    }

    /**
     * Remove artifacts older than X days.
     *
     * @param int $age
     * @throws \Exception
     * @return bool
     */
    public function removeOldArtifacts(int $age = 5) {
        $executeCommand = $this->getExecuteCommand();
        $executeCommand->run(sprintf("find '%s/' -mtime +%d -type f -name '%s*' -exec rm -rf '{}' \\;", $this->getArtifactDirectory(), $age, $this->getArtifactPrefix()));
        return true;
    }

    /**
     * Create a standard artifact file name.
     *
     * @param string $repositoryName
     * @param string $resourceName
     * @throws \Exception
     * @return string
     */
    public function getArtifactName(string $repositoryName, string $resourceName) {
        $artifactName = sprintf("%s%s_%s.tar.gz", $this->getArtifactPrefix(), $repositoryName, $resourceName);
        $artifactName = strtolower($artifactName);
        $artifactName = preg_replace(["/ +/", "/--+/"], "-", $artifactName);
        return $artifactName;
    }

    /**
     * Create a temporary repository to do our work in.
     *
     * @param string $repositoryName
     * @throws \Exception
     * @return bool
     */
    public function clone(string $repositoryName) {
        if (preg_match('/devops/i', $repositoryName)) {
            throw new Exception("DANGER: requested repository is devops");
        }

        if ($this->repositoryInitialized) {
            return true;
        }

        $this->repositoryRoot = $this->getRepositoryDirectory() . "/" . $this->getRepositoryPrefix() . uniqid();
        mkdir($this->repositoryRoot);

        $this->logger
            ->info(sprintf("Setting up '%s' repository at '%s'...", $repositoryName, $this->repositoryRoot))
            ->flush();

        $executeCommand = ($this->getExecuteCommand())->run(sprintf("git clone -q --no-checkout %s/%s.git %s", $this->getClonePrefix(), $repositoryName, $this->repositoryRoot));

        if ($executeCommand->getExitStatus() != 0) {
            throw new Exception(sprintf("Could not clone repository '%s' to '%s'\nSTDERR: %s", $repositoryName, $this->repositoryRoot, $executeCommand->getSTDERR()));
        }

        $this->logger
            ->info("Repository created.")
            ->lineBreakVerbose()
            ->flush();

        chdir($this->repositoryRoot);

        $this->repositoryName = $repositoryName;
        $this->repositoryInitialized = true;
        return $this->repositoryInitialized;
    }

    /**
     * @codeCoverageIgnore
     * @throws \Exception
     */
    private function removeTemporaryRepository() {
        if (empty($this->repositoryRoot)) {
            return;
        }

        $this->logger
            ->lineBreakVerbose()
            ->info(sprintf("Removing temporary git repository '%s'...", $this->repositoryRoot));

        if (!preg_match(sprintf("!^/tmp/%s!", $this->getRepositoryPrefix()), $this->repositoryRoot) ||
            preg_match('![^a-z0-9_/.]!i', $this->repositoryRoot) ||
            strpos($this->repositoryRoot, '..') !== false
        ) {
            $this->logger
                ->warn(sprintf("DANGER: Unsafe to remove temporary repository '%s'", $this->repositoryRoot))
                ->flush();
            return;
        }

        $executeCommand = ($this->getExecuteCommand())->run(sprintf("rm -rf %s", $this->repositoryRoot));

        if ($executeCommand->getExitStatus() != 0) {
            $this->logger->warn(sprintf("ERROR!! Could not remove temporary repository '%s'", $this->repositoryRoot));
        } else {
            $this->logger->info("Done.");
        }
    }

    /**
     * Determine if a branch exists in the remote git repo.
     *
     * @param string $branchName
     * @throws \Exception
     * @return bool
     */
    public function branchExists(string $branchName) {
        $this->ensureRepositoryInitialized();

        $executeCommand = ($this->getExecuteCommand())->run(
            "git branch -r | ".
            sprintf("grep 'origin/%s$'", $branchName)
        );
        return $executeCommand->getExitStatus() == 0 ? true : false;
    }

    /**
     * Determine if a tag exists in the remote git repo.
     *
     * @param string $tagName
     * @throws \Exception
     * @return bool
     */
    public function tagExists(string $tagName) {
        $this->ensureRepositoryInitialized();

        $executeCommand = ($this->getExecuteCommand())->run(
            "git ls-remote --tags origin | ".
            sprintf("grep 'tags/%s$'", $tagName)
        );
        return $executeCommand->getExitStatus() == 0 ? true : false;
    }

    /**
     * @param string $branchOrTagName
     * @throws \Exception
     * @return bool
     */
    public function resourceExists(string $branchOrTagName) {
        return $this->branchExists($branchOrTagName) || $this->tagExists($branchOrTagName);
    }

    /**
     * Get latest tag in remote git repo and increment the minor version.
     *
     * @param string $nextTagPrefix
     * @throws \Exception
     * @return string
     */
    public function getNextTag(string $nextTagPrefix = null) {
        $this->ensureRepositoryInitialized();

        $nextTagPrefix = $nextTagPrefix ?: $this->buildConfig->getValue('git', 'repositoryConfigs', $this->repositoryName, 'nextTagPrefix');

        $executeCommand = ($this->getExecuteCommand())->run(
            "git fetch -q && ".
            "git tag -l --sort=v:refname | ".
            sprintf("grep '^%s' | ", $nextTagPrefix).
            "tail -1"
        );
        $lastTag = $executeCommand->getSTDOUT();

        if (empty($lastTag)) {
            throw new Exception("Could not determine next tag.");
        }

        list($major, $minor, $patch) = explode('.', $lastTag);
        $nextTag = implode('.', [$major, $minor, ++$patch]);

        return $nextTag;
    }

    /**
     * @param string $branchName
     * @param string $destinationBranchName
     * @throws \Exception
     * @return $this
     */
    private function mergePrepare(string $branchName, string $destinationBranchName) {
        $this->ensureRepositoryInitialized();

        $executeCommand = $this->getExecuteCommand();

        $commands = [
            sprintf("git checkout '%s'", $destinationBranchName),
            sprintf("git checkout '%s'", $branchName),
            sprintf("git merge '%s'", $destinationBranchName),
            sprintf("git push"),
        ];
        foreach ($commands as $command) {
            $executeCommand->run($command);

            if ($executeCommand->getExitStatus() !== 0) {
                throw new Exception(sprintf("Could not prepare for git merge '%s' into '%s'. Error:\n%s", $branchName, $destinationBranchName, $executeCommand->getSTDERR()));
            }
        }

        return $this;
    }

    /**
     * @param string $branchName
     * @param string $destinationBranchName
     * @throws \Exception
     * @return $this
     */
    public function merge(string $branchName, string $destinationBranchName = 'master') {
        $this->ensureRepositoryInitialized();

        $this->mergePrepare($branchName, $destinationBranchName);

        $executeCommand = $this->getExecuteCommand();

        $commands = [
            sprintf("git checkout '%s'", $destinationBranchName),
            sprintf("git merge '%s'", $branchName),
            sprintf("git push"),
        ];
        foreach ($commands as $command) {
            $executeCommand->run($command);

            if ($executeCommand->getExitStatus() !== 0) {
                throw new Exception(sprintf("Could not git merge '%s' into '%s'. Error:\n%s", $branchName, $destinationBranchName, $executeCommand->getSTDERR()));
            }
        }

        return $this;
    }

    /**
     * @param string $tagName
     * @throws \Exception
     * @return $this
     */
    public function tag(string $tagName) {
        $this->ensureRepositoryInitialized();

        $executeCommand = ($this->getExecuteCommand())->run(
            sprintf("git tag '%s' -m '%s' && git push && git push origin '%s'", $tagName, $tagName, $tagName)
        );

        if ($executeCommand->getExitStatus() !== 0) {
            throw new Exception(sprintf("Could not git tag '%s'. Error: %s", $executeCommand->getSTDERR(), $tagName));
        }

        return $this;
    }

    /**
     * @param string $branchName
     * @param string $destinationBranchName
     * @throws \Exception
     * @return bool
     */
    public function hasCodeChanges(string $branchName, string $destinationBranchName = 'master') {
        $this->ensureRepositoryInitialized();

        $executeCommand = ($this->getExecuteCommand())->run(
            sprintf("git diff --stat origin/%s..origin/%s | wc -l", $branchName, $destinationBranchName)
        );

        $numFilesChanged = trim($executeCommand->getSTDOUT());

        return (bool)$numFilesChanged;
    }
}
