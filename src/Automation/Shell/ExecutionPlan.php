<?php

namespace Fie\Automation\Shell;

use Exception;
use Fie\Automation\ServiceBase;
use Fie\Automation\ServiceUtils\ServiceException;
use Fie\Automation\Shell\ExecutionPlan\ExecutionStep;

abstract class ExecutionPlan extends ServiceBase
{
    /** @return array */
    abstract protected function getExecutionPlan(): array;

    /** @var ExecutionStep $currentStep */
    private $currentStep = null;

    /** @var ExecutionStep[] $executionSteps */
    private $executionSteps = [];

    private $executionStepNumber = 1;
    private $rollbackStepNumber = 1;
    private $rollbackSteps = [];

    /**
     * Make it go.
     *
     * @throws \Exception
     */
    public function run(): ServiceBase {
        return $this->runExecutionPlan();
    }

    /**
     * @throws ServiceException
     */
    final protected function runExecutionPlan() {
        $this
            ->setup()
            ->executePlan()
            ->exitSuccess();
    }  // @codeCoverageIgnore

    /**
     * Configure, retrieve, and validate the execution plan
     *
     * @throws ServiceException
     * @return ExecutionPlan $this
     */
    private function setup() {
        try {
            $this->executionSteps = $this->getExecutionPlan(); # throws?
            $this->validateExecutionPlan(); # throws
        } catch (ServiceException $e) {
            # fall through...
            throw $e;
        } catch (Exception $e) {
            $this->logger
                ->warn($e->getMessage())
                ->flush();
            $this->fatal();
        }

        return $this;
    }

    /**
     * Validate the given plan.
     *
     * @throws ServiceException
     * @return ExecutionPlan $this
     */
    private function validateExecutionPlan() {
        $this->logger->info("Validating execution plan...");

        if (count($this->executionSteps) == 0) {
            $this->logger
                ->warn("Empty execution plan!")
                ->lineBreakVerbose()
                ->flush();
            $this->exitFailure();
        }

        $executionStepNumber = 1;
        foreach ($this->executionSteps as $idx => $step) {
            try {
                if (is_object($step)) {
                    if ($step instanceof ExecutionStep) {
                        $executionStep = $step;
                    } else {
                        throw new Exception("Invalid object!");
                    }
                } elseif (is_array($step)) {
                    $executionStep = new ExecutionStep($step); # throws if invalid
                } else {
                    throw new Exception("Invalid object!");  // @codeCoverageIgnore
                }
                $this->executionSteps[$idx] = $executionStep;
            } catch (Exception $e) {
                $this->logger
                    ->warn("Malformed execution plan!")
                    ->warn(sprintf("Error in execution step %03d. %s. ExecutionStep details:", $executionStepNumber, $e->getMessage()))
                    ->logVar($step)
                    ->lineBreakVerbose()
                    ->flush();
                $this->exitFailure();
            }
            $executionStepNumber++;
        }

        $this->logger
            ->info("Valid.")
            ->lineBreakVerbose()
            ->info("Execution Plan:");

        $executionStepNumber = 1;
        foreach ($this->executionSteps as $step) {
            $this->logger->info(sprintf("%03d)  %s", $executionStepNumber++, $step->getCommand()));
        }
        $this->logger
            ->lineBreakVerbose()
            ->lineBreakVerbose()
            ->flush();

        return $this;
    }

    /**
     * Execute the given plan.
     *
     * @throws ServiceException
     * @return ExecutionPlan $this
     */
    private function executePlan() {
        foreach ($this->executionSteps as $step) {
            $this->currentStep = $step;
            $this->executeStep($step);
        }
        $this->logger->flush();

        return $this;
    }

    /**
     * Execute a step in the execution plan.
     *
     * @param ExecutionStep $step
     * @throws ServiceException
     * @return ExecutionPlan $this
     */
    private function executeStep(ExecutionStep $step) {
        if ($this->executionStepNumber > 1) {
            $this->logger->lineBreakVerbose();
        }
        $this->logger
            ->info(sprintf("%03d)", $this->executionStepNumber))
            ->infoVar($step->getCommand(),       "command:      ")
            ->infoVar($step->getRollback(),      "rollback:     ")
            ->infoVar($step->getClearRollback(), "clearRollback:")
            ->flush();

        if ($step->getClearRollback()) {
            $this->rollbackSteps = [];
            $this->logger
                ->logSuccess("Rollback plan truncated.")
                ->flush();
        }

        if (count($step->getRollback()) > 0) {
            $this->rollbackSteps = array_merge($step->getRollback(), $this->rollbackSteps);
        }

        $executeCommand = (new ExecuteCommand($this->logger))->run($step->getCommand());

        if ($executeCommand->getExitStatus() != 0) {
            $this
                ->describeRollbackPlan()
                ->executeRollbackPlan()
                ->exitFailure();
        }

        $this->logger
            ->logSuccess(sprintf("Execution step %03d successful!", $this->executionStepNumber))
            ->flush();

        $this->executionStepNumber++;
        return $this;
    }

    /**
     * Describe the steps to be taken in a rollback scenario.
     *
     * @throws ServiceException
     * @return ExecutionPlan $this
     */
    private function describeRollbackPlan() {
        $this->logger
            ->warn(sprintf("Execution step %03d failed!", $this->executionStepNumber))
            ->lineBreakVerbose()
            ->lineBreakVerbose();

        $this->logger->info("Rollback plan:");
        if (count($this->rollbackSteps) == 0) {
            $this->logger->info("None!");
            $this->exitFailure();
        }

        $rollbackStepNumber = $this->rollbackStepNumber;
        foreach ($this->rollbackSteps as $rollbackStep) {
            $this->logger->info(sprintf("%03d)  %s", $rollbackStepNumber++, $rollbackStep));
        }

        $this->logger
            ->lineBreakVerbose()
            ->flush();

        return $this;
    }

    /**
     * Executes all rollback steps if a problem is encountered while executing a step in the execution plan.
     *
     * @throws ServiceException
     * @return ExecutionPlan $this
     */
    private function executeRollbackPlan() {
        $this->logger->info("Executing rollback plan...");
        foreach ($this->rollbackSteps as $rollbackStep) {
            if ($this->rollbackStepNumber > 1) {
                $this->logger->lineBreakVerbose();
            }
            $this->logger->infoVar($rollbackStep, sprintf("%03d) ", $this->rollbackStepNumber));

            $executeCommand = (new ExecuteCommand($this->logger))->run($rollbackStep);

            if ($executeCommand->getExitStatus() != 0) {
                $this->logger
                    ->warn(sprintf("Failed to execute rollback command %03d!", $this->rollbackStepNumber))
                    ->lineBreakVerbose();
                $this->exitFailure();
            }
            $this->logger->logSuccess(sprintf("Rollback command %03d successful.", $this->rollbackStepNumber));

            $this->rollbackStepNumber++;
        }
        $this->logger->flush();

        return $this;
    }
}
