<?php

namespace Fie\Automation\Shell\ExecutionPlan;

use Exception;

final class ExecutionStep
{
    private $command = null;
    private $rollback = [];
    private $clearRollback = false;

    /**
     * $step structure:
     * [
     *      'command'       => 'echo "command to execute"',             # required
     *      'rollback'      => [ 'echo "command to rollback"', ... ],   # optional
     *      'clearRollback' => boolean,                                 # optional
     * ]
     *
     * @param array $step
     * @throws \Exception
     */
    public function __construct(array $step) {
        $this->validateStructure($step);
    }

    /**
     * @param array $step
     * @throws \Exception
     * @return ExecutionStep
     */
    public static function fromArray(array $step) {
        return new self($step);
    }

    /**
     * Returns the 'command' property.
     *
     * @return string
     */
    public function getCommand() {
        return $this->command;
    }

    /**
     * Returns the 'rollback' property.
     *
     * @return array
     */
    public function getRollback() {
        return $this->rollback;
    }

    /**
     * Returns the 'clearRollback' property.
     *
     * @return bool
     */
    public function getClearRollback() {
        return $this->clearRollback;
    }

    /**
     * @param array $step
     * @throws \Exception
     */
    private function validateStructure(array $step) {
        if (array_key_exists('command', $step)) {
            $this->command = $step['command'];
        }
        $this->validateCommand();

        if (array_key_exists('rollback', $step)) {
            if (!is_array($step['rollback'])) {
                $step['rollback'] = [$step['rollback']];
            }
            $this->rollback = $step['rollback'];
            $this->validateRollback();
        }

        if (array_key_exists('clearRollback', $step)) {
            $this->clearRollback = (bool) $step['clearRollback'];
        }
    }

    /**
     * Performs a sanity check on the execution step and rollback execution step(s) we are about to execute.
     *
     * @throws \Exception
     */
    private function validateCommand() {
        if (empty($this->command)) {
            throw new Exception("No command to execute");
        } elseif (!is_string($this->command)) {
            throw new Exception("Malformed command");
        }
    }

    /**
     * Performs a sanity check on the rollback step(s) of an execution step.
     *
     * @throws \Exception
     */
    private function validateRollback() {
        $rollbackStepNumber = 1;
        foreach ($this->rollback as $rollbackStep) {
            if (empty($rollbackStep)) {
                throw new Exception(sprintf("No command to execute for rollback step %03d", $rollbackStepNumber));
            } elseif (!is_string($rollbackStep)) {
                throw new Exception(sprintf("Malformed command for rollback step %03d", $rollbackStepNumber));
            }
            $rollbackStepNumber++;
        }
    }
}
