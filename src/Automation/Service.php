<?php

namespace Fie\Automation;

use Exception;
use Fie\Automation\Auth\User;
use Fie\Automation\ServiceUtils\BuildConfig;
use Fie\Automation\ServiceUtils\DataTracker;
use Fie\Automation\ServiceUtils\LockFile;
use Fie\Automation\ServiceUtils\Logger;
use Fie\Automation\ServiceUtils\Options;
use Fie\Automation\ServiceUtils\ServiceFinder;
use Fie\Automation\ServiceUtils\ServiceException;

final class Service
{
    private $serviceNamespacePrefix = "\\Fie\\Automation\\Services\\";

    /** @var LockFile $lockFile */
    private $lockFile;

    /** @var BuildConfig $buildConfig */
    private $buildConfig;

    /** @var Logger $logger */
    private $logger;

    /** @var ServiceBase $serviceObject */
    private $serviceObject;

    /** @var Options $options */
    private $options;

    /** @var ServiceFinder $serviceFinder */
    private $serviceFinder;

    private $serviceObjectClassName;
    private $serviceClassName;
    private $serviceClassNameFullyQualified;

    /**
     * @param Logger $logger
     * @param ServiceBase $serviceObject
     * @param array $options
     */
    public function __construct(Logger $logger = null, ServiceBase $serviceObject = null, array $options = []) {
        $this->logger        = $logger ?: new Logger();
        $this->lockFile      = new LockFile();
        $this->options       = new Options();
        $this->serviceFinder = new ServiceFinder();

        if (isset($serviceObject)) {
            $this->serviceObjectClassName = self::isPhpUnit() ? 'serviceName' : get_class($serviceObject);
            $this->serviceObject = $serviceObject;
        }

        if (empty($this->serviceObject) && empty($this->serviceObjectClassName)) {
            $this->setServiceClassName();
        }

        $this->serviceClassNameFullyQualified = $this->serviceNamespacePrefix . ($this->serviceObjectClassName ?: $this->serviceClassName);
        if ($this->serviceObject || class_exists($this->serviceClassNameFullyQualified)) {
            $this->serviceObject = $this->serviceObject ?: new $this->serviceClassNameFullyQualified();
        }

        if (count($options)) {
            $this->serviceObject
                ->setLogger($this->logger)
                ->setOptions($options);
        }
    }

    public function __destruct() {
        try {
            $this->lockFile->delete();
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            $this->logger
                ->lineBreak()
                ->warn(sprintf("LockFile Error: %s!", $e->getMessage()));
            // @codeCoverageIgnoreEnd
        }
        $this->logger->flush();
    }

    /**
     * @return bool
     */
    final public static function isPhpUnit() {
        if (defined('PHPUNIT_RUNNING') && PHPUNIT_RUNNING) { # defined in configs/phpunit.xml
            return true;
        }
        return false;  // @codeCoverageIgnore
    }

    /**
     * @return bool
     */
    final public static function isApi() {
        if (!self::isWeb()) {
            return false;
        }
        $uri = array_key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI'] : null;
        $url_path = parse_url($uri, PHP_URL_PATH);
        return (bool)preg_match('!^/api(/|$)!', $url_path);
    }

    /**
     * Detect browser.
     * Better way to do this?
     *
     * @return bool
     */
    final public static function isWeb() {
        return array_key_exists('HTTP_USER_AGENT', $_SERVER) && isset($_SERVER['HTTP_USER_AGENT']);
    }

    /**
     * Set dataType. Example: 'application/json'
     * @param string $dataType
     * @return $this
     */
    final public function setDataType($dataType) {
        $this->logger->setDataTracker(new DataTracker($dataType));
        return $this;
    }

    /**
     * Get/Determine service class name.
     *
     * @return string
     */
    final public function getServiceClassName() {
        if (empty($this->serviceClassName)) {
            $this->setServiceClassName();
        }
        return $this->serviceClassName;
    }

    /**
     * Get service name from CLI args or web URI.
     *
     * @return Service $this
     */
    final public function setServiceClassName() {
        $uri = array_key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI'] : null;

        if (!empty($uri) || $uri === '') {
            $url_path = parse_url($uri, PHP_URL_PATH);
            $className = trim($url_path, '/');
        } else {
            $argv = Options::getArgv();
            $className = array_key_exists(1, $argv) ? $argv[1] : null;
        }

        $className = preg_replace(['!^--!', '!^src/Automation/Services/!', '!^api/!', '!\.php$!'], '', $className);
        $className = str_replace('/', "\\", $className);

        $this->serviceClassName = $className;
        return $this;
    }

    /**
     * @param $serviceFinder
     * @return Service $this
     */
    public function setServiceFinder($serviceFinder) {
        if (is_object($serviceFinder) && method_exists($serviceFinder, 'find')) {
            $this->serviceFinder = $serviceFinder;
        }
        return $this;
    }

    /**
     * Catch exceptions during setup and continue execution of caller (e.g. index.php)
     *
     * @return bool
     */
    private function setup() {
        try {
            $this->buildConfig = new BuildConfig();
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            $this->logger
                ->warn(sprintf("BuildConfig Error! %s", $e->getMessage()))
                ->flush();
            return false;
            // @codeCoverageIgnoreEnd
        }

        try {
            $this->options->processOptions();
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            if (!Service::isPhpUnit()) {
                $this->logger->setup($this->options);
            }
            $this->logger
                ->warn($e->getMessage())
                ->flush();
            return false;
            // @codeCoverageIgnoreEnd
        }

        if (!Service::isPhpUnit()) {
            $this->logger->setup($this->options);  // @codeCoverageIgnore
        }

        if (self::isWeb()) {
            // @codeCoverageIgnoreStart
            $user = new User();
            if (!$user->authenticate()) {
                $this->logger
                    ->lineBreak()
                    ->warn("Unauthorized!")
                    ->flush();
                return false;
            }
            // @codeCoverageIgnoreEnd
        }

        # organized this way for test coverage
        if (!empty($this->serviceClassName) && !isset($this->serviceObject)) {
            $this->help();
            return false;
        } elseif ($this->serviceClassName === '') {
            $this->help();
            return false;
        } elseif (!isset($this->serviceObject)) {
            return false;  // @codeCoverageIgnore
        }

        $serviceName = $this->serviceObjectClassName ?: (isset($this->serviceObject) ? get_class($this->serviceObject) : $this->serviceClassName);
        $lockFileName = sprintf("/tmp/automation_%s.lock", strtolower(str_replace("\\", "_", $serviceName)));
        try {
            $this->lockFile->create($lockFileName);
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            $this->logger
                ->lineBreak()
                ->warn(sprintf("LockFile Error: %s!", $e->getMessage()))
                ->flush();
            return false;
            // @codeCoverageIgnoreEnd
        }

        return true;
    }

    /**
     * Execute run() on the dynamically created services class.
     *
     * @return bool
     */
    final public function run() {
        $returnStatus = false;

        if (!$this->setup()) {
            $this->logger
                ->flush()
                ->printData($returnStatus);
            return $returnStatus;
        }

        $this->logger
            ->info(sprintf("Begin: %s", trim(preg_replace(['!\\\\!', '!Fie/Automation/Services!'], ['/', ''], $this->serviceClassNameFullyQualified), '/')))
            ->lineBreakVerbose();

        try {
            $this->serviceObject
                ->setBuildConfig($this->buildConfig)
                ->setLogger($this->logger)
                ->configureAndRun(); # throws
            $returnStatus = true;
        } catch (ServiceException $e) {
            $returnStatus = $e->isSuccessful();
            // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            # unknown runtime error
            $this->logger
                ->warn($e->getMessage())
                ->flush();
            // @codeCoverageIgnoreEnd
        }

        if (isset($this->serviceObject)) {
            # force ServiceBase to destruct now, not later.
            unset($this->serviceObject);
        }

        $this->logger->printData($returnStatus);

        if (!$this->options->getOptionFlag('help')) {
            $this->logger
                ->debug(sprintf("Service run() returned: %s", $returnStatus ? 'true' : 'false'))
                ->flush();
        }

        return $returnStatus;
    }

    /**
     * @return Service $this
     */
    private function help() {
        $isIndex = strlen($this->serviceClassName) == 0 || preg_match("/^index/", $this->serviceClassName) == 1;
        $isApiIndex = $this->serviceClassName == "api";
        $isHelp = $this->serviceClassName == "help";

        if (!$isIndex && !$isApiIndex && !$isHelp) {
            $this->logger
                ->warn(sprintf("Invalid service_name '%s'.", $this->serviceClassName))
                ->lineBreak()
                ->lineBreak()
                ->flush();
        }

        if (self::isWeb()) {
            $hostname = array_key_exists('HTTP_HOST', $_SERVER) ? $_SERVER['HTTP_HOST'] : '[hostname]';

            if (self::isApi()) {
                $this->logger
                    ->log(sprintf('Help documentation available at http://%s/?help', $hostname))
                    ->flush();
                return $this;
            }

            $usage = <<<EOL
Usage:
    http://$hostname/service_name

Additional help documentation available with:
    http://$hostname/service_name?help
EOL;
        } else {
            $script = $_SERVER['argv'][0];
            $usage = <<<EOL
Usage:
    $script service_name

Additional help documentation available with:
    $script service_name --help
EOL;
        }

        $this->logger
            ->log($usage)
            ->lineBreak()
            ->lineBreak()
            ->flush();
        $this->listServices();
        $this->logger->flush();

        return $this;
    }

    /**
     * Look up valid values for service_name.
     *
     * @return Service $this
     */
    private function listServices() {
        $services = $this->serviceFinder->find(__DIR__ . '/Services');

        $this->logger->log("List of known values for service_name:");
        if (count($services) > 0) {
            $this->logger->log("    ".implode("\n    ", $services));
        } else {
            $this->logger->log("    (Could not find service_name list)");
        }

        return $this;
    }
}
