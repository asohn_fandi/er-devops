<?php

namespace Fie\Automation\App;

require_once(__DIR__ . '/../bootstrap.php');

use Fie\Automation\Service;

?>
<!doctype html>
<html>
    <head>
        <title>DevOps</title>
    </head>
    <body>
        <pre>
<?php
$service = new Service();
$service->run();
?>
        </pre>
    </body>
</html>
