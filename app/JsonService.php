<?php

/**
 * Very basic JSON WebService for automation services.
 */

namespace Fie\Automation\App;

require_once(__DIR__ . '/../bootstrap.php');

use Fie\Automation\Service;

(new Service())
    ->setDataType('application/json')
    ->run();
