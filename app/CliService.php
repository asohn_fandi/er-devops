<?php

/**
 * Command-Line Interface (CLI) for automation services.
 */

namespace Fie\Automation\App;

use Fie\Automation\Service;

require_once(__DIR__ . '/../bootstrap.php');

$service = new Service();
exit($service->run() ? 0 : 1);
